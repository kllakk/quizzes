<?php


class ResultsController
{

    /*
     * Отправка через очередь битрикс, на всякий случай:
     *
     * 1. предварительно создать шаблон quiz и message (напр., 66) с параметрами USER_TO, MESSAGE, SUBJECT
     * 2. $result = CEvent::Send('quiz', 's1', array( 'USER_TO' => '', 'MESSAGE' => 'Mail text', 'SUBJECT' => 'Mail subject' ), 'N', 66);
     * */

    public function submit()
    {
        header('Content-Type: application/json');

        switch ($_POST['action']) {
            case 'qmn_process_quiz':

                $sent = false;
                $quiz = QuizzesQuery::create()->findPk($_POST['qmn_quiz_id']);
                $QuizId = $quiz->getQuizId();
                $QuizName = $quiz->getQuizName();
                $settings = json_decode($quiz->getQuizSettings());

                $qaArray = array ();
                foreach ($_POST as $key => $value) {
                    if (preg_match('/^question(\d+)(_(\d+))?$/', $key, $matches)) {
                        $questionId = isset($matches[1]) ? $matches[1] : null;
                        if ($questionId) {
                            $question = QuestionsQuery::create()->findPk($questionId);
                            $QuestionName = $question->getQuestionName();
                            $qaArray [$questionId]['name'] = $QuestionName;
                            $qaArray [$questionId]['answers'][] = $_POST[$matches[0]];
                        }
                    }
                }

                $contactName = $_POST['contact_field_0'];
                $contactPhone = $_POST['contact_field_1'];

                $resultsFormatted = $this->formatResults($QuizName, $qaArray, $contactName, $contactPhone);

                $results = new Results();
                $results->setQuizId($QuizId);
                $results->setQuizName($QuizName);
                $results->setName($contactName);
                $results->setPhone($contactPhone);
                $results->setQuizResults($resultsFormatted);
                $results->save();

                if ($settings->emails) {
                    $adminEmail = COption::GetOptionString('main', 'email_from', 'default@admin.email');

                    $emails = array();
                    // валидация емейлов
                    foreach (explode(',', $settings->emails) as $email) {
                        $email = trim($email);
                        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $emails [] = $email;
                        }
                    }

                    if (count($emails) > 0) {
                        $to      = implode(',', $emails);
                        $subject = $settings->subject ? $settings->subject : $QuizName;

                        $message = $resultsFormatted;
                        $headers [] = "From: {$adminEmail}";
                        $headers [] = "Reply-To: {$adminEmail}";
                        $headers [] = "X-Mailer: PHP/" . phpversion();
                        $headers [] = "MIME-Version: 1.0";
                        $headers [] = "Content-type: text/html; charset=UTF-8";

                        $sent = mail($to, $subject, $message, implode("\r\n", $headers));
                    }
                }

                $resultsText = $settings ? $settings->resultsText : 'Спасибо';
                return json_encode(array(
                    'result' => $sent,
                    'display' => '<div class="qsm-results-page"><span style="color: green">✔</span>' . $resultsText . '</div>',
                    'redirect' => false
                ));
                break;
        }
    }

    private function formatResults($QuizName, $qaArray, $contactName, $contactPhone)
    {
        $date = date('d.m.Y');

        $result = '<html><body>';
        $result .= "<p>Клиент: {$contactName}</p>";
        $result .= "<p>Телефон: {$contactPhone}<p>";
        //$result .= "<p>Опрос: {$QuizName}</p>";
        $result .= "<p>Дата: {$date}</p>";

        foreach ($qaArray as $question) {

            foreach ($question['answers'] as &$answer) {
                if (preg_match('/\/quizzes\/upload\/(.*).jpg/i', $answer)) {
                    $answer = '<img style="max-width: 250px;" src="//' .  $_SERVER['SERVER_NAME'] . ($_SERVER['SERVER_PORT'] != 80 ? ':' . $_SERVER['SERVER_PORT'] : '') . $answer . '" />';
                }
            }

            $answers = implode(',', $question['answers']);
            $result .= "<p><b>{$question['name']}</b>: {$answers}</p>";
        }

        $result .= "</body></html>";

        return $result;
    }
}