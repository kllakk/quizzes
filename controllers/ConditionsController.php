<?php


class ConditionsController
{
    public function get()
    {
        header('Content-Type: application/json');

        $result = '';

        if (isset($_GET['QuestionId'])) {
            if ($condition = ConditionsQuery::create()->filterByQuestionId($_GET['QuestionId'])->findOne()) {
                $result = $condition->toJSON(false);
            }
        }

        return $result;
    }

    public function post()
    {
        header('Content-Type: application/json');

        $result = '';

        if (isset($_GET['operation']) && isset($_POST['QuizId']) && isset($_POST['QuestionId'])) {
            switch ($_GET['operation']) {
                case 'update':
                    if (!($condition = ConditionsQuery::create()->filterByQuestionId($_POST['QuestionId'])->findOne())) {
                        $condition = new Conditions();
                    }
                    $condition->setQuizId($_POST['QuizId']);
                    $condition->setQuestionId($_POST['QuestionId']);
                    $condition->setConditionArray(isset($_POST['ConditionArray']) ? json_encode($_POST['ConditionArray']) : null);
                    $condition->save();
                    $result = $condition->toJSON();
                    break;
            }
        }

        return $result;
    }
}