<?php


class QuizzesController
{
    public $app;

    public function import()
    {
        $content = file_get_contents($_FILES['file']['tmp_name'][0]);
        $data = json_decode($content, true);

        if ($iQuiz = $data['rfn_quizzes'][0]) {
            $prevQuizId = $iQuiz['quiz_id'];
            $QuizName = $iQuiz['quiz_name'];
            $QuizSettings = $iQuiz['quiz_settings'];

            $quiz = new Quizzes();
            $quiz->setQuizName($QuizName);
            $quiz->setQuizSettings($QuizSettings);
            $quiz->save();
            $quizId = $quiz->getQuizId();

            if ($quizId) {
                $prevQuestionsMap = array();
                if ($questions = $data['rfn_questions']) {
                    foreach ($questions as $iQuestion) {
                        $prevQuestionId = $iQuestion['question_id'];
                        $QuestionName = $iQuestion['question_name'];
                        $QuestionSettings = $iQuestion['question_settings'];
                        $QuestionType = $iQuestion['question_type'];
                        $AnswerArray = $iQuestion['answer_array'];
                        $QuestionOrder = $iQuestion['question_order'];

                        $question = new Questions();
                        $question->setQuizId($quizId);
                        $question->setQuestionName($QuestionName);
                        $question->setQuestionSettings($QuestionSettings);
                        $question->setQuestionType($QuestionType);
                        $question->setAnswerArray($AnswerArray);
                        $question->setQuestionOrder($QuestionOrder);
                        $question->save();
                        $questionId = $question->getQuestionId();

                        $prevQuestionsMap [$prevQuestionId] = $questionId;
                    }
                }

                if ($conditions = $data['rfn_conditions']) {
                    foreach ($conditions as $iCondition) {
                        $condition = new Conditions();
                        $condition->setQuizId($quizId);
                        $condition->setQuestionId($prevQuestionsMap[$iCondition['question_id']]);

                        $ConditionArray = json_decode($iCondition['condition_array'], true);
                        foreach ($ConditionArray as &$condition_item) {
                            $condition_item['QuestionRelatedId'] = $prevQuestionsMap[$condition_item['QuestionRelatedId']];
                        }
                        $ConditionArray = json_encode($ConditionArray);

                        $condition->setConditionArray($ConditionArray);
                        $condition->save();
                    }
                }
            }
        }

        return array(
            'success' => isset($quizId) ? $quizId : false
        );
    }

    public function export($id)
    {
        $result = array();

        foreach (QuizzesQuery::create()->filterByQuizId($id)->find() as $quiz) {
            $result ['rfn_quizzes'][] = array(
                'quiz_id' => $quiz->getQuizId(),
                'quiz_name' => $quiz->getQuizName(),
                'quiz_settings' => $quiz->getQuizSettings(),
            );
        }

        foreach (QuestionsQuery::create()->filterByQuizId($id)->find() as $question) {
            $result ['rfn_questions'][] = array(
                'question_id' => $question->getQuestionId(),
                'quiz_id' => $question->getQuizId(),
                'question_name' => $question->getQuestionName(),
                'question_settings' => $question->getQuestionSettings(),
                'question_type' => $question->getQuestionType(),
                'answer_array' => $question->getAnswerArray(),
                'question_order' => $question->getQuestionOrder(),
            );
        }

        foreach (ConditionsQuery::create()->filterByQuizId($id)->find() as $condition) {
            $result ['rfn_conditions'][] = array(
                'quiz_id' => $condition->getQuizId(),
                'question_id' => $condition->getQuestionId(),
                'condition_array' => $condition->getConditionArray(),
            );
        }

        return $result;
    }

    public function preview($id)
    {
        $quiz = QuizzesQuery::create()->findPk($id)->toArray();
        $questions = QuestionsQuery::create()->filterByQuizId($id)->orderByQuestionId(Criteria::ASC)->find()->toArray();
        $conditions = ConditionsQuery::create()->filterByQuizId($id)->find()->toArray();
        $this->app->render('quizzes/preview.php', compact('id', 'quiz', 'questions', 'conditions'));
    }

    public function get()
    {
        header('Content-Type: application/json');

        $result = '';

        if (isset($_GET['id'])) {
            if ($quiz = QuizzesQuery::create()->findPk($_GET['id'])) {
                $result = $quiz->toJSON(false);
            }
        } else {
            $quiz = QuizzesQuery::create()->orderByQuizId(Criteria::DESC)->find();
            $result = $quiz->toJSON(false);
        }

        return $result;
    }

    public function post()
    {
        header('Content-Type: application/json');

        $result = '';

        if (isset($_GET['operation'])) {
            switch ($_GET['operation']) {
                case 'create':
                    if (isset($_POST['QuizName'])) {
                        $quiz = new Quizzes();
                        $quiz->setQuizName($_POST['QuizName']);
                        $quiz->setQuizSettings(isset($_POST['QuizSettings']) ? json_encode($_POST['QuizSettings']) : null);
                        $quiz->save();
                        $result = $quiz->toJSON();
                    }
                    break;
                default:
                    if (isset($_POST['id'])) {
                        if ($quiz = QuizzesQuery::create()->findPk($_POST['id'])) {
                            switch ($_GET['operation']) {
                                case 'update':
                                    $quiz->setQuizName($_POST['QuizName']);
                                    $quiz->setQuizSettings(isset($_POST['QuizSettings']) ? json_encode($_POST['QuizSettings']) : null);
                                    $quiz->save();
                                    break;
                                case 'delete':
                                    $quiz->delete();
                                    break;
                                case 'export':
                                    break;
                            }
                            $result = $quiz->toJSON(false);
                        }
                    }
                    break;
            }
        }

        return $result;
    }

    public function __construct($app)
    {
        $this->app = $app;
    }
}