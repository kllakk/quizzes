<?php


class QuestionsController
{
    public function get()
    {
        header('Content-Type: application/json');

        $result = '';

        if (isset($_GET['id'])) {
            if ($question = QuestionsQuery::create()->findPk($_GET['id'])) {
                $result = $question->toJSON(false);
            }
        } elseif (isset($_GET['QuizId'])) {
            $question = QuestionsQuery::create()->filterByQuizId($_GET['QuizId'])->orderByQuestionId(Criteria::ASC)->find();
            $result = $question->toJSON(false);
        }

        return $result;
    }

    public function post()
    {
        header('Content-Type: application/json');

        $result = '';

        if (isset($_GET['operation'])) {
            switch ($_GET['operation']) {
                case 'create':
                    if (isset($_POST['form'])) {
                        $form = json_decode($_POST['form'], true);
                        if (isset($form['QuizId'])) {
                            $question = new Questions();
                            $question->setQuizId($form['QuizId']);
                            $question->setQuestionName($form['QuestionName']);
                            $question->setQuestionType($form['QuestionType']);
                            $answerArray = isset($form['AnswerArray']) ? $form['AnswerArray'] : null;
                            $answerArray = $this->uploaderAnswerArray($_FILES, $answerArray);
                            $question->setAnswerArray(json_encode($answerArray));
                            $question->save();
                            $result = $question->toJSON();
                        }
                    }
                    break;
                default:
                    if (isset($_GET['id'])) {
                        if ($question = QuestionsQuery::create()->findPk($_GET['id'])) {
                            switch ($_GET['operation']) {
                                case 'update':
                                    if (isset($_POST['form'])) {
                                        $form = json_decode($_POST['form'], true);
                                        $question->setQuestionName($form['QuestionName']);
                                        $question->setQuestionType($form['QuestionType']);
                                        $answerArray = isset($form['AnswerArray']) ? $form['AnswerArray'] : null;
                                        $answerArray = $this->uploaderAnswerArray($_FILES, $answerArray);
                                        $question->setAnswerArray(json_encode($answerArray));
                                        $question->save();
                                    }
                                    break;
                                case 'delete':
                                    $question->delete();
                                    break;
                            }
                            $result = $question->toJSON(false);
                        }
                    }
                    break;
            }
        }

        return $result;
    }

    private function uploaderAnswerArray($files, $answerArray)
    {
        $urls = array();
        foreach ($files as $indexName => $file) {
            $name = $file['name'][0];
            $tmp_name = $file['tmp_name'][0];
            $index = str_replace('file-', '', $indexName);
            $nameArray = explode('.', $name);
            $extension = end($nameArray);
            $newName = md5(uniqid(rand(), true));
            $uploads_dir = __DIR__ . '/../upload';
            $result = move_uploaded_file($tmp_name, "{$uploads_dir}/{$newName}.{$extension}");
            $urls [$index] = "/quizzes/upload/{$newName}.{$extension}";
        }

        if ($answerArray) {
            foreach ($answerArray as $index => &$answer) {
                if (array_key_exists($index, $urls)) {
                    $answer['AnswerImage'] = null;
                    $answer['AnswerImageUrl'] = $urls[$index];
                }
            }
        }

        return $answerArray;
    }
}