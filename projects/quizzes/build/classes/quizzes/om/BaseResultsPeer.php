<?php


/**
 * Base static class for performing query and update operations on the 'rfn_results' table.
 *
 *
 *
 * @package propel.generator.quizzes.om
 */
abstract class BaseResultsPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'rfn_new2';

    /** the table name for this class */
    const TABLE_NAME = 'rfn_results';

    /** the related Propel class for this table */
    const OM_CLASS = 'Results';

    /** the related TableMap class for this table */
    const TM_CLASS = 'ResultsTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 18;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 18;

    /** the column name for the result_id field */
    const RESULT_ID = 'rfn_results.result_id';

    /** the column name for the quiz_id field */
    const QUIZ_ID = 'rfn_results.quiz_id';

    /** the column name for the quiz_name field */
    const QUIZ_NAME = 'rfn_results.quiz_name';

    /** the column name for the quiz_system field */
    const QUIZ_SYSTEM = 'rfn_results.quiz_system';

    /** the column name for the point_score field */
    const POINT_SCORE = 'rfn_results.point_score';

    /** the column name for the correct_score field */
    const CORRECT_SCORE = 'rfn_results.correct_score';

    /** the column name for the correct field */
    const CORRECT = 'rfn_results.correct';

    /** the column name for the total field */
    const TOTAL = 'rfn_results.total';

    /** the column name for the name field */
    const NAME = 'rfn_results.name';

    /** the column name for the business field */
    const BUSINESS = 'rfn_results.business';

    /** the column name for the email field */
    const EMAIL = 'rfn_results.email';

    /** the column name for the phone field */
    const PHONE = 'rfn_results.phone';

    /** the column name for the user field */
    const USER = 'rfn_results.user';

    /** the column name for the user_ip field */
    const USER_IP = 'rfn_results.user_ip';

    /** the column name for the time_taken field */
    const TIME_TAKEN = 'rfn_results.time_taken';

    /** the column name for the time_taken_real field */
    const TIME_TAKEN_REAL = 'rfn_results.time_taken_real';

    /** the column name for the quiz_results field */
    const QUIZ_RESULTS = 'rfn_results.quiz_results';

    /** the column name for the deleted field */
    const DELETED = 'rfn_results.deleted';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of Results objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Results[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. ResultsPeer::$fieldNames[ResultsPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('ResultId', 'QuizId', 'QuizName', 'QuizSystem', 'PointScore', 'CorrectScore', 'Correct', 'Total', 'Name', 'Business', 'Email', 'Phone', 'User', 'UserIp', 'TimeTaken', 'TimeTakenReal', 'QuizResults', 'Deleted', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('resultId', 'quizId', 'quizName', 'quizSystem', 'pointScore', 'correctScore', 'correct', 'total', 'name', 'business', 'email', 'phone', 'user', 'userIp', 'timeTaken', 'timeTakenReal', 'quizResults', 'deleted', ),
        BasePeer::TYPE_COLNAME => array (ResultsPeer::RESULT_ID, ResultsPeer::QUIZ_ID, ResultsPeer::QUIZ_NAME, ResultsPeer::QUIZ_SYSTEM, ResultsPeer::POINT_SCORE, ResultsPeer::CORRECT_SCORE, ResultsPeer::CORRECT, ResultsPeer::TOTAL, ResultsPeer::NAME, ResultsPeer::BUSINESS, ResultsPeer::EMAIL, ResultsPeer::PHONE, ResultsPeer::USER, ResultsPeer::USER_IP, ResultsPeer::TIME_TAKEN, ResultsPeer::TIME_TAKEN_REAL, ResultsPeer::QUIZ_RESULTS, ResultsPeer::DELETED, ),
        BasePeer::TYPE_RAW_COLNAME => array ('RESULT_ID', 'QUIZ_ID', 'QUIZ_NAME', 'QUIZ_SYSTEM', 'POINT_SCORE', 'CORRECT_SCORE', 'CORRECT', 'TOTAL', 'NAME', 'BUSINESS', 'EMAIL', 'PHONE', 'USER', 'USER_IP', 'TIME_TAKEN', 'TIME_TAKEN_REAL', 'QUIZ_RESULTS', 'DELETED', ),
        BasePeer::TYPE_FIELDNAME => array ('result_id', 'quiz_id', 'quiz_name', 'quiz_system', 'point_score', 'correct_score', 'correct', 'total', 'name', 'business', 'email', 'phone', 'user', 'user_ip', 'time_taken', 'time_taken_real', 'quiz_results', 'deleted', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. ResultsPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('ResultId' => 0, 'QuizId' => 1, 'QuizName' => 2, 'QuizSystem' => 3, 'PointScore' => 4, 'CorrectScore' => 5, 'Correct' => 6, 'Total' => 7, 'Name' => 8, 'Business' => 9, 'Email' => 10, 'Phone' => 11, 'User' => 12, 'UserIp' => 13, 'TimeTaken' => 14, 'TimeTakenReal' => 15, 'QuizResults' => 16, 'Deleted' => 17, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('resultId' => 0, 'quizId' => 1, 'quizName' => 2, 'quizSystem' => 3, 'pointScore' => 4, 'correctScore' => 5, 'correct' => 6, 'total' => 7, 'name' => 8, 'business' => 9, 'email' => 10, 'phone' => 11, 'user' => 12, 'userIp' => 13, 'timeTaken' => 14, 'timeTakenReal' => 15, 'quizResults' => 16, 'deleted' => 17, ),
        BasePeer::TYPE_COLNAME => array (ResultsPeer::RESULT_ID => 0, ResultsPeer::QUIZ_ID => 1, ResultsPeer::QUIZ_NAME => 2, ResultsPeer::QUIZ_SYSTEM => 3, ResultsPeer::POINT_SCORE => 4, ResultsPeer::CORRECT_SCORE => 5, ResultsPeer::CORRECT => 6, ResultsPeer::TOTAL => 7, ResultsPeer::NAME => 8, ResultsPeer::BUSINESS => 9, ResultsPeer::EMAIL => 10, ResultsPeer::PHONE => 11, ResultsPeer::USER => 12, ResultsPeer::USER_IP => 13, ResultsPeer::TIME_TAKEN => 14, ResultsPeer::TIME_TAKEN_REAL => 15, ResultsPeer::QUIZ_RESULTS => 16, ResultsPeer::DELETED => 17, ),
        BasePeer::TYPE_RAW_COLNAME => array ('RESULT_ID' => 0, 'QUIZ_ID' => 1, 'QUIZ_NAME' => 2, 'QUIZ_SYSTEM' => 3, 'POINT_SCORE' => 4, 'CORRECT_SCORE' => 5, 'CORRECT' => 6, 'TOTAL' => 7, 'NAME' => 8, 'BUSINESS' => 9, 'EMAIL' => 10, 'PHONE' => 11, 'USER' => 12, 'USER_IP' => 13, 'TIME_TAKEN' => 14, 'TIME_TAKEN_REAL' => 15, 'QUIZ_RESULTS' => 16, 'DELETED' => 17, ),
        BasePeer::TYPE_FIELDNAME => array ('result_id' => 0, 'quiz_id' => 1, 'quiz_name' => 2, 'quiz_system' => 3, 'point_score' => 4, 'correct_score' => 5, 'correct' => 6, 'total' => 7, 'name' => 8, 'business' => 9, 'email' => 10, 'phone' => 11, 'user' => 12, 'user_ip' => 13, 'time_taken' => 14, 'time_taken_real' => 15, 'quiz_results' => 16, 'deleted' => 17, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = ResultsPeer::getFieldNames($toType);
        $key = isset(ResultsPeer::$fieldKeys[$fromType][$name]) ? ResultsPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(ResultsPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, ResultsPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return ResultsPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. ResultsPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(ResultsPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ResultsPeer::RESULT_ID);
            $criteria->addSelectColumn(ResultsPeer::QUIZ_ID);
            $criteria->addSelectColumn(ResultsPeer::QUIZ_NAME);
            $criteria->addSelectColumn(ResultsPeer::QUIZ_SYSTEM);
            $criteria->addSelectColumn(ResultsPeer::POINT_SCORE);
            $criteria->addSelectColumn(ResultsPeer::CORRECT_SCORE);
            $criteria->addSelectColumn(ResultsPeer::CORRECT);
            $criteria->addSelectColumn(ResultsPeer::TOTAL);
            $criteria->addSelectColumn(ResultsPeer::NAME);
            $criteria->addSelectColumn(ResultsPeer::BUSINESS);
            $criteria->addSelectColumn(ResultsPeer::EMAIL);
            $criteria->addSelectColumn(ResultsPeer::PHONE);
            $criteria->addSelectColumn(ResultsPeer::USER);
            $criteria->addSelectColumn(ResultsPeer::USER_IP);
            $criteria->addSelectColumn(ResultsPeer::TIME_TAKEN);
            $criteria->addSelectColumn(ResultsPeer::TIME_TAKEN_REAL);
            $criteria->addSelectColumn(ResultsPeer::QUIZ_RESULTS);
            $criteria->addSelectColumn(ResultsPeer::DELETED);
        } else {
            $criteria->addSelectColumn($alias . '.result_id');
            $criteria->addSelectColumn($alias . '.quiz_id');
            $criteria->addSelectColumn($alias . '.quiz_name');
            $criteria->addSelectColumn($alias . '.quiz_system');
            $criteria->addSelectColumn($alias . '.point_score');
            $criteria->addSelectColumn($alias . '.correct_score');
            $criteria->addSelectColumn($alias . '.correct');
            $criteria->addSelectColumn($alias . '.total');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.business');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.user');
            $criteria->addSelectColumn($alias . '.user_ip');
            $criteria->addSelectColumn($alias . '.time_taken');
            $criteria->addSelectColumn($alias . '.time_taken_real');
            $criteria->addSelectColumn($alias . '.quiz_results');
            $criteria->addSelectColumn($alias . '.deleted');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ResultsPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ResultsPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(ResultsPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return Results
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = ResultsPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return ResultsPeer::populateObjects(ResultsPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            ResultsPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(ResultsPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param Results $obj A Results object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getResultId();
            } // if key === null
            ResultsPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Results object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Results) {
                $key = (string) $value->getResultId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Results object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(ResultsPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return Results Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(ResultsPeer::$instances[$key])) {
                return ResultsPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (ResultsPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        ResultsPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to rfn_results
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = ResultsPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = ResultsPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = ResultsPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ResultsPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Results object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = ResultsPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = ResultsPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + ResultsPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ResultsPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            ResultsPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(ResultsPeer::DATABASE_NAME)->getTable(ResultsPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseResultsPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseResultsPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new \ResultsTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return ResultsPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Results or Criteria object.
     *
     * @param      mixed $values Criteria or Results object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Results object
        }

        if ($criteria->containsKey(ResultsPeer::RESULT_ID) && $criteria->keyContainsValue(ResultsPeer::RESULT_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ResultsPeer::RESULT_ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(ResultsPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Results or Criteria object.
     *
     * @param      mixed $values Criteria or Results object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(ResultsPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(ResultsPeer::RESULT_ID);
            $value = $criteria->remove(ResultsPeer::RESULT_ID);
            if ($value) {
                $selectCriteria->add(ResultsPeer::RESULT_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(ResultsPeer::TABLE_NAME);
            }

        } else { // $values is Results object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(ResultsPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the rfn_results table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(ResultsPeer::TABLE_NAME, $con, ResultsPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ResultsPeer::clearInstancePool();
            ResultsPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Results or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Results object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            ResultsPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Results) { // it's a model object
            // invalidate the cache for this single object
            ResultsPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ResultsPeer::DATABASE_NAME);
            $criteria->add(ResultsPeer::RESULT_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                ResultsPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(ResultsPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            ResultsPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Results object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param Results $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(ResultsPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(ResultsPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(ResultsPeer::DATABASE_NAME, ResultsPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Results
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = ResultsPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(ResultsPeer::DATABASE_NAME);
        $criteria->add(ResultsPeer::RESULT_ID, $pk);

        $v = ResultsPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Results[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(ResultsPeer::DATABASE_NAME);
            $criteria->add(ResultsPeer::RESULT_ID, $pks, Criteria::IN);
            $objs = ResultsPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseResultsPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseResultsPeer::buildTableMap();

