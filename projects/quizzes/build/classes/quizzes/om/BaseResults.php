<?php


/**
 * Base class that represents a row from the 'rfn_results' table.
 *
 *
 *
 * @package    propel.generator.quizzes.om
 */
abstract class BaseResults extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ResultsPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ResultsPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the result_id field.
     * @var        int
     */
    protected $result_id;

    /**
     * The value for the quiz_id field.
     * @var        int
     */
    protected $quiz_id;

    /**
     * The value for the quiz_name field.
     * @var        string
     */
    protected $quiz_name;

    /**
     * The value for the quiz_system field.
     * @var        int
     */
    protected $quiz_system;

    /**
     * The value for the point_score field.
     * @var        int
     */
    protected $point_score;

    /**
     * The value for the correct_score field.
     * @var        int
     */
    protected $correct_score;

    /**
     * The value for the correct field.
     * @var        int
     */
    protected $correct;

    /**
     * The value for the total field.
     * @var        int
     */
    protected $total;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the business field.
     * @var        string
     */
    protected $business;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the phone field.
     * @var        string
     */
    protected $phone;

    /**
     * The value for the user field.
     * @var        int
     */
    protected $user;

    /**
     * The value for the user_ip field.
     * @var        string
     */
    protected $user_ip;

    /**
     * The value for the time_taken field.
     * @var        string
     */
    protected $time_taken;

    /**
     * The value for the time_taken_real field.
     * @var        string
     */
    protected $time_taken_real;

    /**
     * The value for the quiz_results field.
     * @var        string
     */
    protected $quiz_results;

    /**
     * The value for the deleted field.
     * @var        int
     */
    protected $deleted;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [result_id] column value.
     *
     * @return int
     */
    public function getResultId()
    {

        return $this->result_id;
    }

    /**
     * Get the [quiz_id] column value.
     *
     * @return int
     */
    public function getQuizId()
    {

        return $this->quiz_id;
    }

    /**
     * Get the [quiz_name] column value.
     *
     * @return string
     */
    public function getQuizName()
    {

        return $this->quiz_name;
    }

    /**
     * Get the [quiz_system] column value.
     *
     * @return int
     */
    public function getQuizSystem()
    {

        return $this->quiz_system;
    }

    /**
     * Get the [point_score] column value.
     *
     * @return int
     */
    public function getPointScore()
    {

        return $this->point_score;
    }

    /**
     * Get the [correct_score] column value.
     *
     * @return int
     */
    public function getCorrectScore()
    {

        return $this->correct_score;
    }

    /**
     * Get the [correct] column value.
     *
     * @return int
     */
    public function getCorrect()
    {

        return $this->correct;
    }

    /**
     * Get the [total] column value.
     *
     * @return int
     */
    public function getTotal()
    {

        return $this->total;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [business] column value.
     *
     * @return string
     */
    public function getBusiness()
    {

        return $this->business;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [phone] column value.
     *
     * @return string
     */
    public function getPhone()
    {

        return $this->phone;
    }

    /**
     * Get the [user] column value.
     *
     * @return int
     */
    public function getUser()
    {

        return $this->user;
    }

    /**
     * Get the [user_ip] column value.
     *
     * @return string
     */
    public function getUserIp()
    {

        return $this->user_ip;
    }

    /**
     * Get the [time_taken] column value.
     *
     * @return string
     */
    public function getTimeTaken()
    {

        return $this->time_taken;
    }

    /**
     * Get the [optionally formatted] temporal [time_taken_real] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTimeTakenReal($format = 'Y-m-d H:i:s')
    {
        if ($this->time_taken_real === null) {
            return null;
        }

        if ($this->time_taken_real === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->time_taken_real);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->time_taken_real, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [quiz_results] column value.
     *
     * @return string
     */
    public function getQuizResults()
    {

        return $this->quiz_results;
    }

    /**
     * Get the [deleted] column value.
     *
     * @return int
     */
    public function getDeleted()
    {

        return $this->deleted;
    }

    /**
     * Set the value of [result_id] column.
     *
     * @param  int $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setResultId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->result_id !== $v) {
            $this->result_id = $v;
            $this->modifiedColumns[] = ResultsPeer::RESULT_ID;
        }


        return $this;
    } // setResultId()

    /**
     * Set the value of [quiz_id] column.
     *
     * @param  int $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setQuizId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->quiz_id !== $v) {
            $this->quiz_id = $v;
            $this->modifiedColumns[] = ResultsPeer::QUIZ_ID;
        }


        return $this;
    } // setQuizId()

    /**
     * Set the value of [quiz_name] column.
     *
     * @param  string $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setQuizName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->quiz_name !== $v) {
            $this->quiz_name = $v;
            $this->modifiedColumns[] = ResultsPeer::QUIZ_NAME;
        }


        return $this;
    } // setQuizName()

    /**
     * Set the value of [quiz_system] column.
     *
     * @param  int $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setQuizSystem($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->quiz_system !== $v) {
            $this->quiz_system = $v;
            $this->modifiedColumns[] = ResultsPeer::QUIZ_SYSTEM;
        }


        return $this;
    } // setQuizSystem()

    /**
     * Set the value of [point_score] column.
     *
     * @param  int $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setPointScore($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->point_score !== $v) {
            $this->point_score = $v;
            $this->modifiedColumns[] = ResultsPeer::POINT_SCORE;
        }


        return $this;
    } // setPointScore()

    /**
     * Set the value of [correct_score] column.
     *
     * @param  int $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setCorrectScore($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->correct_score !== $v) {
            $this->correct_score = $v;
            $this->modifiedColumns[] = ResultsPeer::CORRECT_SCORE;
        }


        return $this;
    } // setCorrectScore()

    /**
     * Set the value of [correct] column.
     *
     * @param  int $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setCorrect($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->correct !== $v) {
            $this->correct = $v;
            $this->modifiedColumns[] = ResultsPeer::CORRECT;
        }


        return $this;
    } // setCorrect()

    /**
     * Set the value of [total] column.
     *
     * @param  int $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setTotal($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->total !== $v) {
            $this->total = $v;
            $this->modifiedColumns[] = ResultsPeer::TOTAL;
        }


        return $this;
    } // setTotal()

    /**
     * Set the value of [name] column.
     *
     * @param  string $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = ResultsPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [business] column.
     *
     * @param  string $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setBusiness($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->business !== $v) {
            $this->business = $v;
            $this->modifiedColumns[] = ResultsPeer::BUSINESS;
        }


        return $this;
    } // setBusiness()

    /**
     * Set the value of [email] column.
     *
     * @param  string $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = ResultsPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [phone] column.
     *
     * @param  string $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[] = ResultsPeer::PHONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [user] column.
     *
     * @param  int $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setUser($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user !== $v) {
            $this->user = $v;
            $this->modifiedColumns[] = ResultsPeer::USER;
        }


        return $this;
    } // setUser()

    /**
     * Set the value of [user_ip] column.
     *
     * @param  string $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setUserIp($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->user_ip !== $v) {
            $this->user_ip = $v;
            $this->modifiedColumns[] = ResultsPeer::USER_IP;
        }


        return $this;
    } // setUserIp()

    /**
     * Set the value of [time_taken] column.
     *
     * @param  string $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setTimeTaken($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->time_taken !== $v) {
            $this->time_taken = $v;
            $this->modifiedColumns[] = ResultsPeer::TIME_TAKEN;
        }


        return $this;
    } // setTimeTaken()

    /**
     * Sets the value of [time_taken_real] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Results The current object (for fluent API support)
     */
    public function setTimeTakenReal($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->time_taken_real !== null || $dt !== null) {
            $currentDateAsString = ($this->time_taken_real !== null && $tmpDt = new DateTime($this->time_taken_real)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->time_taken_real = $newDateAsString;
                $this->modifiedColumns[] = ResultsPeer::TIME_TAKEN_REAL;
            }
        } // if either are not null


        return $this;
    } // setTimeTakenReal()

    /**
     * Set the value of [quiz_results] column.
     *
     * @param  string $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setQuizResults($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->quiz_results !== $v) {
            $this->quiz_results = $v;
            $this->modifiedColumns[] = ResultsPeer::QUIZ_RESULTS;
        }


        return $this;
    } // setQuizResults()

    /**
     * Set the value of [deleted] column.
     *
     * @param  int $v new value
     * @return Results The current object (for fluent API support)
     */
    public function setDeleted($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->deleted !== $v) {
            $this->deleted = $v;
            $this->modifiedColumns[] = ResultsPeer::DELETED;
        }


        return $this;
    } // setDeleted()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->result_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->quiz_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->quiz_name = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->quiz_system = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->point_score = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->correct_score = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->correct = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->total = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->name = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->business = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->email = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->phone = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->user = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->user_ip = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->time_taken = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->time_taken_real = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->quiz_results = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->deleted = ($row[$startcol + 17] !== null) ? (int) $row[$startcol + 17] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 18; // 18 = ResultsPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Results object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ResultsPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ResultsQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ResultsPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ResultsPeer::RESULT_ID;
        if (null !== $this->result_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ResultsPeer::RESULT_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ResultsPeer::RESULT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`result_id`';
        }
        if ($this->isColumnModified(ResultsPeer::QUIZ_ID)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_id`';
        }
        if ($this->isColumnModified(ResultsPeer::QUIZ_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_name`';
        }
        if ($this->isColumnModified(ResultsPeer::QUIZ_SYSTEM)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_system`';
        }
        if ($this->isColumnModified(ResultsPeer::POINT_SCORE)) {
            $modifiedColumns[':p' . $index++]  = '`point_score`';
        }
        if ($this->isColumnModified(ResultsPeer::CORRECT_SCORE)) {
            $modifiedColumns[':p' . $index++]  = '`correct_score`';
        }
        if ($this->isColumnModified(ResultsPeer::CORRECT)) {
            $modifiedColumns[':p' . $index++]  = '`correct`';
        }
        if ($this->isColumnModified(ResultsPeer::TOTAL)) {
            $modifiedColumns[':p' . $index++]  = '`total`';
        }
        if ($this->isColumnModified(ResultsPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`name`';
        }
        if ($this->isColumnModified(ResultsPeer::BUSINESS)) {
            $modifiedColumns[':p' . $index++]  = '`business`';
        }
        if ($this->isColumnModified(ResultsPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(ResultsPeer::PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`phone`';
        }
        if ($this->isColumnModified(ResultsPeer::USER)) {
            $modifiedColumns[':p' . $index++]  = '`user`';
        }
        if ($this->isColumnModified(ResultsPeer::USER_IP)) {
            $modifiedColumns[':p' . $index++]  = '`user_ip`';
        }
        if ($this->isColumnModified(ResultsPeer::TIME_TAKEN)) {
            $modifiedColumns[':p' . $index++]  = '`time_taken`';
        }
        if ($this->isColumnModified(ResultsPeer::TIME_TAKEN_REAL)) {
            $modifiedColumns[':p' . $index++]  = '`time_taken_real`';
        }
        if ($this->isColumnModified(ResultsPeer::QUIZ_RESULTS)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_results`';
        }
        if ($this->isColumnModified(ResultsPeer::DELETED)) {
            $modifiedColumns[':p' . $index++]  = '`deleted`';
        }

        $sql = sprintf(
            'INSERT INTO `rfn_results` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`result_id`':
                        $stmt->bindValue($identifier, $this->result_id, PDO::PARAM_INT);
                        break;
                    case '`quiz_id`':
                        $stmt->bindValue($identifier, $this->quiz_id, PDO::PARAM_INT);
                        break;
                    case '`quiz_name`':
                        $stmt->bindValue($identifier, $this->quiz_name, PDO::PARAM_STR);
                        break;
                    case '`quiz_system`':
                        $stmt->bindValue($identifier, $this->quiz_system, PDO::PARAM_INT);
                        break;
                    case '`point_score`':
                        $stmt->bindValue($identifier, $this->point_score, PDO::PARAM_INT);
                        break;
                    case '`correct_score`':
                        $stmt->bindValue($identifier, $this->correct_score, PDO::PARAM_INT);
                        break;
                    case '`correct`':
                        $stmt->bindValue($identifier, $this->correct, PDO::PARAM_INT);
                        break;
                    case '`total`':
                        $stmt->bindValue($identifier, $this->total, PDO::PARAM_INT);
                        break;
                    case '`name`':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`business`':
                        $stmt->bindValue($identifier, $this->business, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`phone`':
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case '`user`':
                        $stmt->bindValue($identifier, $this->user, PDO::PARAM_INT);
                        break;
                    case '`user_ip`':
                        $stmt->bindValue($identifier, $this->user_ip, PDO::PARAM_STR);
                        break;
                    case '`time_taken`':
                        $stmt->bindValue($identifier, $this->time_taken, PDO::PARAM_STR);
                        break;
                    case '`time_taken_real`':
                        $stmt->bindValue($identifier, $this->time_taken_real, PDO::PARAM_STR);
                        break;
                    case '`quiz_results`':
                        $stmt->bindValue($identifier, $this->quiz_results, PDO::PARAM_STR);
                        break;
                    case '`deleted`':
                        $stmt->bindValue($identifier, $this->deleted, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setResultId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = ResultsPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ResultsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getResultId();
                break;
            case 1:
                return $this->getQuizId();
                break;
            case 2:
                return $this->getQuizName();
                break;
            case 3:
                return $this->getQuizSystem();
                break;
            case 4:
                return $this->getPointScore();
                break;
            case 5:
                return $this->getCorrectScore();
                break;
            case 6:
                return $this->getCorrect();
                break;
            case 7:
                return $this->getTotal();
                break;
            case 8:
                return $this->getName();
                break;
            case 9:
                return $this->getBusiness();
                break;
            case 10:
                return $this->getEmail();
                break;
            case 11:
                return $this->getPhone();
                break;
            case 12:
                return $this->getUser();
                break;
            case 13:
                return $this->getUserIp();
                break;
            case 14:
                return $this->getTimeTaken();
                break;
            case 15:
                return $this->getTimeTakenReal();
                break;
            case 16:
                return $this->getQuizResults();
                break;
            case 17:
                return $this->getDeleted();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['Results'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Results'][$this->getPrimaryKey()] = true;
        $keys = ResultsPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getResultId(),
            $keys[1] => $this->getQuizId(),
            $keys[2] => $this->getQuizName(),
            $keys[3] => $this->getQuizSystem(),
            $keys[4] => $this->getPointScore(),
            $keys[5] => $this->getCorrectScore(),
            $keys[6] => $this->getCorrect(),
            $keys[7] => $this->getTotal(),
            $keys[8] => $this->getName(),
            $keys[9] => $this->getBusiness(),
            $keys[10] => $this->getEmail(),
            $keys[11] => $this->getPhone(),
            $keys[12] => $this->getUser(),
            $keys[13] => $this->getUserIp(),
            $keys[14] => $this->getTimeTaken(),
            $keys[15] => $this->getTimeTakenReal(),
            $keys[16] => $this->getQuizResults(),
            $keys[17] => $this->getDeleted(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ResultsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setResultId($value);
                break;
            case 1:
                $this->setQuizId($value);
                break;
            case 2:
                $this->setQuizName($value);
                break;
            case 3:
                $this->setQuizSystem($value);
                break;
            case 4:
                $this->setPointScore($value);
                break;
            case 5:
                $this->setCorrectScore($value);
                break;
            case 6:
                $this->setCorrect($value);
                break;
            case 7:
                $this->setTotal($value);
                break;
            case 8:
                $this->setName($value);
                break;
            case 9:
                $this->setBusiness($value);
                break;
            case 10:
                $this->setEmail($value);
                break;
            case 11:
                $this->setPhone($value);
                break;
            case 12:
                $this->setUser($value);
                break;
            case 13:
                $this->setUserIp($value);
                break;
            case 14:
                $this->setTimeTaken($value);
                break;
            case 15:
                $this->setTimeTakenReal($value);
                break;
            case 16:
                $this->setQuizResults($value);
                break;
            case 17:
                $this->setDeleted($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ResultsPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setResultId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setQuizId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setQuizName($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setQuizSystem($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPointScore($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setCorrectScore($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCorrect($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setTotal($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setName($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setBusiness($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setEmail($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setPhone($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setUser($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setUserIp($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setTimeTaken($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setTimeTakenReal($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setQuizResults($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setDeleted($arr[$keys[17]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ResultsPeer::DATABASE_NAME);

        if ($this->isColumnModified(ResultsPeer::RESULT_ID)) $criteria->add(ResultsPeer::RESULT_ID, $this->result_id);
        if ($this->isColumnModified(ResultsPeer::QUIZ_ID)) $criteria->add(ResultsPeer::QUIZ_ID, $this->quiz_id);
        if ($this->isColumnModified(ResultsPeer::QUIZ_NAME)) $criteria->add(ResultsPeer::QUIZ_NAME, $this->quiz_name);
        if ($this->isColumnModified(ResultsPeer::QUIZ_SYSTEM)) $criteria->add(ResultsPeer::QUIZ_SYSTEM, $this->quiz_system);
        if ($this->isColumnModified(ResultsPeer::POINT_SCORE)) $criteria->add(ResultsPeer::POINT_SCORE, $this->point_score);
        if ($this->isColumnModified(ResultsPeer::CORRECT_SCORE)) $criteria->add(ResultsPeer::CORRECT_SCORE, $this->correct_score);
        if ($this->isColumnModified(ResultsPeer::CORRECT)) $criteria->add(ResultsPeer::CORRECT, $this->correct);
        if ($this->isColumnModified(ResultsPeer::TOTAL)) $criteria->add(ResultsPeer::TOTAL, $this->total);
        if ($this->isColumnModified(ResultsPeer::NAME)) $criteria->add(ResultsPeer::NAME, $this->name);
        if ($this->isColumnModified(ResultsPeer::BUSINESS)) $criteria->add(ResultsPeer::BUSINESS, $this->business);
        if ($this->isColumnModified(ResultsPeer::EMAIL)) $criteria->add(ResultsPeer::EMAIL, $this->email);
        if ($this->isColumnModified(ResultsPeer::PHONE)) $criteria->add(ResultsPeer::PHONE, $this->phone);
        if ($this->isColumnModified(ResultsPeer::USER)) $criteria->add(ResultsPeer::USER, $this->user);
        if ($this->isColumnModified(ResultsPeer::USER_IP)) $criteria->add(ResultsPeer::USER_IP, $this->user_ip);
        if ($this->isColumnModified(ResultsPeer::TIME_TAKEN)) $criteria->add(ResultsPeer::TIME_TAKEN, $this->time_taken);
        if ($this->isColumnModified(ResultsPeer::TIME_TAKEN_REAL)) $criteria->add(ResultsPeer::TIME_TAKEN_REAL, $this->time_taken_real);
        if ($this->isColumnModified(ResultsPeer::QUIZ_RESULTS)) $criteria->add(ResultsPeer::QUIZ_RESULTS, $this->quiz_results);
        if ($this->isColumnModified(ResultsPeer::DELETED)) $criteria->add(ResultsPeer::DELETED, $this->deleted);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ResultsPeer::DATABASE_NAME);
        $criteria->add(ResultsPeer::RESULT_ID, $this->result_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getResultId();
    }

    /**
     * Generic method to set the primary key (result_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setResultId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getResultId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Results (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setQuizId($this->getQuizId());
        $copyObj->setQuizName($this->getQuizName());
        $copyObj->setQuizSystem($this->getQuizSystem());
        $copyObj->setPointScore($this->getPointScore());
        $copyObj->setCorrectScore($this->getCorrectScore());
        $copyObj->setCorrect($this->getCorrect());
        $copyObj->setTotal($this->getTotal());
        $copyObj->setName($this->getName());
        $copyObj->setBusiness($this->getBusiness());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setUser($this->getUser());
        $copyObj->setUserIp($this->getUserIp());
        $copyObj->setTimeTaken($this->getTimeTaken());
        $copyObj->setTimeTakenReal($this->getTimeTakenReal());
        $copyObj->setQuizResults($this->getQuizResults());
        $copyObj->setDeleted($this->getDeleted());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setResultId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Results Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ResultsPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ResultsPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->result_id = null;
        $this->quiz_id = null;
        $this->quiz_name = null;
        $this->quiz_system = null;
        $this->point_score = null;
        $this->correct_score = null;
        $this->correct = null;
        $this->total = null;
        $this->name = null;
        $this->business = null;
        $this->email = null;
        $this->phone = null;
        $this->user = null;
        $this->user_ip = null;
        $this->time_taken = null;
        $this->time_taken_real = null;
        $this->quiz_results = null;
        $this->deleted = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ResultsPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
