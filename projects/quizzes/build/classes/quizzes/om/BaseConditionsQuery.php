<?php


/**
 * Base class that represents a query for the 'rfn_conditions' table.
 *
 *
 *
 * @method ConditionsQuery orderByQuestionId($order = Criteria::ASC) Order by the question_id column
 * @method ConditionsQuery orderByQuizId($order = Criteria::ASC) Order by the quiz_id column
 * @method ConditionsQuery orderByConditionArray($order = Criteria::ASC) Order by the condition_array column
 * @method ConditionsQuery orderByDeleted($order = Criteria::ASC) Order by the deleted column
 *
 * @method ConditionsQuery groupByQuestionId() Group by the question_id column
 * @method ConditionsQuery groupByQuizId() Group by the quiz_id column
 * @method ConditionsQuery groupByConditionArray() Group by the condition_array column
 * @method ConditionsQuery groupByDeleted() Group by the deleted column
 *
 * @method ConditionsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ConditionsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ConditionsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Conditions findOne(PropelPDO $con = null) Return the first Conditions matching the query
 * @method Conditions findOneOrCreate(PropelPDO $con = null) Return the first Conditions matching the query, or a new Conditions object populated from the query conditions when no match is found
 *
 * @method Conditions findOneByQuizId(int $quiz_id) Return the first Conditions filtered by the quiz_id column
 * @method Conditions findOneByConditionArray(string $condition_array) Return the first Conditions filtered by the condition_array column
 * @method Conditions findOneByDeleted(int $deleted) Return the first Conditions filtered by the deleted column
 *
 * @method array findByQuestionId(int $question_id) Return Conditions objects filtered by the question_id column
 * @method array findByQuizId(int $quiz_id) Return Conditions objects filtered by the quiz_id column
 * @method array findByConditionArray(string $condition_array) Return Conditions objects filtered by the condition_array column
 * @method array findByDeleted(int $deleted) Return Conditions objects filtered by the deleted column
 *
 * @package    propel.generator.quizzes.om
 */
abstract class BaseConditionsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseConditionsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'rfn_new2';
        }
        if (null === $modelName) {
            $modelName = 'Conditions';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ConditionsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ConditionsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ConditionsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ConditionsQuery) {
            return $criteria;
        }
        $query = new ConditionsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Conditions|Conditions[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ConditionsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ConditionsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Conditions A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByQuestionId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Conditions A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `question_id`, `quiz_id`, `condition_array`, `deleted` FROM `rfn_conditions` WHERE `question_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Conditions();
            $obj->hydrate($row);
            ConditionsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Conditions|Conditions[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Conditions[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ConditionsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ConditionsPeer::QUESTION_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ConditionsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ConditionsPeer::QUESTION_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the question_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionId(1234); // WHERE question_id = 1234
     * $query->filterByQuestionId(array(12, 34)); // WHERE question_id IN (12, 34)
     * $query->filterByQuestionId(array('min' => 12)); // WHERE question_id >= 12
     * $query->filterByQuestionId(array('max' => 12)); // WHERE question_id <= 12
     * </code>
     *
     * @param     mixed $questionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConditionsQuery The current query, for fluid interface
     */
    public function filterByQuestionId($questionId = null, $comparison = null)
    {
        if (is_array($questionId)) {
            $useMinMax = false;
            if (isset($questionId['min'])) {
                $this->addUsingAlias(ConditionsPeer::QUESTION_ID, $questionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionId['max'])) {
                $this->addUsingAlias(ConditionsPeer::QUESTION_ID, $questionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConditionsPeer::QUESTION_ID, $questionId, $comparison);
    }

    /**
     * Filter the query on the quiz_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizId(1234); // WHERE quiz_id = 1234
     * $query->filterByQuizId(array(12, 34)); // WHERE quiz_id IN (12, 34)
     * $query->filterByQuizId(array('min' => 12)); // WHERE quiz_id >= 12
     * $query->filterByQuizId(array('max' => 12)); // WHERE quiz_id <= 12
     * </code>
     *
     * @param     mixed $quizId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConditionsQuery The current query, for fluid interface
     */
    public function filterByQuizId($quizId = null, $comparison = null)
    {
        if (is_array($quizId)) {
            $useMinMax = false;
            if (isset($quizId['min'])) {
                $this->addUsingAlias(ConditionsPeer::QUIZ_ID, $quizId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quizId['max'])) {
                $this->addUsingAlias(ConditionsPeer::QUIZ_ID, $quizId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConditionsPeer::QUIZ_ID, $quizId, $comparison);
    }

    /**
     * Filter the query on the condition_array column
     *
     * Example usage:
     * <code>
     * $query->filterByConditionArray('fooValue');   // WHERE condition_array = 'fooValue'
     * $query->filterByConditionArray('%fooValue%'); // WHERE condition_array LIKE '%fooValue%'
     * </code>
     *
     * @param     string $conditionArray The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConditionsQuery The current query, for fluid interface
     */
    public function filterByConditionArray($conditionArray = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($conditionArray)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $conditionArray)) {
                $conditionArray = str_replace('*', '%', $conditionArray);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ConditionsPeer::CONDITION_ARRAY, $conditionArray, $comparison);
    }

    /**
     * Filter the query on the deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByDeleted(1234); // WHERE deleted = 1234
     * $query->filterByDeleted(array(12, 34)); // WHERE deleted IN (12, 34)
     * $query->filterByDeleted(array('min' => 12)); // WHERE deleted >= 12
     * $query->filterByDeleted(array('max' => 12)); // WHERE deleted <= 12
     * </code>
     *
     * @param     mixed $deleted The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConditionsQuery The current query, for fluid interface
     */
    public function filterByDeleted($deleted = null, $comparison = null)
    {
        if (is_array($deleted)) {
            $useMinMax = false;
            if (isset($deleted['min'])) {
                $this->addUsingAlias(ConditionsPeer::DELETED, $deleted['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deleted['max'])) {
                $this->addUsingAlias(ConditionsPeer::DELETED, $deleted['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConditionsPeer::DELETED, $deleted, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Conditions $conditions Object to remove from the list of results
     *
     * @return ConditionsQuery The current query, for fluid interface
     */
    public function prune($conditions = null)
    {
        if ($conditions) {
            $this->addUsingAlias(ConditionsPeer::QUESTION_ID, $conditions->getQuestionId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
