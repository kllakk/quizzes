<?php


/**
 * Base class that represents a row from the 'rfn_quizzes' table.
 *
 *
 *
 * @package    propel.generator.quizzes.om
 */
abstract class BaseQuizzes extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'QuizzesPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        QuizzesPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the quiz_id field.
     * @var        int
     */
    protected $quiz_id;

    /**
     * The value for the quiz_name field.
     * @var        string
     */
    protected $quiz_name;

    /**
     * The value for the message_before field.
     * @var        string
     */
    protected $message_before;

    /**
     * The value for the message_after field.
     * @var        string
     */
    protected $message_after;

    /**
     * The value for the message_comment field.
     * @var        string
     */
    protected $message_comment;

    /**
     * The value for the message_end_template field.
     * @var        string
     */
    protected $message_end_template;

    /**
     * The value for the user_email_template field.
     * @var        string
     */
    protected $user_email_template;

    /**
     * The value for the admin_email_template field.
     * @var        string
     */
    protected $admin_email_template;

    /**
     * The value for the submit_button_text field.
     * @var        string
     */
    protected $submit_button_text;

    /**
     * The value for the name_field_text field.
     * @var        string
     */
    protected $name_field_text;

    /**
     * The value for the business_field_text field.
     * @var        string
     */
    protected $business_field_text;

    /**
     * The value for the email_field_text field.
     * @var        string
     */
    protected $email_field_text;

    /**
     * The value for the phone_field_text field.
     * @var        string
     */
    protected $phone_field_text;

    /**
     * The value for the comment_field_text field.
     * @var        string
     */
    protected $comment_field_text;

    /**
     * The value for the email_from_text field.
     * @var        string
     */
    protected $email_from_text;

    /**
     * The value for the question_answer_template field.
     * @var        string
     */
    protected $question_answer_template;

    /**
     * The value for the leaderboard_template field.
     * @var        string
     */
    protected $leaderboard_template;

    /**
     * The value for the system field.
     * @var        int
     */
    protected $system;

    /**
     * The value for the randomness_order field.
     * @var        int
     */
    protected $randomness_order;

    /**
     * The value for the loggedin_user_contact field.
     * @var        int
     */
    protected $loggedin_user_contact;

    /**
     * The value for the show_score field.
     * @var        int
     */
    protected $show_score;

    /**
     * The value for the send_user_email field.
     * @var        int
     */
    protected $send_user_email;

    /**
     * The value for the send_admin_email field.
     * @var        int
     */
    protected $send_admin_email;

    /**
     * The value for the contact_info_location field.
     * @var        int
     */
    protected $contact_info_location;

    /**
     * The value for the user_name field.
     * @var        int
     */
    protected $user_name;

    /**
     * The value for the user_comp field.
     * @var        int
     */
    protected $user_comp;

    /**
     * The value for the user_email field.
     * @var        int
     */
    protected $user_email;

    /**
     * The value for the user_phone field.
     * @var        int
     */
    protected $user_phone;

    /**
     * The value for the admin_email field.
     * @var        string
     */
    protected $admin_email;

    /**
     * The value for the comment_section field.
     * @var        int
     */
    protected $comment_section;

    /**
     * The value for the question_from_total field.
     * @var        int
     */
    protected $question_from_total;

    /**
     * The value for the total_user_tries field.
     * @var        int
     */
    protected $total_user_tries;

    /**
     * The value for the total_user_tries_text field.
     * @var        string
     */
    protected $total_user_tries_text;

    /**
     * The value for the certificate_template field.
     * @var        string
     */
    protected $certificate_template;

    /**
     * The value for the social_media field.
     * @var        int
     */
    protected $social_media;

    /**
     * The value for the social_media_text field.
     * @var        string
     */
    protected $social_media_text;

    /**
     * The value for the pagination field.
     * @var        int
     */
    protected $pagination;

    /**
     * The value for the pagination_text field.
     * @var        string
     */
    protected $pagination_text;

    /**
     * The value for the timer_limit field.
     * @var        int
     */
    protected $timer_limit;

    /**
     * The value for the quiz_stye field.
     * @var        string
     */
    protected $quiz_stye;

    /**
     * The value for the question_numbering field.
     * @var        int
     */
    protected $question_numbering;

    /**
     * The value for the quiz_settings field.
     * @var        string
     */
    protected $quiz_settings;

    /**
     * The value for the theme_selected field.
     * @var        string
     */
    protected $theme_selected;

    /**
     * The value for the last_activity field.
     * @var        string
     */
    protected $last_activity;

    /**
     * The value for the require_log_in field.
     * @var        int
     */
    protected $require_log_in;

    /**
     * The value for the require_log_in_text field.
     * @var        string
     */
    protected $require_log_in_text;

    /**
     * The value for the limit_total_entries field.
     * @var        int
     */
    protected $limit_total_entries;

    /**
     * The value for the limit_total_entries_text field.
     * @var        string
     */
    protected $limit_total_entries_text;

    /**
     * The value for the scheduled_timeframe field.
     * @var        string
     */
    protected $scheduled_timeframe;

    /**
     * The value for the scheduled_timeframe_text field.
     * @var        string
     */
    protected $scheduled_timeframe_text;

    /**
     * The value for the disable_answer_onselect field.
     * @var        int
     */
    protected $disable_answer_onselect;

    /**
     * The value for the ajax_show_correct field.
     * @var        int
     */
    protected $ajax_show_correct;

    /**
     * The value for the quiz_views field.
     * @var        int
     */
    protected $quiz_views;

    /**
     * The value for the quiz_taken field.
     * @var        int
     */
    protected $quiz_taken;

    /**
     * The value for the deleted field.
     * @var        int
     */
    protected $deleted;

    /**
     * The value for the quiz_description field.
     * @var        string
     */
    protected $quiz_description;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [quiz_id] column value.
     *
     * @return int
     */
    public function getQuizId()
    {

        return $this->quiz_id;
    }

    /**
     * Get the [quiz_name] column value.
     *
     * @return string
     */
    public function getQuizName()
    {

        return $this->quiz_name;
    }

    /**
     * Get the [message_before] column value.
     *
     * @return string
     */
    public function getMessageBefore()
    {

        return $this->message_before;
    }

    /**
     * Get the [message_after] column value.
     *
     * @return string
     */
    public function getMessageAfter()
    {

        return $this->message_after;
    }

    /**
     * Get the [message_comment] column value.
     *
     * @return string
     */
    public function getMessageComment()
    {

        return $this->message_comment;
    }

    /**
     * Get the [message_end_template] column value.
     *
     * @return string
     */
    public function getMessageEndTemplate()
    {

        return $this->message_end_template;
    }

    /**
     * Get the [user_email_template] column value.
     *
     * @return string
     */
    public function getUserEmailTemplate()
    {

        return $this->user_email_template;
    }

    /**
     * Get the [admin_email_template] column value.
     *
     * @return string
     */
    public function getAdminEmailTemplate()
    {

        return $this->admin_email_template;
    }

    /**
     * Get the [submit_button_text] column value.
     *
     * @return string
     */
    public function getSubmitButtonText()
    {

        return $this->submit_button_text;
    }

    /**
     * Get the [name_field_text] column value.
     *
     * @return string
     */
    public function getNameFieldText()
    {

        return $this->name_field_text;
    }

    /**
     * Get the [business_field_text] column value.
     *
     * @return string
     */
    public function getBusinessFieldText()
    {

        return $this->business_field_text;
    }

    /**
     * Get the [email_field_text] column value.
     *
     * @return string
     */
    public function getEmailFieldText()
    {

        return $this->email_field_text;
    }

    /**
     * Get the [phone_field_text] column value.
     *
     * @return string
     */
    public function getPhoneFieldText()
    {

        return $this->phone_field_text;
    }

    /**
     * Get the [comment_field_text] column value.
     *
     * @return string
     */
    public function getCommentFieldText()
    {

        return $this->comment_field_text;
    }

    /**
     * Get the [email_from_text] column value.
     *
     * @return string
     */
    public function getEmailFromText()
    {

        return $this->email_from_text;
    }

    /**
     * Get the [question_answer_template] column value.
     *
     * @return string
     */
    public function getQuestionAnswerTemplate()
    {

        return $this->question_answer_template;
    }

    /**
     * Get the [leaderboard_template] column value.
     *
     * @return string
     */
    public function getLeaderboardTemplate()
    {

        return $this->leaderboard_template;
    }

    /**
     * Get the [system] column value.
     *
     * @return int
     */
    public function getSystem()
    {

        return $this->system;
    }

    /**
     * Get the [randomness_order] column value.
     *
     * @return int
     */
    public function getRandomnessOrder()
    {

        return $this->randomness_order;
    }

    /**
     * Get the [loggedin_user_contact] column value.
     *
     * @return int
     */
    public function getLoggedinUserContact()
    {

        return $this->loggedin_user_contact;
    }

    /**
     * Get the [show_score] column value.
     *
     * @return int
     */
    public function getShowScore()
    {

        return $this->show_score;
    }

    /**
     * Get the [send_user_email] column value.
     *
     * @return int
     */
    public function getSendUserEmail()
    {

        return $this->send_user_email;
    }

    /**
     * Get the [send_admin_email] column value.
     *
     * @return int
     */
    public function getSendAdminEmail()
    {

        return $this->send_admin_email;
    }

    /**
     * Get the [contact_info_location] column value.
     *
     * @return int
     */
    public function getContactInfoLocation()
    {

        return $this->contact_info_location;
    }

    /**
     * Get the [user_name] column value.
     *
     * @return int
     */
    public function getUserName()
    {

        return $this->user_name;
    }

    /**
     * Get the [user_comp] column value.
     *
     * @return int
     */
    public function getUserComp()
    {

        return $this->user_comp;
    }

    /**
     * Get the [user_email] column value.
     *
     * @return int
     */
    public function getUserEmail()
    {

        return $this->user_email;
    }

    /**
     * Get the [user_phone] column value.
     *
     * @return int
     */
    public function getUserPhone()
    {

        return $this->user_phone;
    }

    /**
     * Get the [admin_email] column value.
     *
     * @return string
     */
    public function getAdminEmail()
    {

        return $this->admin_email;
    }

    /**
     * Get the [comment_section] column value.
     *
     * @return int
     */
    public function getCommentSection()
    {

        return $this->comment_section;
    }

    /**
     * Get the [question_from_total] column value.
     *
     * @return int
     */
    public function getQuestionFromTotal()
    {

        return $this->question_from_total;
    }

    /**
     * Get the [total_user_tries] column value.
     *
     * @return int
     */
    public function getTotalUserTries()
    {

        return $this->total_user_tries;
    }

    /**
     * Get the [total_user_tries_text] column value.
     *
     * @return string
     */
    public function getTotalUserTriesText()
    {

        return $this->total_user_tries_text;
    }

    /**
     * Get the [certificate_template] column value.
     *
     * @return string
     */
    public function getCertificateTemplate()
    {

        return $this->certificate_template;
    }

    /**
     * Get the [social_media] column value.
     *
     * @return int
     */
    public function getSocialMedia()
    {

        return $this->social_media;
    }

    /**
     * Get the [social_media_text] column value.
     *
     * @return string
     */
    public function getSocialMediaText()
    {

        return $this->social_media_text;
    }

    /**
     * Get the [pagination] column value.
     *
     * @return int
     */
    public function getPagination()
    {

        return $this->pagination;
    }

    /**
     * Get the [pagination_text] column value.
     *
     * @return string
     */
    public function getPaginationText()
    {

        return $this->pagination_text;
    }

    /**
     * Get the [timer_limit] column value.
     *
     * @return int
     */
    public function getTimerLimit()
    {

        return $this->timer_limit;
    }

    /**
     * Get the [quiz_stye] column value.
     *
     * @return string
     */
    public function getQuizStye()
    {

        return $this->quiz_stye;
    }

    /**
     * Get the [question_numbering] column value.
     *
     * @return int
     */
    public function getQuestionNumbering()
    {

        return $this->question_numbering;
    }

    /**
     * Get the [quiz_settings] column value.
     *
     * @return string
     */
    public function getQuizSettings()
    {

        return $this->quiz_settings;
    }

    /**
     * Get the [theme_selected] column value.
     *
     * @return string
     */
    public function getThemeSelected()
    {

        return $this->theme_selected;
    }

    /**
     * Get the [optionally formatted] temporal [last_activity] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastActivity($format = 'Y-m-d H:i:s')
    {
        if ($this->last_activity === null) {
            return null;
        }

        if ($this->last_activity === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->last_activity);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_activity, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [require_log_in] column value.
     *
     * @return int
     */
    public function getRequireLogIn()
    {

        return $this->require_log_in;
    }

    /**
     * Get the [require_log_in_text] column value.
     *
     * @return string
     */
    public function getRequireLogInText()
    {

        return $this->require_log_in_text;
    }

    /**
     * Get the [limit_total_entries] column value.
     *
     * @return int
     */
    public function getLimitTotalEntries()
    {

        return $this->limit_total_entries;
    }

    /**
     * Get the [limit_total_entries_text] column value.
     *
     * @return string
     */
    public function getLimitTotalEntriesText()
    {

        return $this->limit_total_entries_text;
    }

    /**
     * Get the [scheduled_timeframe] column value.
     *
     * @return string
     */
    public function getScheduledTimeframe()
    {

        return $this->scheduled_timeframe;
    }

    /**
     * Get the [scheduled_timeframe_text] column value.
     *
     * @return string
     */
    public function getScheduledTimeframeText()
    {

        return $this->scheduled_timeframe_text;
    }

    /**
     * Get the [disable_answer_onselect] column value.
     *
     * @return int
     */
    public function getDisableAnswerOnselect()
    {

        return $this->disable_answer_onselect;
    }

    /**
     * Get the [ajax_show_correct] column value.
     *
     * @return int
     */
    public function getAjaxShowCorrect()
    {

        return $this->ajax_show_correct;
    }

    /**
     * Get the [quiz_views] column value.
     *
     * @return int
     */
    public function getQuizViews()
    {

        return $this->quiz_views;
    }

    /**
     * Get the [quiz_taken] column value.
     *
     * @return int
     */
    public function getQuizTaken()
    {

        return $this->quiz_taken;
    }

    /**
     * Get the [deleted] column value.
     *
     * @return int
     */
    public function getDeleted()
    {

        return $this->deleted;
    }

    /**
     * Get the [quiz_description] column value.
     *
     * @return string
     */
    public function getQuizDescription()
    {

        return $this->quiz_description;
    }

    /**
     * Set the value of [quiz_id] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setQuizId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->quiz_id !== $v) {
            $this->quiz_id = $v;
            $this->modifiedColumns[] = QuizzesPeer::QUIZ_ID;
        }


        return $this;
    } // setQuizId()

    /**
     * Set the value of [quiz_name] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setQuizName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->quiz_name !== $v) {
            $this->quiz_name = $v;
            $this->modifiedColumns[] = QuizzesPeer::QUIZ_NAME;
        }


        return $this;
    } // setQuizName()

    /**
     * Set the value of [message_before] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setMessageBefore($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->message_before !== $v) {
            $this->message_before = $v;
            $this->modifiedColumns[] = QuizzesPeer::MESSAGE_BEFORE;
        }


        return $this;
    } // setMessageBefore()

    /**
     * Set the value of [message_after] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setMessageAfter($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->message_after !== $v) {
            $this->message_after = $v;
            $this->modifiedColumns[] = QuizzesPeer::MESSAGE_AFTER;
        }


        return $this;
    } // setMessageAfter()

    /**
     * Set the value of [message_comment] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setMessageComment($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->message_comment !== $v) {
            $this->message_comment = $v;
            $this->modifiedColumns[] = QuizzesPeer::MESSAGE_COMMENT;
        }


        return $this;
    } // setMessageComment()

    /**
     * Set the value of [message_end_template] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setMessageEndTemplate($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->message_end_template !== $v) {
            $this->message_end_template = $v;
            $this->modifiedColumns[] = QuizzesPeer::MESSAGE_END_TEMPLATE;
        }


        return $this;
    } // setMessageEndTemplate()

    /**
     * Set the value of [user_email_template] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setUserEmailTemplate($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->user_email_template !== $v) {
            $this->user_email_template = $v;
            $this->modifiedColumns[] = QuizzesPeer::USER_EMAIL_TEMPLATE;
        }


        return $this;
    } // setUserEmailTemplate()

    /**
     * Set the value of [admin_email_template] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setAdminEmailTemplate($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->admin_email_template !== $v) {
            $this->admin_email_template = $v;
            $this->modifiedColumns[] = QuizzesPeer::ADMIN_EMAIL_TEMPLATE;
        }


        return $this;
    } // setAdminEmailTemplate()

    /**
     * Set the value of [submit_button_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setSubmitButtonText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->submit_button_text !== $v) {
            $this->submit_button_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::SUBMIT_BUTTON_TEXT;
        }


        return $this;
    } // setSubmitButtonText()

    /**
     * Set the value of [name_field_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setNameFieldText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name_field_text !== $v) {
            $this->name_field_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::NAME_FIELD_TEXT;
        }


        return $this;
    } // setNameFieldText()

    /**
     * Set the value of [business_field_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setBusinessFieldText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->business_field_text !== $v) {
            $this->business_field_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::BUSINESS_FIELD_TEXT;
        }


        return $this;
    } // setBusinessFieldText()

    /**
     * Set the value of [email_field_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setEmailFieldText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email_field_text !== $v) {
            $this->email_field_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::EMAIL_FIELD_TEXT;
        }


        return $this;
    } // setEmailFieldText()

    /**
     * Set the value of [phone_field_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setPhoneFieldText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone_field_text !== $v) {
            $this->phone_field_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::PHONE_FIELD_TEXT;
        }


        return $this;
    } // setPhoneFieldText()

    /**
     * Set the value of [comment_field_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setCommentFieldText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->comment_field_text !== $v) {
            $this->comment_field_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::COMMENT_FIELD_TEXT;
        }


        return $this;
    } // setCommentFieldText()

    /**
     * Set the value of [email_from_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setEmailFromText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email_from_text !== $v) {
            $this->email_from_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::EMAIL_FROM_TEXT;
        }


        return $this;
    } // setEmailFromText()

    /**
     * Set the value of [question_answer_template] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setQuestionAnswerTemplate($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->question_answer_template !== $v) {
            $this->question_answer_template = $v;
            $this->modifiedColumns[] = QuizzesPeer::QUESTION_ANSWER_TEMPLATE;
        }


        return $this;
    } // setQuestionAnswerTemplate()

    /**
     * Set the value of [leaderboard_template] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setLeaderboardTemplate($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->leaderboard_template !== $v) {
            $this->leaderboard_template = $v;
            $this->modifiedColumns[] = QuizzesPeer::LEADERBOARD_TEMPLATE;
        }


        return $this;
    } // setLeaderboardTemplate()

    /**
     * Set the value of [system] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setSystem($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->system !== $v) {
            $this->system = $v;
            $this->modifiedColumns[] = QuizzesPeer::SYSTEM;
        }


        return $this;
    } // setSystem()

    /**
     * Set the value of [randomness_order] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setRandomnessOrder($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->randomness_order !== $v) {
            $this->randomness_order = $v;
            $this->modifiedColumns[] = QuizzesPeer::RANDOMNESS_ORDER;
        }


        return $this;
    } // setRandomnessOrder()

    /**
     * Set the value of [loggedin_user_contact] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setLoggedinUserContact($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->loggedin_user_contact !== $v) {
            $this->loggedin_user_contact = $v;
            $this->modifiedColumns[] = QuizzesPeer::LOGGEDIN_USER_CONTACT;
        }


        return $this;
    } // setLoggedinUserContact()

    /**
     * Set the value of [show_score] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setShowScore($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->show_score !== $v) {
            $this->show_score = $v;
            $this->modifiedColumns[] = QuizzesPeer::SHOW_SCORE;
        }


        return $this;
    } // setShowScore()

    /**
     * Set the value of [send_user_email] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setSendUserEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->send_user_email !== $v) {
            $this->send_user_email = $v;
            $this->modifiedColumns[] = QuizzesPeer::SEND_USER_EMAIL;
        }


        return $this;
    } // setSendUserEmail()

    /**
     * Set the value of [send_admin_email] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setSendAdminEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->send_admin_email !== $v) {
            $this->send_admin_email = $v;
            $this->modifiedColumns[] = QuizzesPeer::SEND_ADMIN_EMAIL;
        }


        return $this;
    } // setSendAdminEmail()

    /**
     * Set the value of [contact_info_location] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setContactInfoLocation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->contact_info_location !== $v) {
            $this->contact_info_location = $v;
            $this->modifiedColumns[] = QuizzesPeer::CONTACT_INFO_LOCATION;
        }


        return $this;
    } // setContactInfoLocation()

    /**
     * Set the value of [user_name] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setUserName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user_name !== $v) {
            $this->user_name = $v;
            $this->modifiedColumns[] = QuizzesPeer::USER_NAME;
        }


        return $this;
    } // setUserName()

    /**
     * Set the value of [user_comp] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setUserComp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user_comp !== $v) {
            $this->user_comp = $v;
            $this->modifiedColumns[] = QuizzesPeer::USER_COMP;
        }


        return $this;
    } // setUserComp()

    /**
     * Set the value of [user_email] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setUserEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user_email !== $v) {
            $this->user_email = $v;
            $this->modifiedColumns[] = QuizzesPeer::USER_EMAIL;
        }


        return $this;
    } // setUserEmail()

    /**
     * Set the value of [user_phone] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setUserPhone($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user_phone !== $v) {
            $this->user_phone = $v;
            $this->modifiedColumns[] = QuizzesPeer::USER_PHONE;
        }


        return $this;
    } // setUserPhone()

    /**
     * Set the value of [admin_email] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setAdminEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->admin_email !== $v) {
            $this->admin_email = $v;
            $this->modifiedColumns[] = QuizzesPeer::ADMIN_EMAIL;
        }


        return $this;
    } // setAdminEmail()

    /**
     * Set the value of [comment_section] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setCommentSection($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->comment_section !== $v) {
            $this->comment_section = $v;
            $this->modifiedColumns[] = QuizzesPeer::COMMENT_SECTION;
        }


        return $this;
    } // setCommentSection()

    /**
     * Set the value of [question_from_total] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setQuestionFromTotal($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->question_from_total !== $v) {
            $this->question_from_total = $v;
            $this->modifiedColumns[] = QuizzesPeer::QUESTION_FROM_TOTAL;
        }


        return $this;
    } // setQuestionFromTotal()

    /**
     * Set the value of [total_user_tries] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setTotalUserTries($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->total_user_tries !== $v) {
            $this->total_user_tries = $v;
            $this->modifiedColumns[] = QuizzesPeer::TOTAL_USER_TRIES;
        }


        return $this;
    } // setTotalUserTries()

    /**
     * Set the value of [total_user_tries_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setTotalUserTriesText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->total_user_tries_text !== $v) {
            $this->total_user_tries_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::TOTAL_USER_TRIES_TEXT;
        }


        return $this;
    } // setTotalUserTriesText()

    /**
     * Set the value of [certificate_template] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setCertificateTemplate($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->certificate_template !== $v) {
            $this->certificate_template = $v;
            $this->modifiedColumns[] = QuizzesPeer::CERTIFICATE_TEMPLATE;
        }


        return $this;
    } // setCertificateTemplate()

    /**
     * Set the value of [social_media] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setSocialMedia($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->social_media !== $v) {
            $this->social_media = $v;
            $this->modifiedColumns[] = QuizzesPeer::SOCIAL_MEDIA;
        }


        return $this;
    } // setSocialMedia()

    /**
     * Set the value of [social_media_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setSocialMediaText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->social_media_text !== $v) {
            $this->social_media_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::SOCIAL_MEDIA_TEXT;
        }


        return $this;
    } // setSocialMediaText()

    /**
     * Set the value of [pagination] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setPagination($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pagination !== $v) {
            $this->pagination = $v;
            $this->modifiedColumns[] = QuizzesPeer::PAGINATION;
        }


        return $this;
    } // setPagination()

    /**
     * Set the value of [pagination_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setPaginationText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pagination_text !== $v) {
            $this->pagination_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::PAGINATION_TEXT;
        }


        return $this;
    } // setPaginationText()

    /**
     * Set the value of [timer_limit] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setTimerLimit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->timer_limit !== $v) {
            $this->timer_limit = $v;
            $this->modifiedColumns[] = QuizzesPeer::TIMER_LIMIT;
        }


        return $this;
    } // setTimerLimit()

    /**
     * Set the value of [quiz_stye] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setQuizStye($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->quiz_stye !== $v) {
            $this->quiz_stye = $v;
            $this->modifiedColumns[] = QuizzesPeer::QUIZ_STYE;
        }


        return $this;
    } // setQuizStye()

    /**
     * Set the value of [question_numbering] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setQuestionNumbering($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->question_numbering !== $v) {
            $this->question_numbering = $v;
            $this->modifiedColumns[] = QuizzesPeer::QUESTION_NUMBERING;
        }


        return $this;
    } // setQuestionNumbering()

    /**
     * Set the value of [quiz_settings] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setQuizSettings($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->quiz_settings !== $v) {
            $this->quiz_settings = $v;
            $this->modifiedColumns[] = QuizzesPeer::QUIZ_SETTINGS;
        }


        return $this;
    } // setQuizSettings()

    /**
     * Set the value of [theme_selected] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setThemeSelected($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->theme_selected !== $v) {
            $this->theme_selected = $v;
            $this->modifiedColumns[] = QuizzesPeer::THEME_SELECTED;
        }


        return $this;
    } // setThemeSelected()

    /**
     * Sets the value of [last_activity] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Quizzes The current object (for fluent API support)
     */
    public function setLastActivity($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_activity !== null || $dt !== null) {
            $currentDateAsString = ($this->last_activity !== null && $tmpDt = new DateTime($this->last_activity)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_activity = $newDateAsString;
                $this->modifiedColumns[] = QuizzesPeer::LAST_ACTIVITY;
            }
        } // if either are not null


        return $this;
    } // setLastActivity()

    /**
     * Set the value of [require_log_in] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setRequireLogIn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->require_log_in !== $v) {
            $this->require_log_in = $v;
            $this->modifiedColumns[] = QuizzesPeer::REQUIRE_LOG_IN;
        }


        return $this;
    } // setRequireLogIn()

    /**
     * Set the value of [require_log_in_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setRequireLogInText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->require_log_in_text !== $v) {
            $this->require_log_in_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::REQUIRE_LOG_IN_TEXT;
        }


        return $this;
    } // setRequireLogInText()

    /**
     * Set the value of [limit_total_entries] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setLimitTotalEntries($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->limit_total_entries !== $v) {
            $this->limit_total_entries = $v;
            $this->modifiedColumns[] = QuizzesPeer::LIMIT_TOTAL_ENTRIES;
        }


        return $this;
    } // setLimitTotalEntries()

    /**
     * Set the value of [limit_total_entries_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setLimitTotalEntriesText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->limit_total_entries_text !== $v) {
            $this->limit_total_entries_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::LIMIT_TOTAL_ENTRIES_TEXT;
        }


        return $this;
    } // setLimitTotalEntriesText()

    /**
     * Set the value of [scheduled_timeframe] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setScheduledTimeframe($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->scheduled_timeframe !== $v) {
            $this->scheduled_timeframe = $v;
            $this->modifiedColumns[] = QuizzesPeer::SCHEDULED_TIMEFRAME;
        }


        return $this;
    } // setScheduledTimeframe()

    /**
     * Set the value of [scheduled_timeframe_text] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setScheduledTimeframeText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->scheduled_timeframe_text !== $v) {
            $this->scheduled_timeframe_text = $v;
            $this->modifiedColumns[] = QuizzesPeer::SCHEDULED_TIMEFRAME_TEXT;
        }


        return $this;
    } // setScheduledTimeframeText()

    /**
     * Set the value of [disable_answer_onselect] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setDisableAnswerOnselect($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->disable_answer_onselect !== $v) {
            $this->disable_answer_onselect = $v;
            $this->modifiedColumns[] = QuizzesPeer::DISABLE_ANSWER_ONSELECT;
        }


        return $this;
    } // setDisableAnswerOnselect()

    /**
     * Set the value of [ajax_show_correct] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setAjaxShowCorrect($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ajax_show_correct !== $v) {
            $this->ajax_show_correct = $v;
            $this->modifiedColumns[] = QuizzesPeer::AJAX_SHOW_CORRECT;
        }


        return $this;
    } // setAjaxShowCorrect()

    /**
     * Set the value of [quiz_views] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setQuizViews($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->quiz_views !== $v) {
            $this->quiz_views = $v;
            $this->modifiedColumns[] = QuizzesPeer::QUIZ_VIEWS;
        }


        return $this;
    } // setQuizViews()

    /**
     * Set the value of [quiz_taken] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setQuizTaken($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->quiz_taken !== $v) {
            $this->quiz_taken = $v;
            $this->modifiedColumns[] = QuizzesPeer::QUIZ_TAKEN;
        }


        return $this;
    } // setQuizTaken()

    /**
     * Set the value of [deleted] column.
     *
     * @param  int $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setDeleted($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->deleted !== $v) {
            $this->deleted = $v;
            $this->modifiedColumns[] = QuizzesPeer::DELETED;
        }


        return $this;
    } // setDeleted()

    /**
     * Set the value of [quiz_description] column.
     *
     * @param  string $v new value
     * @return Quizzes The current object (for fluent API support)
     */
    public function setQuizDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->quiz_description !== $v) {
            $this->quiz_description = $v;
            $this->modifiedColumns[] = QuizzesPeer::QUIZ_DESCRIPTION;
        }


        return $this;
    } // setQuizDescription()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->quiz_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->quiz_name = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->message_before = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->message_after = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->message_comment = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->message_end_template = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->user_email_template = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->admin_email_template = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->submit_button_text = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->name_field_text = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->business_field_text = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->email_field_text = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->phone_field_text = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->comment_field_text = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->email_from_text = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->question_answer_template = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->leaderboard_template = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->system = ($row[$startcol + 17] !== null) ? (int) $row[$startcol + 17] : null;
            $this->randomness_order = ($row[$startcol + 18] !== null) ? (int) $row[$startcol + 18] : null;
            $this->loggedin_user_contact = ($row[$startcol + 19] !== null) ? (int) $row[$startcol + 19] : null;
            $this->show_score = ($row[$startcol + 20] !== null) ? (int) $row[$startcol + 20] : null;
            $this->send_user_email = ($row[$startcol + 21] !== null) ? (int) $row[$startcol + 21] : null;
            $this->send_admin_email = ($row[$startcol + 22] !== null) ? (int) $row[$startcol + 22] : null;
            $this->contact_info_location = ($row[$startcol + 23] !== null) ? (int) $row[$startcol + 23] : null;
            $this->user_name = ($row[$startcol + 24] !== null) ? (int) $row[$startcol + 24] : null;
            $this->user_comp = ($row[$startcol + 25] !== null) ? (int) $row[$startcol + 25] : null;
            $this->user_email = ($row[$startcol + 26] !== null) ? (int) $row[$startcol + 26] : null;
            $this->user_phone = ($row[$startcol + 27] !== null) ? (int) $row[$startcol + 27] : null;
            $this->admin_email = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->comment_section = ($row[$startcol + 29] !== null) ? (int) $row[$startcol + 29] : null;
            $this->question_from_total = ($row[$startcol + 30] !== null) ? (int) $row[$startcol + 30] : null;
            $this->total_user_tries = ($row[$startcol + 31] !== null) ? (int) $row[$startcol + 31] : null;
            $this->total_user_tries_text = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
            $this->certificate_template = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->social_media = ($row[$startcol + 34] !== null) ? (int) $row[$startcol + 34] : null;
            $this->social_media_text = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->pagination = ($row[$startcol + 36] !== null) ? (int) $row[$startcol + 36] : null;
            $this->pagination_text = ($row[$startcol + 37] !== null) ? (string) $row[$startcol + 37] : null;
            $this->timer_limit = ($row[$startcol + 38] !== null) ? (int) $row[$startcol + 38] : null;
            $this->quiz_stye = ($row[$startcol + 39] !== null) ? (string) $row[$startcol + 39] : null;
            $this->question_numbering = ($row[$startcol + 40] !== null) ? (int) $row[$startcol + 40] : null;
            $this->quiz_settings = ($row[$startcol + 41] !== null) ? (string) $row[$startcol + 41] : null;
            $this->theme_selected = ($row[$startcol + 42] !== null) ? (string) $row[$startcol + 42] : null;
            $this->last_activity = ($row[$startcol + 43] !== null) ? (string) $row[$startcol + 43] : null;
            $this->require_log_in = ($row[$startcol + 44] !== null) ? (int) $row[$startcol + 44] : null;
            $this->require_log_in_text = ($row[$startcol + 45] !== null) ? (string) $row[$startcol + 45] : null;
            $this->limit_total_entries = ($row[$startcol + 46] !== null) ? (int) $row[$startcol + 46] : null;
            $this->limit_total_entries_text = ($row[$startcol + 47] !== null) ? (string) $row[$startcol + 47] : null;
            $this->scheduled_timeframe = ($row[$startcol + 48] !== null) ? (string) $row[$startcol + 48] : null;
            $this->scheduled_timeframe_text = ($row[$startcol + 49] !== null) ? (string) $row[$startcol + 49] : null;
            $this->disable_answer_onselect = ($row[$startcol + 50] !== null) ? (int) $row[$startcol + 50] : null;
            $this->ajax_show_correct = ($row[$startcol + 51] !== null) ? (int) $row[$startcol + 51] : null;
            $this->quiz_views = ($row[$startcol + 52] !== null) ? (int) $row[$startcol + 52] : null;
            $this->quiz_taken = ($row[$startcol + 53] !== null) ? (int) $row[$startcol + 53] : null;
            $this->deleted = ($row[$startcol + 54] !== null) ? (int) $row[$startcol + 54] : null;
            $this->quiz_description = ($row[$startcol + 55] !== null) ? (string) $row[$startcol + 55] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 56; // 56 = QuizzesPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Quizzes object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = QuizzesPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = QuizzesQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                QuizzesPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = QuizzesPeer::QUIZ_ID;
        if (null !== $this->quiz_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . QuizzesPeer::QUIZ_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(QuizzesPeer::QUIZ_ID)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_id`';
        }
        if ($this->isColumnModified(QuizzesPeer::QUIZ_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_name`';
        }
        if ($this->isColumnModified(QuizzesPeer::MESSAGE_BEFORE)) {
            $modifiedColumns[':p' . $index++]  = '`message_before`';
        }
        if ($this->isColumnModified(QuizzesPeer::MESSAGE_AFTER)) {
            $modifiedColumns[':p' . $index++]  = '`message_after`';
        }
        if ($this->isColumnModified(QuizzesPeer::MESSAGE_COMMENT)) {
            $modifiedColumns[':p' . $index++]  = '`message_comment`';
        }
        if ($this->isColumnModified(QuizzesPeer::MESSAGE_END_TEMPLATE)) {
            $modifiedColumns[':p' . $index++]  = '`message_end_template`';
        }
        if ($this->isColumnModified(QuizzesPeer::USER_EMAIL_TEMPLATE)) {
            $modifiedColumns[':p' . $index++]  = '`user_email_template`';
        }
        if ($this->isColumnModified(QuizzesPeer::ADMIN_EMAIL_TEMPLATE)) {
            $modifiedColumns[':p' . $index++]  = '`admin_email_template`';
        }
        if ($this->isColumnModified(QuizzesPeer::SUBMIT_BUTTON_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`submit_button_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::NAME_FIELD_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`name_field_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::BUSINESS_FIELD_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`business_field_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::EMAIL_FIELD_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`email_field_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::PHONE_FIELD_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`phone_field_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::COMMENT_FIELD_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`comment_field_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::EMAIL_FROM_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`email_from_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::QUESTION_ANSWER_TEMPLATE)) {
            $modifiedColumns[':p' . $index++]  = '`question_answer_template`';
        }
        if ($this->isColumnModified(QuizzesPeer::LEADERBOARD_TEMPLATE)) {
            $modifiedColumns[':p' . $index++]  = '`leaderboard_template`';
        }
        if ($this->isColumnModified(QuizzesPeer::SYSTEM)) {
            $modifiedColumns[':p' . $index++]  = '`system`';
        }
        if ($this->isColumnModified(QuizzesPeer::RANDOMNESS_ORDER)) {
            $modifiedColumns[':p' . $index++]  = '`randomness_order`';
        }
        if ($this->isColumnModified(QuizzesPeer::LOGGEDIN_USER_CONTACT)) {
            $modifiedColumns[':p' . $index++]  = '`loggedin_user_contact`';
        }
        if ($this->isColumnModified(QuizzesPeer::SHOW_SCORE)) {
            $modifiedColumns[':p' . $index++]  = '`show_score`';
        }
        if ($this->isColumnModified(QuizzesPeer::SEND_USER_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`send_user_email`';
        }
        if ($this->isColumnModified(QuizzesPeer::SEND_ADMIN_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`send_admin_email`';
        }
        if ($this->isColumnModified(QuizzesPeer::CONTACT_INFO_LOCATION)) {
            $modifiedColumns[':p' . $index++]  = '`contact_info_location`';
        }
        if ($this->isColumnModified(QuizzesPeer::USER_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`user_name`';
        }
        if ($this->isColumnModified(QuizzesPeer::USER_COMP)) {
            $modifiedColumns[':p' . $index++]  = '`user_comp`';
        }
        if ($this->isColumnModified(QuizzesPeer::USER_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`user_email`';
        }
        if ($this->isColumnModified(QuizzesPeer::USER_PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`user_phone`';
        }
        if ($this->isColumnModified(QuizzesPeer::ADMIN_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`admin_email`';
        }
        if ($this->isColumnModified(QuizzesPeer::COMMENT_SECTION)) {
            $modifiedColumns[':p' . $index++]  = '`comment_section`';
        }
        if ($this->isColumnModified(QuizzesPeer::QUESTION_FROM_TOTAL)) {
            $modifiedColumns[':p' . $index++]  = '`question_from_total`';
        }
        if ($this->isColumnModified(QuizzesPeer::TOTAL_USER_TRIES)) {
            $modifiedColumns[':p' . $index++]  = '`total_user_tries`';
        }
        if ($this->isColumnModified(QuizzesPeer::TOTAL_USER_TRIES_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`total_user_tries_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::CERTIFICATE_TEMPLATE)) {
            $modifiedColumns[':p' . $index++]  = '`certificate_template`';
        }
        if ($this->isColumnModified(QuizzesPeer::SOCIAL_MEDIA)) {
            $modifiedColumns[':p' . $index++]  = '`social_media`';
        }
        if ($this->isColumnModified(QuizzesPeer::SOCIAL_MEDIA_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`social_media_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::PAGINATION)) {
            $modifiedColumns[':p' . $index++]  = '`pagination`';
        }
        if ($this->isColumnModified(QuizzesPeer::PAGINATION_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`pagination_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::TIMER_LIMIT)) {
            $modifiedColumns[':p' . $index++]  = '`timer_limit`';
        }
        if ($this->isColumnModified(QuizzesPeer::QUIZ_STYE)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_stye`';
        }
        if ($this->isColumnModified(QuizzesPeer::QUESTION_NUMBERING)) {
            $modifiedColumns[':p' . $index++]  = '`question_numbering`';
        }
        if ($this->isColumnModified(QuizzesPeer::QUIZ_SETTINGS)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_settings`';
        }
        if ($this->isColumnModified(QuizzesPeer::THEME_SELECTED)) {
            $modifiedColumns[':p' . $index++]  = '`theme_selected`';
        }
        if ($this->isColumnModified(QuizzesPeer::LAST_ACTIVITY)) {
            $modifiedColumns[':p' . $index++]  = '`last_activity`';
        }
        if ($this->isColumnModified(QuizzesPeer::REQUIRE_LOG_IN)) {
            $modifiedColumns[':p' . $index++]  = '`require_log_in`';
        }
        if ($this->isColumnModified(QuizzesPeer::REQUIRE_LOG_IN_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`require_log_in_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::LIMIT_TOTAL_ENTRIES)) {
            $modifiedColumns[':p' . $index++]  = '`limit_total_entries`';
        }
        if ($this->isColumnModified(QuizzesPeer::LIMIT_TOTAL_ENTRIES_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`limit_total_entries_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::SCHEDULED_TIMEFRAME)) {
            $modifiedColumns[':p' . $index++]  = '`scheduled_timeframe`';
        }
        if ($this->isColumnModified(QuizzesPeer::SCHEDULED_TIMEFRAME_TEXT)) {
            $modifiedColumns[':p' . $index++]  = '`scheduled_timeframe_text`';
        }
        if ($this->isColumnModified(QuizzesPeer::DISABLE_ANSWER_ONSELECT)) {
            $modifiedColumns[':p' . $index++]  = '`disable_answer_onselect`';
        }
        if ($this->isColumnModified(QuizzesPeer::AJAX_SHOW_CORRECT)) {
            $modifiedColumns[':p' . $index++]  = '`ajax_show_correct`';
        }
        if ($this->isColumnModified(QuizzesPeer::QUIZ_VIEWS)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_views`';
        }
        if ($this->isColumnModified(QuizzesPeer::QUIZ_TAKEN)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_taken`';
        }
        if ($this->isColumnModified(QuizzesPeer::DELETED)) {
            $modifiedColumns[':p' . $index++]  = '`deleted`';
        }
        if ($this->isColumnModified(QuizzesPeer::QUIZ_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_description`';
        }

        $sql = sprintf(
            'INSERT INTO `rfn_quizzes` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`quiz_id`':
                        $stmt->bindValue($identifier, $this->quiz_id, PDO::PARAM_INT);
                        break;
                    case '`quiz_name`':
                        $stmt->bindValue($identifier, $this->quiz_name, PDO::PARAM_STR);
                        break;
                    case '`message_before`':
                        $stmt->bindValue($identifier, $this->message_before, PDO::PARAM_STR);
                        break;
                    case '`message_after`':
                        $stmt->bindValue($identifier, $this->message_after, PDO::PARAM_STR);
                        break;
                    case '`message_comment`':
                        $stmt->bindValue($identifier, $this->message_comment, PDO::PARAM_STR);
                        break;
                    case '`message_end_template`':
                        $stmt->bindValue($identifier, $this->message_end_template, PDO::PARAM_STR);
                        break;
                    case '`user_email_template`':
                        $stmt->bindValue($identifier, $this->user_email_template, PDO::PARAM_STR);
                        break;
                    case '`admin_email_template`':
                        $stmt->bindValue($identifier, $this->admin_email_template, PDO::PARAM_STR);
                        break;
                    case '`submit_button_text`':
                        $stmt->bindValue($identifier, $this->submit_button_text, PDO::PARAM_STR);
                        break;
                    case '`name_field_text`':
                        $stmt->bindValue($identifier, $this->name_field_text, PDO::PARAM_STR);
                        break;
                    case '`business_field_text`':
                        $stmt->bindValue($identifier, $this->business_field_text, PDO::PARAM_STR);
                        break;
                    case '`email_field_text`':
                        $stmt->bindValue($identifier, $this->email_field_text, PDO::PARAM_STR);
                        break;
                    case '`phone_field_text`':
                        $stmt->bindValue($identifier, $this->phone_field_text, PDO::PARAM_STR);
                        break;
                    case '`comment_field_text`':
                        $stmt->bindValue($identifier, $this->comment_field_text, PDO::PARAM_STR);
                        break;
                    case '`email_from_text`':
                        $stmt->bindValue($identifier, $this->email_from_text, PDO::PARAM_STR);
                        break;
                    case '`question_answer_template`':
                        $stmt->bindValue($identifier, $this->question_answer_template, PDO::PARAM_STR);
                        break;
                    case '`leaderboard_template`':
                        $stmt->bindValue($identifier, $this->leaderboard_template, PDO::PARAM_STR);
                        break;
                    case '`system`':
                        $stmt->bindValue($identifier, $this->system, PDO::PARAM_INT);
                        break;
                    case '`randomness_order`':
                        $stmt->bindValue($identifier, $this->randomness_order, PDO::PARAM_INT);
                        break;
                    case '`loggedin_user_contact`':
                        $stmt->bindValue($identifier, $this->loggedin_user_contact, PDO::PARAM_INT);
                        break;
                    case '`show_score`':
                        $stmt->bindValue($identifier, $this->show_score, PDO::PARAM_INT);
                        break;
                    case '`send_user_email`':
                        $stmt->bindValue($identifier, $this->send_user_email, PDO::PARAM_INT);
                        break;
                    case '`send_admin_email`':
                        $stmt->bindValue($identifier, $this->send_admin_email, PDO::PARAM_INT);
                        break;
                    case '`contact_info_location`':
                        $stmt->bindValue($identifier, $this->contact_info_location, PDO::PARAM_INT);
                        break;
                    case '`user_name`':
                        $stmt->bindValue($identifier, $this->user_name, PDO::PARAM_INT);
                        break;
                    case '`user_comp`':
                        $stmt->bindValue($identifier, $this->user_comp, PDO::PARAM_INT);
                        break;
                    case '`user_email`':
                        $stmt->bindValue($identifier, $this->user_email, PDO::PARAM_INT);
                        break;
                    case '`user_phone`':
                        $stmt->bindValue($identifier, $this->user_phone, PDO::PARAM_INT);
                        break;
                    case '`admin_email`':
                        $stmt->bindValue($identifier, $this->admin_email, PDO::PARAM_STR);
                        break;
                    case '`comment_section`':
                        $stmt->bindValue($identifier, $this->comment_section, PDO::PARAM_INT);
                        break;
                    case '`question_from_total`':
                        $stmt->bindValue($identifier, $this->question_from_total, PDO::PARAM_INT);
                        break;
                    case '`total_user_tries`':
                        $stmt->bindValue($identifier, $this->total_user_tries, PDO::PARAM_INT);
                        break;
                    case '`total_user_tries_text`':
                        $stmt->bindValue($identifier, $this->total_user_tries_text, PDO::PARAM_STR);
                        break;
                    case '`certificate_template`':
                        $stmt->bindValue($identifier, $this->certificate_template, PDO::PARAM_STR);
                        break;
                    case '`social_media`':
                        $stmt->bindValue($identifier, $this->social_media, PDO::PARAM_INT);
                        break;
                    case '`social_media_text`':
                        $stmt->bindValue($identifier, $this->social_media_text, PDO::PARAM_STR);
                        break;
                    case '`pagination`':
                        $stmt->bindValue($identifier, $this->pagination, PDO::PARAM_INT);
                        break;
                    case '`pagination_text`':
                        $stmt->bindValue($identifier, $this->pagination_text, PDO::PARAM_STR);
                        break;
                    case '`timer_limit`':
                        $stmt->bindValue($identifier, $this->timer_limit, PDO::PARAM_INT);
                        break;
                    case '`quiz_stye`':
                        $stmt->bindValue($identifier, $this->quiz_stye, PDO::PARAM_STR);
                        break;
                    case '`question_numbering`':
                        $stmt->bindValue($identifier, $this->question_numbering, PDO::PARAM_INT);
                        break;
                    case '`quiz_settings`':
                        $stmt->bindValue($identifier, $this->quiz_settings, PDO::PARAM_STR);
                        break;
                    case '`theme_selected`':
                        $stmt->bindValue($identifier, $this->theme_selected, PDO::PARAM_STR);
                        break;
                    case '`last_activity`':
                        $stmt->bindValue($identifier, $this->last_activity, PDO::PARAM_STR);
                        break;
                    case '`require_log_in`':
                        $stmt->bindValue($identifier, $this->require_log_in, PDO::PARAM_INT);
                        break;
                    case '`require_log_in_text`':
                        $stmt->bindValue($identifier, $this->require_log_in_text, PDO::PARAM_STR);
                        break;
                    case '`limit_total_entries`':
                        $stmt->bindValue($identifier, $this->limit_total_entries, PDO::PARAM_INT);
                        break;
                    case '`limit_total_entries_text`':
                        $stmt->bindValue($identifier, $this->limit_total_entries_text, PDO::PARAM_STR);
                        break;
                    case '`scheduled_timeframe`':
                        $stmt->bindValue($identifier, $this->scheduled_timeframe, PDO::PARAM_STR);
                        break;
                    case '`scheduled_timeframe_text`':
                        $stmt->bindValue($identifier, $this->scheduled_timeframe_text, PDO::PARAM_STR);
                        break;
                    case '`disable_answer_onselect`':
                        $stmt->bindValue($identifier, $this->disable_answer_onselect, PDO::PARAM_INT);
                        break;
                    case '`ajax_show_correct`':
                        $stmt->bindValue($identifier, $this->ajax_show_correct, PDO::PARAM_INT);
                        break;
                    case '`quiz_views`':
                        $stmt->bindValue($identifier, $this->quiz_views, PDO::PARAM_INT);
                        break;
                    case '`quiz_taken`':
                        $stmt->bindValue($identifier, $this->quiz_taken, PDO::PARAM_INT);
                        break;
                    case '`deleted`':
                        $stmt->bindValue($identifier, $this->deleted, PDO::PARAM_INT);
                        break;
                    case '`quiz_description`':
                        $stmt->bindValue($identifier, $this->quiz_description, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setQuizId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = QuizzesPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuizzesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getQuizId();
                break;
            case 1:
                return $this->getQuizName();
                break;
            case 2:
                return $this->getMessageBefore();
                break;
            case 3:
                return $this->getMessageAfter();
                break;
            case 4:
                return $this->getMessageComment();
                break;
            case 5:
                return $this->getMessageEndTemplate();
                break;
            case 6:
                return $this->getUserEmailTemplate();
                break;
            case 7:
                return $this->getAdminEmailTemplate();
                break;
            case 8:
                return $this->getSubmitButtonText();
                break;
            case 9:
                return $this->getNameFieldText();
                break;
            case 10:
                return $this->getBusinessFieldText();
                break;
            case 11:
                return $this->getEmailFieldText();
                break;
            case 12:
                return $this->getPhoneFieldText();
                break;
            case 13:
                return $this->getCommentFieldText();
                break;
            case 14:
                return $this->getEmailFromText();
                break;
            case 15:
                return $this->getQuestionAnswerTemplate();
                break;
            case 16:
                return $this->getLeaderboardTemplate();
                break;
            case 17:
                return $this->getSystem();
                break;
            case 18:
                return $this->getRandomnessOrder();
                break;
            case 19:
                return $this->getLoggedinUserContact();
                break;
            case 20:
                return $this->getShowScore();
                break;
            case 21:
                return $this->getSendUserEmail();
                break;
            case 22:
                return $this->getSendAdminEmail();
                break;
            case 23:
                return $this->getContactInfoLocation();
                break;
            case 24:
                return $this->getUserName();
                break;
            case 25:
                return $this->getUserComp();
                break;
            case 26:
                return $this->getUserEmail();
                break;
            case 27:
                return $this->getUserPhone();
                break;
            case 28:
                return $this->getAdminEmail();
                break;
            case 29:
                return $this->getCommentSection();
                break;
            case 30:
                return $this->getQuestionFromTotal();
                break;
            case 31:
                return $this->getTotalUserTries();
                break;
            case 32:
                return $this->getTotalUserTriesText();
                break;
            case 33:
                return $this->getCertificateTemplate();
                break;
            case 34:
                return $this->getSocialMedia();
                break;
            case 35:
                return $this->getSocialMediaText();
                break;
            case 36:
                return $this->getPagination();
                break;
            case 37:
                return $this->getPaginationText();
                break;
            case 38:
                return $this->getTimerLimit();
                break;
            case 39:
                return $this->getQuizStye();
                break;
            case 40:
                return $this->getQuestionNumbering();
                break;
            case 41:
                return $this->getQuizSettings();
                break;
            case 42:
                return $this->getThemeSelected();
                break;
            case 43:
                return $this->getLastActivity();
                break;
            case 44:
                return $this->getRequireLogIn();
                break;
            case 45:
                return $this->getRequireLogInText();
                break;
            case 46:
                return $this->getLimitTotalEntries();
                break;
            case 47:
                return $this->getLimitTotalEntriesText();
                break;
            case 48:
                return $this->getScheduledTimeframe();
                break;
            case 49:
                return $this->getScheduledTimeframeText();
                break;
            case 50:
                return $this->getDisableAnswerOnselect();
                break;
            case 51:
                return $this->getAjaxShowCorrect();
                break;
            case 52:
                return $this->getQuizViews();
                break;
            case 53:
                return $this->getQuizTaken();
                break;
            case 54:
                return $this->getDeleted();
                break;
            case 55:
                return $this->getQuizDescription();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['Quizzes'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Quizzes'][$this->getPrimaryKey()] = true;
        $keys = QuizzesPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getQuizId(),
            $keys[1] => $this->getQuizName(),
            $keys[2] => $this->getMessageBefore(),
            $keys[3] => $this->getMessageAfter(),
            $keys[4] => $this->getMessageComment(),
            $keys[5] => $this->getMessageEndTemplate(),
            $keys[6] => $this->getUserEmailTemplate(),
            $keys[7] => $this->getAdminEmailTemplate(),
            $keys[8] => $this->getSubmitButtonText(),
            $keys[9] => $this->getNameFieldText(),
            $keys[10] => $this->getBusinessFieldText(),
            $keys[11] => $this->getEmailFieldText(),
            $keys[12] => $this->getPhoneFieldText(),
            $keys[13] => $this->getCommentFieldText(),
            $keys[14] => $this->getEmailFromText(),
            $keys[15] => $this->getQuestionAnswerTemplate(),
            $keys[16] => $this->getLeaderboardTemplate(),
            $keys[17] => $this->getSystem(),
            $keys[18] => $this->getRandomnessOrder(),
            $keys[19] => $this->getLoggedinUserContact(),
            $keys[20] => $this->getShowScore(),
            $keys[21] => $this->getSendUserEmail(),
            $keys[22] => $this->getSendAdminEmail(),
            $keys[23] => $this->getContactInfoLocation(),
            $keys[24] => $this->getUserName(),
            $keys[25] => $this->getUserComp(),
            $keys[26] => $this->getUserEmail(),
            $keys[27] => $this->getUserPhone(),
            $keys[28] => $this->getAdminEmail(),
            $keys[29] => $this->getCommentSection(),
            $keys[30] => $this->getQuestionFromTotal(),
            $keys[31] => $this->getTotalUserTries(),
            $keys[32] => $this->getTotalUserTriesText(),
            $keys[33] => $this->getCertificateTemplate(),
            $keys[34] => $this->getSocialMedia(),
            $keys[35] => $this->getSocialMediaText(),
            $keys[36] => $this->getPagination(),
            $keys[37] => $this->getPaginationText(),
            $keys[38] => $this->getTimerLimit(),
            $keys[39] => $this->getQuizStye(),
            $keys[40] => $this->getQuestionNumbering(),
            $keys[41] => $this->getQuizSettings(),
            $keys[42] => $this->getThemeSelected(),
            $keys[43] => $this->getLastActivity(),
            $keys[44] => $this->getRequireLogIn(),
            $keys[45] => $this->getRequireLogInText(),
            $keys[46] => $this->getLimitTotalEntries(),
            $keys[47] => $this->getLimitTotalEntriesText(),
            $keys[48] => $this->getScheduledTimeframe(),
            $keys[49] => $this->getScheduledTimeframeText(),
            $keys[50] => $this->getDisableAnswerOnselect(),
            $keys[51] => $this->getAjaxShowCorrect(),
            $keys[52] => $this->getQuizViews(),
            $keys[53] => $this->getQuizTaken(),
            $keys[54] => $this->getDeleted(),
            $keys[55] => $this->getQuizDescription(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuizzesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setQuizId($value);
                break;
            case 1:
                $this->setQuizName($value);
                break;
            case 2:
                $this->setMessageBefore($value);
                break;
            case 3:
                $this->setMessageAfter($value);
                break;
            case 4:
                $this->setMessageComment($value);
                break;
            case 5:
                $this->setMessageEndTemplate($value);
                break;
            case 6:
                $this->setUserEmailTemplate($value);
                break;
            case 7:
                $this->setAdminEmailTemplate($value);
                break;
            case 8:
                $this->setSubmitButtonText($value);
                break;
            case 9:
                $this->setNameFieldText($value);
                break;
            case 10:
                $this->setBusinessFieldText($value);
                break;
            case 11:
                $this->setEmailFieldText($value);
                break;
            case 12:
                $this->setPhoneFieldText($value);
                break;
            case 13:
                $this->setCommentFieldText($value);
                break;
            case 14:
                $this->setEmailFromText($value);
                break;
            case 15:
                $this->setQuestionAnswerTemplate($value);
                break;
            case 16:
                $this->setLeaderboardTemplate($value);
                break;
            case 17:
                $this->setSystem($value);
                break;
            case 18:
                $this->setRandomnessOrder($value);
                break;
            case 19:
                $this->setLoggedinUserContact($value);
                break;
            case 20:
                $this->setShowScore($value);
                break;
            case 21:
                $this->setSendUserEmail($value);
                break;
            case 22:
                $this->setSendAdminEmail($value);
                break;
            case 23:
                $this->setContactInfoLocation($value);
                break;
            case 24:
                $this->setUserName($value);
                break;
            case 25:
                $this->setUserComp($value);
                break;
            case 26:
                $this->setUserEmail($value);
                break;
            case 27:
                $this->setUserPhone($value);
                break;
            case 28:
                $this->setAdminEmail($value);
                break;
            case 29:
                $this->setCommentSection($value);
                break;
            case 30:
                $this->setQuestionFromTotal($value);
                break;
            case 31:
                $this->setTotalUserTries($value);
                break;
            case 32:
                $this->setTotalUserTriesText($value);
                break;
            case 33:
                $this->setCertificateTemplate($value);
                break;
            case 34:
                $this->setSocialMedia($value);
                break;
            case 35:
                $this->setSocialMediaText($value);
                break;
            case 36:
                $this->setPagination($value);
                break;
            case 37:
                $this->setPaginationText($value);
                break;
            case 38:
                $this->setTimerLimit($value);
                break;
            case 39:
                $this->setQuizStye($value);
                break;
            case 40:
                $this->setQuestionNumbering($value);
                break;
            case 41:
                $this->setQuizSettings($value);
                break;
            case 42:
                $this->setThemeSelected($value);
                break;
            case 43:
                $this->setLastActivity($value);
                break;
            case 44:
                $this->setRequireLogIn($value);
                break;
            case 45:
                $this->setRequireLogInText($value);
                break;
            case 46:
                $this->setLimitTotalEntries($value);
                break;
            case 47:
                $this->setLimitTotalEntriesText($value);
                break;
            case 48:
                $this->setScheduledTimeframe($value);
                break;
            case 49:
                $this->setScheduledTimeframeText($value);
                break;
            case 50:
                $this->setDisableAnswerOnselect($value);
                break;
            case 51:
                $this->setAjaxShowCorrect($value);
                break;
            case 52:
                $this->setQuizViews($value);
                break;
            case 53:
                $this->setQuizTaken($value);
                break;
            case 54:
                $this->setDeleted($value);
                break;
            case 55:
                $this->setQuizDescription($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = QuizzesPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setQuizId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setQuizName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setMessageBefore($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setMessageAfter($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMessageComment($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setMessageEndTemplate($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setUserEmailTemplate($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setAdminEmailTemplate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setSubmitButtonText($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setNameFieldText($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setBusinessFieldText($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setEmailFieldText($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setPhoneFieldText($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setCommentFieldText($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setEmailFromText($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setQuestionAnswerTemplate($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setLeaderboardTemplate($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setSystem($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setRandomnessOrder($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setLoggedinUserContact($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setShowScore($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setSendUserEmail($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setSendAdminEmail($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setContactInfoLocation($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setUserName($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setUserComp($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setUserEmail($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setUserPhone($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setAdminEmail($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setCommentSection($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setQuestionFromTotal($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setTotalUserTries($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setTotalUserTriesText($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setCertificateTemplate($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setSocialMedia($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setSocialMediaText($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setPagination($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setPaginationText($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setTimerLimit($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setQuizStye($arr[$keys[39]]);
        if (array_key_exists($keys[40], $arr)) $this->setQuestionNumbering($arr[$keys[40]]);
        if (array_key_exists($keys[41], $arr)) $this->setQuizSettings($arr[$keys[41]]);
        if (array_key_exists($keys[42], $arr)) $this->setThemeSelected($arr[$keys[42]]);
        if (array_key_exists($keys[43], $arr)) $this->setLastActivity($arr[$keys[43]]);
        if (array_key_exists($keys[44], $arr)) $this->setRequireLogIn($arr[$keys[44]]);
        if (array_key_exists($keys[45], $arr)) $this->setRequireLogInText($arr[$keys[45]]);
        if (array_key_exists($keys[46], $arr)) $this->setLimitTotalEntries($arr[$keys[46]]);
        if (array_key_exists($keys[47], $arr)) $this->setLimitTotalEntriesText($arr[$keys[47]]);
        if (array_key_exists($keys[48], $arr)) $this->setScheduledTimeframe($arr[$keys[48]]);
        if (array_key_exists($keys[49], $arr)) $this->setScheduledTimeframeText($arr[$keys[49]]);
        if (array_key_exists($keys[50], $arr)) $this->setDisableAnswerOnselect($arr[$keys[50]]);
        if (array_key_exists($keys[51], $arr)) $this->setAjaxShowCorrect($arr[$keys[51]]);
        if (array_key_exists($keys[52], $arr)) $this->setQuizViews($arr[$keys[52]]);
        if (array_key_exists($keys[53], $arr)) $this->setQuizTaken($arr[$keys[53]]);
        if (array_key_exists($keys[54], $arr)) $this->setDeleted($arr[$keys[54]]);
        if (array_key_exists($keys[55], $arr)) $this->setQuizDescription($arr[$keys[55]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(QuizzesPeer::DATABASE_NAME);

        if ($this->isColumnModified(QuizzesPeer::QUIZ_ID)) $criteria->add(QuizzesPeer::QUIZ_ID, $this->quiz_id);
        if ($this->isColumnModified(QuizzesPeer::QUIZ_NAME)) $criteria->add(QuizzesPeer::QUIZ_NAME, $this->quiz_name);
        if ($this->isColumnModified(QuizzesPeer::MESSAGE_BEFORE)) $criteria->add(QuizzesPeer::MESSAGE_BEFORE, $this->message_before);
        if ($this->isColumnModified(QuizzesPeer::MESSAGE_AFTER)) $criteria->add(QuizzesPeer::MESSAGE_AFTER, $this->message_after);
        if ($this->isColumnModified(QuizzesPeer::MESSAGE_COMMENT)) $criteria->add(QuizzesPeer::MESSAGE_COMMENT, $this->message_comment);
        if ($this->isColumnModified(QuizzesPeer::MESSAGE_END_TEMPLATE)) $criteria->add(QuizzesPeer::MESSAGE_END_TEMPLATE, $this->message_end_template);
        if ($this->isColumnModified(QuizzesPeer::USER_EMAIL_TEMPLATE)) $criteria->add(QuizzesPeer::USER_EMAIL_TEMPLATE, $this->user_email_template);
        if ($this->isColumnModified(QuizzesPeer::ADMIN_EMAIL_TEMPLATE)) $criteria->add(QuizzesPeer::ADMIN_EMAIL_TEMPLATE, $this->admin_email_template);
        if ($this->isColumnModified(QuizzesPeer::SUBMIT_BUTTON_TEXT)) $criteria->add(QuizzesPeer::SUBMIT_BUTTON_TEXT, $this->submit_button_text);
        if ($this->isColumnModified(QuizzesPeer::NAME_FIELD_TEXT)) $criteria->add(QuizzesPeer::NAME_FIELD_TEXT, $this->name_field_text);
        if ($this->isColumnModified(QuizzesPeer::BUSINESS_FIELD_TEXT)) $criteria->add(QuizzesPeer::BUSINESS_FIELD_TEXT, $this->business_field_text);
        if ($this->isColumnModified(QuizzesPeer::EMAIL_FIELD_TEXT)) $criteria->add(QuizzesPeer::EMAIL_FIELD_TEXT, $this->email_field_text);
        if ($this->isColumnModified(QuizzesPeer::PHONE_FIELD_TEXT)) $criteria->add(QuizzesPeer::PHONE_FIELD_TEXT, $this->phone_field_text);
        if ($this->isColumnModified(QuizzesPeer::COMMENT_FIELD_TEXT)) $criteria->add(QuizzesPeer::COMMENT_FIELD_TEXT, $this->comment_field_text);
        if ($this->isColumnModified(QuizzesPeer::EMAIL_FROM_TEXT)) $criteria->add(QuizzesPeer::EMAIL_FROM_TEXT, $this->email_from_text);
        if ($this->isColumnModified(QuizzesPeer::QUESTION_ANSWER_TEMPLATE)) $criteria->add(QuizzesPeer::QUESTION_ANSWER_TEMPLATE, $this->question_answer_template);
        if ($this->isColumnModified(QuizzesPeer::LEADERBOARD_TEMPLATE)) $criteria->add(QuizzesPeer::LEADERBOARD_TEMPLATE, $this->leaderboard_template);
        if ($this->isColumnModified(QuizzesPeer::SYSTEM)) $criteria->add(QuizzesPeer::SYSTEM, $this->system);
        if ($this->isColumnModified(QuizzesPeer::RANDOMNESS_ORDER)) $criteria->add(QuizzesPeer::RANDOMNESS_ORDER, $this->randomness_order);
        if ($this->isColumnModified(QuizzesPeer::LOGGEDIN_USER_CONTACT)) $criteria->add(QuizzesPeer::LOGGEDIN_USER_CONTACT, $this->loggedin_user_contact);
        if ($this->isColumnModified(QuizzesPeer::SHOW_SCORE)) $criteria->add(QuizzesPeer::SHOW_SCORE, $this->show_score);
        if ($this->isColumnModified(QuizzesPeer::SEND_USER_EMAIL)) $criteria->add(QuizzesPeer::SEND_USER_EMAIL, $this->send_user_email);
        if ($this->isColumnModified(QuizzesPeer::SEND_ADMIN_EMAIL)) $criteria->add(QuizzesPeer::SEND_ADMIN_EMAIL, $this->send_admin_email);
        if ($this->isColumnModified(QuizzesPeer::CONTACT_INFO_LOCATION)) $criteria->add(QuizzesPeer::CONTACT_INFO_LOCATION, $this->contact_info_location);
        if ($this->isColumnModified(QuizzesPeer::USER_NAME)) $criteria->add(QuizzesPeer::USER_NAME, $this->user_name);
        if ($this->isColumnModified(QuizzesPeer::USER_COMP)) $criteria->add(QuizzesPeer::USER_COMP, $this->user_comp);
        if ($this->isColumnModified(QuizzesPeer::USER_EMAIL)) $criteria->add(QuizzesPeer::USER_EMAIL, $this->user_email);
        if ($this->isColumnModified(QuizzesPeer::USER_PHONE)) $criteria->add(QuizzesPeer::USER_PHONE, $this->user_phone);
        if ($this->isColumnModified(QuizzesPeer::ADMIN_EMAIL)) $criteria->add(QuizzesPeer::ADMIN_EMAIL, $this->admin_email);
        if ($this->isColumnModified(QuizzesPeer::COMMENT_SECTION)) $criteria->add(QuizzesPeer::COMMENT_SECTION, $this->comment_section);
        if ($this->isColumnModified(QuizzesPeer::QUESTION_FROM_TOTAL)) $criteria->add(QuizzesPeer::QUESTION_FROM_TOTAL, $this->question_from_total);
        if ($this->isColumnModified(QuizzesPeer::TOTAL_USER_TRIES)) $criteria->add(QuizzesPeer::TOTAL_USER_TRIES, $this->total_user_tries);
        if ($this->isColumnModified(QuizzesPeer::TOTAL_USER_TRIES_TEXT)) $criteria->add(QuizzesPeer::TOTAL_USER_TRIES_TEXT, $this->total_user_tries_text);
        if ($this->isColumnModified(QuizzesPeer::CERTIFICATE_TEMPLATE)) $criteria->add(QuizzesPeer::CERTIFICATE_TEMPLATE, $this->certificate_template);
        if ($this->isColumnModified(QuizzesPeer::SOCIAL_MEDIA)) $criteria->add(QuizzesPeer::SOCIAL_MEDIA, $this->social_media);
        if ($this->isColumnModified(QuizzesPeer::SOCIAL_MEDIA_TEXT)) $criteria->add(QuizzesPeer::SOCIAL_MEDIA_TEXT, $this->social_media_text);
        if ($this->isColumnModified(QuizzesPeer::PAGINATION)) $criteria->add(QuizzesPeer::PAGINATION, $this->pagination);
        if ($this->isColumnModified(QuizzesPeer::PAGINATION_TEXT)) $criteria->add(QuizzesPeer::PAGINATION_TEXT, $this->pagination_text);
        if ($this->isColumnModified(QuizzesPeer::TIMER_LIMIT)) $criteria->add(QuizzesPeer::TIMER_LIMIT, $this->timer_limit);
        if ($this->isColumnModified(QuizzesPeer::QUIZ_STYE)) $criteria->add(QuizzesPeer::QUIZ_STYE, $this->quiz_stye);
        if ($this->isColumnModified(QuizzesPeer::QUESTION_NUMBERING)) $criteria->add(QuizzesPeer::QUESTION_NUMBERING, $this->question_numbering);
        if ($this->isColumnModified(QuizzesPeer::QUIZ_SETTINGS)) $criteria->add(QuizzesPeer::QUIZ_SETTINGS, $this->quiz_settings);
        if ($this->isColumnModified(QuizzesPeer::THEME_SELECTED)) $criteria->add(QuizzesPeer::THEME_SELECTED, $this->theme_selected);
        if ($this->isColumnModified(QuizzesPeer::LAST_ACTIVITY)) $criteria->add(QuizzesPeer::LAST_ACTIVITY, $this->last_activity);
        if ($this->isColumnModified(QuizzesPeer::REQUIRE_LOG_IN)) $criteria->add(QuizzesPeer::REQUIRE_LOG_IN, $this->require_log_in);
        if ($this->isColumnModified(QuizzesPeer::REQUIRE_LOG_IN_TEXT)) $criteria->add(QuizzesPeer::REQUIRE_LOG_IN_TEXT, $this->require_log_in_text);
        if ($this->isColumnModified(QuizzesPeer::LIMIT_TOTAL_ENTRIES)) $criteria->add(QuizzesPeer::LIMIT_TOTAL_ENTRIES, $this->limit_total_entries);
        if ($this->isColumnModified(QuizzesPeer::LIMIT_TOTAL_ENTRIES_TEXT)) $criteria->add(QuizzesPeer::LIMIT_TOTAL_ENTRIES_TEXT, $this->limit_total_entries_text);
        if ($this->isColumnModified(QuizzesPeer::SCHEDULED_TIMEFRAME)) $criteria->add(QuizzesPeer::SCHEDULED_TIMEFRAME, $this->scheduled_timeframe);
        if ($this->isColumnModified(QuizzesPeer::SCHEDULED_TIMEFRAME_TEXT)) $criteria->add(QuizzesPeer::SCHEDULED_TIMEFRAME_TEXT, $this->scheduled_timeframe_text);
        if ($this->isColumnModified(QuizzesPeer::DISABLE_ANSWER_ONSELECT)) $criteria->add(QuizzesPeer::DISABLE_ANSWER_ONSELECT, $this->disable_answer_onselect);
        if ($this->isColumnModified(QuizzesPeer::AJAX_SHOW_CORRECT)) $criteria->add(QuizzesPeer::AJAX_SHOW_CORRECT, $this->ajax_show_correct);
        if ($this->isColumnModified(QuizzesPeer::QUIZ_VIEWS)) $criteria->add(QuizzesPeer::QUIZ_VIEWS, $this->quiz_views);
        if ($this->isColumnModified(QuizzesPeer::QUIZ_TAKEN)) $criteria->add(QuizzesPeer::QUIZ_TAKEN, $this->quiz_taken);
        if ($this->isColumnModified(QuizzesPeer::DELETED)) $criteria->add(QuizzesPeer::DELETED, $this->deleted);
        if ($this->isColumnModified(QuizzesPeer::QUIZ_DESCRIPTION)) $criteria->add(QuizzesPeer::QUIZ_DESCRIPTION, $this->quiz_description);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(QuizzesPeer::DATABASE_NAME);
        $criteria->add(QuizzesPeer::QUIZ_ID, $this->quiz_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getQuizId();
    }

    /**
     * Generic method to set the primary key (quiz_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setQuizId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getQuizId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Quizzes (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setQuizName($this->getQuizName());
        $copyObj->setMessageBefore($this->getMessageBefore());
        $copyObj->setMessageAfter($this->getMessageAfter());
        $copyObj->setMessageComment($this->getMessageComment());
        $copyObj->setMessageEndTemplate($this->getMessageEndTemplate());
        $copyObj->setUserEmailTemplate($this->getUserEmailTemplate());
        $copyObj->setAdminEmailTemplate($this->getAdminEmailTemplate());
        $copyObj->setSubmitButtonText($this->getSubmitButtonText());
        $copyObj->setNameFieldText($this->getNameFieldText());
        $copyObj->setBusinessFieldText($this->getBusinessFieldText());
        $copyObj->setEmailFieldText($this->getEmailFieldText());
        $copyObj->setPhoneFieldText($this->getPhoneFieldText());
        $copyObj->setCommentFieldText($this->getCommentFieldText());
        $copyObj->setEmailFromText($this->getEmailFromText());
        $copyObj->setQuestionAnswerTemplate($this->getQuestionAnswerTemplate());
        $copyObj->setLeaderboardTemplate($this->getLeaderboardTemplate());
        $copyObj->setSystem($this->getSystem());
        $copyObj->setRandomnessOrder($this->getRandomnessOrder());
        $copyObj->setLoggedinUserContact($this->getLoggedinUserContact());
        $copyObj->setShowScore($this->getShowScore());
        $copyObj->setSendUserEmail($this->getSendUserEmail());
        $copyObj->setSendAdminEmail($this->getSendAdminEmail());
        $copyObj->setContactInfoLocation($this->getContactInfoLocation());
        $copyObj->setUserName($this->getUserName());
        $copyObj->setUserComp($this->getUserComp());
        $copyObj->setUserEmail($this->getUserEmail());
        $copyObj->setUserPhone($this->getUserPhone());
        $copyObj->setAdminEmail($this->getAdminEmail());
        $copyObj->setCommentSection($this->getCommentSection());
        $copyObj->setQuestionFromTotal($this->getQuestionFromTotal());
        $copyObj->setTotalUserTries($this->getTotalUserTries());
        $copyObj->setTotalUserTriesText($this->getTotalUserTriesText());
        $copyObj->setCertificateTemplate($this->getCertificateTemplate());
        $copyObj->setSocialMedia($this->getSocialMedia());
        $copyObj->setSocialMediaText($this->getSocialMediaText());
        $copyObj->setPagination($this->getPagination());
        $copyObj->setPaginationText($this->getPaginationText());
        $copyObj->setTimerLimit($this->getTimerLimit());
        $copyObj->setQuizStye($this->getQuizStye());
        $copyObj->setQuestionNumbering($this->getQuestionNumbering());
        $copyObj->setQuizSettings($this->getQuizSettings());
        $copyObj->setThemeSelected($this->getThemeSelected());
        $copyObj->setLastActivity($this->getLastActivity());
        $copyObj->setRequireLogIn($this->getRequireLogIn());
        $copyObj->setRequireLogInText($this->getRequireLogInText());
        $copyObj->setLimitTotalEntries($this->getLimitTotalEntries());
        $copyObj->setLimitTotalEntriesText($this->getLimitTotalEntriesText());
        $copyObj->setScheduledTimeframe($this->getScheduledTimeframe());
        $copyObj->setScheduledTimeframeText($this->getScheduledTimeframeText());
        $copyObj->setDisableAnswerOnselect($this->getDisableAnswerOnselect());
        $copyObj->setAjaxShowCorrect($this->getAjaxShowCorrect());
        $copyObj->setQuizViews($this->getQuizViews());
        $copyObj->setQuizTaken($this->getQuizTaken());
        $copyObj->setDeleted($this->getDeleted());
        $copyObj->setQuizDescription($this->getQuizDescription());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setQuizId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Quizzes Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return QuizzesPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new QuizzesPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->quiz_id = null;
        $this->quiz_name = null;
        $this->message_before = null;
        $this->message_after = null;
        $this->message_comment = null;
        $this->message_end_template = null;
        $this->user_email_template = null;
        $this->admin_email_template = null;
        $this->submit_button_text = null;
        $this->name_field_text = null;
        $this->business_field_text = null;
        $this->email_field_text = null;
        $this->phone_field_text = null;
        $this->comment_field_text = null;
        $this->email_from_text = null;
        $this->question_answer_template = null;
        $this->leaderboard_template = null;
        $this->system = null;
        $this->randomness_order = null;
        $this->loggedin_user_contact = null;
        $this->show_score = null;
        $this->send_user_email = null;
        $this->send_admin_email = null;
        $this->contact_info_location = null;
        $this->user_name = null;
        $this->user_comp = null;
        $this->user_email = null;
        $this->user_phone = null;
        $this->admin_email = null;
        $this->comment_section = null;
        $this->question_from_total = null;
        $this->total_user_tries = null;
        $this->total_user_tries_text = null;
        $this->certificate_template = null;
        $this->social_media = null;
        $this->social_media_text = null;
        $this->pagination = null;
        $this->pagination_text = null;
        $this->timer_limit = null;
        $this->quiz_stye = null;
        $this->question_numbering = null;
        $this->quiz_settings = null;
        $this->theme_selected = null;
        $this->last_activity = null;
        $this->require_log_in = null;
        $this->require_log_in_text = null;
        $this->limit_total_entries = null;
        $this->limit_total_entries_text = null;
        $this->scheduled_timeframe = null;
        $this->scheduled_timeframe_text = null;
        $this->disable_answer_onselect = null;
        $this->ajax_show_correct = null;
        $this->quiz_views = null;
        $this->quiz_taken = null;
        $this->deleted = null;
        $this->quiz_description = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(QuizzesPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
