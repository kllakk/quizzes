<?php


/**
 * Base class that represents a query for the 'rfn_quizzes' table.
 *
 *
 *
 * @method QuizzesQuery orderByQuizId($order = Criteria::ASC) Order by the quiz_id column
 * @method QuizzesQuery orderByQuizName($order = Criteria::ASC) Order by the quiz_name column
 * @method QuizzesQuery orderByMessageBefore($order = Criteria::ASC) Order by the message_before column
 * @method QuizzesQuery orderByMessageAfter($order = Criteria::ASC) Order by the message_after column
 * @method QuizzesQuery orderByMessageComment($order = Criteria::ASC) Order by the message_comment column
 * @method QuizzesQuery orderByMessageEndTemplate($order = Criteria::ASC) Order by the message_end_template column
 * @method QuizzesQuery orderByUserEmailTemplate($order = Criteria::ASC) Order by the user_email_template column
 * @method QuizzesQuery orderByAdminEmailTemplate($order = Criteria::ASC) Order by the admin_email_template column
 * @method QuizzesQuery orderBySubmitButtonText($order = Criteria::ASC) Order by the submit_button_text column
 * @method QuizzesQuery orderByNameFieldText($order = Criteria::ASC) Order by the name_field_text column
 * @method QuizzesQuery orderByBusinessFieldText($order = Criteria::ASC) Order by the business_field_text column
 * @method QuizzesQuery orderByEmailFieldText($order = Criteria::ASC) Order by the email_field_text column
 * @method QuizzesQuery orderByPhoneFieldText($order = Criteria::ASC) Order by the phone_field_text column
 * @method QuizzesQuery orderByCommentFieldText($order = Criteria::ASC) Order by the comment_field_text column
 * @method QuizzesQuery orderByEmailFromText($order = Criteria::ASC) Order by the email_from_text column
 * @method QuizzesQuery orderByQuestionAnswerTemplate($order = Criteria::ASC) Order by the question_answer_template column
 * @method QuizzesQuery orderByLeaderboardTemplate($order = Criteria::ASC) Order by the leaderboard_template column
 * @method QuizzesQuery orderBySystem($order = Criteria::ASC) Order by the system column
 * @method QuizzesQuery orderByRandomnessOrder($order = Criteria::ASC) Order by the randomness_order column
 * @method QuizzesQuery orderByLoggedinUserContact($order = Criteria::ASC) Order by the loggedin_user_contact column
 * @method QuizzesQuery orderByShowScore($order = Criteria::ASC) Order by the show_score column
 * @method QuizzesQuery orderBySendUserEmail($order = Criteria::ASC) Order by the send_user_email column
 * @method QuizzesQuery orderBySendAdminEmail($order = Criteria::ASC) Order by the send_admin_email column
 * @method QuizzesQuery orderByContactInfoLocation($order = Criteria::ASC) Order by the contact_info_location column
 * @method QuizzesQuery orderByUserName($order = Criteria::ASC) Order by the user_name column
 * @method QuizzesQuery orderByUserComp($order = Criteria::ASC) Order by the user_comp column
 * @method QuizzesQuery orderByUserEmail($order = Criteria::ASC) Order by the user_email column
 * @method QuizzesQuery orderByUserPhone($order = Criteria::ASC) Order by the user_phone column
 * @method QuizzesQuery orderByAdminEmail($order = Criteria::ASC) Order by the admin_email column
 * @method QuizzesQuery orderByCommentSection($order = Criteria::ASC) Order by the comment_section column
 * @method QuizzesQuery orderByQuestionFromTotal($order = Criteria::ASC) Order by the question_from_total column
 * @method QuizzesQuery orderByTotalUserTries($order = Criteria::ASC) Order by the total_user_tries column
 * @method QuizzesQuery orderByTotalUserTriesText($order = Criteria::ASC) Order by the total_user_tries_text column
 * @method QuizzesQuery orderByCertificateTemplate($order = Criteria::ASC) Order by the certificate_template column
 * @method QuizzesQuery orderBySocialMedia($order = Criteria::ASC) Order by the social_media column
 * @method QuizzesQuery orderBySocialMediaText($order = Criteria::ASC) Order by the social_media_text column
 * @method QuizzesQuery orderByPagination($order = Criteria::ASC) Order by the pagination column
 * @method QuizzesQuery orderByPaginationText($order = Criteria::ASC) Order by the pagination_text column
 * @method QuizzesQuery orderByTimerLimit($order = Criteria::ASC) Order by the timer_limit column
 * @method QuizzesQuery orderByQuizStye($order = Criteria::ASC) Order by the quiz_stye column
 * @method QuizzesQuery orderByQuestionNumbering($order = Criteria::ASC) Order by the question_numbering column
 * @method QuizzesQuery orderByQuizSettings($order = Criteria::ASC) Order by the quiz_settings column
 * @method QuizzesQuery orderByThemeSelected($order = Criteria::ASC) Order by the theme_selected column
 * @method QuizzesQuery orderByLastActivity($order = Criteria::ASC) Order by the last_activity column
 * @method QuizzesQuery orderByRequireLogIn($order = Criteria::ASC) Order by the require_log_in column
 * @method QuizzesQuery orderByRequireLogInText($order = Criteria::ASC) Order by the require_log_in_text column
 * @method QuizzesQuery orderByLimitTotalEntries($order = Criteria::ASC) Order by the limit_total_entries column
 * @method QuizzesQuery orderByLimitTotalEntriesText($order = Criteria::ASC) Order by the limit_total_entries_text column
 * @method QuizzesQuery orderByScheduledTimeframe($order = Criteria::ASC) Order by the scheduled_timeframe column
 * @method QuizzesQuery orderByScheduledTimeframeText($order = Criteria::ASC) Order by the scheduled_timeframe_text column
 * @method QuizzesQuery orderByDisableAnswerOnselect($order = Criteria::ASC) Order by the disable_answer_onselect column
 * @method QuizzesQuery orderByAjaxShowCorrect($order = Criteria::ASC) Order by the ajax_show_correct column
 * @method QuizzesQuery orderByQuizViews($order = Criteria::ASC) Order by the quiz_views column
 * @method QuizzesQuery orderByQuizTaken($order = Criteria::ASC) Order by the quiz_taken column
 * @method QuizzesQuery orderByDeleted($order = Criteria::ASC) Order by the deleted column
 * @method QuizzesQuery orderByQuizDescription($order = Criteria::ASC) Order by the quiz_description column
 *
 * @method QuizzesQuery groupByQuizId() Group by the quiz_id column
 * @method QuizzesQuery groupByQuizName() Group by the quiz_name column
 * @method QuizzesQuery groupByMessageBefore() Group by the message_before column
 * @method QuizzesQuery groupByMessageAfter() Group by the message_after column
 * @method QuizzesQuery groupByMessageComment() Group by the message_comment column
 * @method QuizzesQuery groupByMessageEndTemplate() Group by the message_end_template column
 * @method QuizzesQuery groupByUserEmailTemplate() Group by the user_email_template column
 * @method QuizzesQuery groupByAdminEmailTemplate() Group by the admin_email_template column
 * @method QuizzesQuery groupBySubmitButtonText() Group by the submit_button_text column
 * @method QuizzesQuery groupByNameFieldText() Group by the name_field_text column
 * @method QuizzesQuery groupByBusinessFieldText() Group by the business_field_text column
 * @method QuizzesQuery groupByEmailFieldText() Group by the email_field_text column
 * @method QuizzesQuery groupByPhoneFieldText() Group by the phone_field_text column
 * @method QuizzesQuery groupByCommentFieldText() Group by the comment_field_text column
 * @method QuizzesQuery groupByEmailFromText() Group by the email_from_text column
 * @method QuizzesQuery groupByQuestionAnswerTemplate() Group by the question_answer_template column
 * @method QuizzesQuery groupByLeaderboardTemplate() Group by the leaderboard_template column
 * @method QuizzesQuery groupBySystem() Group by the system column
 * @method QuizzesQuery groupByRandomnessOrder() Group by the randomness_order column
 * @method QuizzesQuery groupByLoggedinUserContact() Group by the loggedin_user_contact column
 * @method QuizzesQuery groupByShowScore() Group by the show_score column
 * @method QuizzesQuery groupBySendUserEmail() Group by the send_user_email column
 * @method QuizzesQuery groupBySendAdminEmail() Group by the send_admin_email column
 * @method QuizzesQuery groupByContactInfoLocation() Group by the contact_info_location column
 * @method QuizzesQuery groupByUserName() Group by the user_name column
 * @method QuizzesQuery groupByUserComp() Group by the user_comp column
 * @method QuizzesQuery groupByUserEmail() Group by the user_email column
 * @method QuizzesQuery groupByUserPhone() Group by the user_phone column
 * @method QuizzesQuery groupByAdminEmail() Group by the admin_email column
 * @method QuizzesQuery groupByCommentSection() Group by the comment_section column
 * @method QuizzesQuery groupByQuestionFromTotal() Group by the question_from_total column
 * @method QuizzesQuery groupByTotalUserTries() Group by the total_user_tries column
 * @method QuizzesQuery groupByTotalUserTriesText() Group by the total_user_tries_text column
 * @method QuizzesQuery groupByCertificateTemplate() Group by the certificate_template column
 * @method QuizzesQuery groupBySocialMedia() Group by the social_media column
 * @method QuizzesQuery groupBySocialMediaText() Group by the social_media_text column
 * @method QuizzesQuery groupByPagination() Group by the pagination column
 * @method QuizzesQuery groupByPaginationText() Group by the pagination_text column
 * @method QuizzesQuery groupByTimerLimit() Group by the timer_limit column
 * @method QuizzesQuery groupByQuizStye() Group by the quiz_stye column
 * @method QuizzesQuery groupByQuestionNumbering() Group by the question_numbering column
 * @method QuizzesQuery groupByQuizSettings() Group by the quiz_settings column
 * @method QuizzesQuery groupByThemeSelected() Group by the theme_selected column
 * @method QuizzesQuery groupByLastActivity() Group by the last_activity column
 * @method QuizzesQuery groupByRequireLogIn() Group by the require_log_in column
 * @method QuizzesQuery groupByRequireLogInText() Group by the require_log_in_text column
 * @method QuizzesQuery groupByLimitTotalEntries() Group by the limit_total_entries column
 * @method QuizzesQuery groupByLimitTotalEntriesText() Group by the limit_total_entries_text column
 * @method QuizzesQuery groupByScheduledTimeframe() Group by the scheduled_timeframe column
 * @method QuizzesQuery groupByScheduledTimeframeText() Group by the scheduled_timeframe_text column
 * @method QuizzesQuery groupByDisableAnswerOnselect() Group by the disable_answer_onselect column
 * @method QuizzesQuery groupByAjaxShowCorrect() Group by the ajax_show_correct column
 * @method QuizzesQuery groupByQuizViews() Group by the quiz_views column
 * @method QuizzesQuery groupByQuizTaken() Group by the quiz_taken column
 * @method QuizzesQuery groupByDeleted() Group by the deleted column
 * @method QuizzesQuery groupByQuizDescription() Group by the quiz_description column
 *
 * @method QuizzesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method QuizzesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method QuizzesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Quizzes findOne(PropelPDO $con = null) Return the first Quizzes matching the query
 * @method Quizzes findOneOrCreate(PropelPDO $con = null) Return the first Quizzes matching the query, or a new Quizzes object populated from the query conditions when no match is found
 *
 * @method Quizzes findOneByQuizName(string $quiz_name) Return the first Quizzes filtered by the quiz_name column
 * @method Quizzes findOneByMessageBefore(string $message_before) Return the first Quizzes filtered by the message_before column
 * @method Quizzes findOneByMessageAfter(string $message_after) Return the first Quizzes filtered by the message_after column
 * @method Quizzes findOneByMessageComment(string $message_comment) Return the first Quizzes filtered by the message_comment column
 * @method Quizzes findOneByMessageEndTemplate(string $message_end_template) Return the first Quizzes filtered by the message_end_template column
 * @method Quizzes findOneByUserEmailTemplate(string $user_email_template) Return the first Quizzes filtered by the user_email_template column
 * @method Quizzes findOneByAdminEmailTemplate(string $admin_email_template) Return the first Quizzes filtered by the admin_email_template column
 * @method Quizzes findOneBySubmitButtonText(string $submit_button_text) Return the first Quizzes filtered by the submit_button_text column
 * @method Quizzes findOneByNameFieldText(string $name_field_text) Return the first Quizzes filtered by the name_field_text column
 * @method Quizzes findOneByBusinessFieldText(string $business_field_text) Return the first Quizzes filtered by the business_field_text column
 * @method Quizzes findOneByEmailFieldText(string $email_field_text) Return the first Quizzes filtered by the email_field_text column
 * @method Quizzes findOneByPhoneFieldText(string $phone_field_text) Return the first Quizzes filtered by the phone_field_text column
 * @method Quizzes findOneByCommentFieldText(string $comment_field_text) Return the first Quizzes filtered by the comment_field_text column
 * @method Quizzes findOneByEmailFromText(string $email_from_text) Return the first Quizzes filtered by the email_from_text column
 * @method Quizzes findOneByQuestionAnswerTemplate(string $question_answer_template) Return the first Quizzes filtered by the question_answer_template column
 * @method Quizzes findOneByLeaderboardTemplate(string $leaderboard_template) Return the first Quizzes filtered by the leaderboard_template column
 * @method Quizzes findOneBySystem(int $system) Return the first Quizzes filtered by the system column
 * @method Quizzes findOneByRandomnessOrder(int $randomness_order) Return the first Quizzes filtered by the randomness_order column
 * @method Quizzes findOneByLoggedinUserContact(int $loggedin_user_contact) Return the first Quizzes filtered by the loggedin_user_contact column
 * @method Quizzes findOneByShowScore(int $show_score) Return the first Quizzes filtered by the show_score column
 * @method Quizzes findOneBySendUserEmail(int $send_user_email) Return the first Quizzes filtered by the send_user_email column
 * @method Quizzes findOneBySendAdminEmail(int $send_admin_email) Return the first Quizzes filtered by the send_admin_email column
 * @method Quizzes findOneByContactInfoLocation(int $contact_info_location) Return the first Quizzes filtered by the contact_info_location column
 * @method Quizzes findOneByUserName(int $user_name) Return the first Quizzes filtered by the user_name column
 * @method Quizzes findOneByUserComp(int $user_comp) Return the first Quizzes filtered by the user_comp column
 * @method Quizzes findOneByUserEmail(int $user_email) Return the first Quizzes filtered by the user_email column
 * @method Quizzes findOneByUserPhone(int $user_phone) Return the first Quizzes filtered by the user_phone column
 * @method Quizzes findOneByAdminEmail(string $admin_email) Return the first Quizzes filtered by the admin_email column
 * @method Quizzes findOneByCommentSection(int $comment_section) Return the first Quizzes filtered by the comment_section column
 * @method Quizzes findOneByQuestionFromTotal(int $question_from_total) Return the first Quizzes filtered by the question_from_total column
 * @method Quizzes findOneByTotalUserTries(int $total_user_tries) Return the first Quizzes filtered by the total_user_tries column
 * @method Quizzes findOneByTotalUserTriesText(string $total_user_tries_text) Return the first Quizzes filtered by the total_user_tries_text column
 * @method Quizzes findOneByCertificateTemplate(string $certificate_template) Return the first Quizzes filtered by the certificate_template column
 * @method Quizzes findOneBySocialMedia(int $social_media) Return the first Quizzes filtered by the social_media column
 * @method Quizzes findOneBySocialMediaText(string $social_media_text) Return the first Quizzes filtered by the social_media_text column
 * @method Quizzes findOneByPagination(int $pagination) Return the first Quizzes filtered by the pagination column
 * @method Quizzes findOneByPaginationText(string $pagination_text) Return the first Quizzes filtered by the pagination_text column
 * @method Quizzes findOneByTimerLimit(int $timer_limit) Return the first Quizzes filtered by the timer_limit column
 * @method Quizzes findOneByQuizStye(string $quiz_stye) Return the first Quizzes filtered by the quiz_stye column
 * @method Quizzes findOneByQuestionNumbering(int $question_numbering) Return the first Quizzes filtered by the question_numbering column
 * @method Quizzes findOneByQuizSettings(string $quiz_settings) Return the first Quizzes filtered by the quiz_settings column
 * @method Quizzes findOneByThemeSelected(string $theme_selected) Return the first Quizzes filtered by the theme_selected column
 * @method Quizzes findOneByLastActivity(string $last_activity) Return the first Quizzes filtered by the last_activity column
 * @method Quizzes findOneByRequireLogIn(int $require_log_in) Return the first Quizzes filtered by the require_log_in column
 * @method Quizzes findOneByRequireLogInText(string $require_log_in_text) Return the first Quizzes filtered by the require_log_in_text column
 * @method Quizzes findOneByLimitTotalEntries(int $limit_total_entries) Return the first Quizzes filtered by the limit_total_entries column
 * @method Quizzes findOneByLimitTotalEntriesText(string $limit_total_entries_text) Return the first Quizzes filtered by the limit_total_entries_text column
 * @method Quizzes findOneByScheduledTimeframe(string $scheduled_timeframe) Return the first Quizzes filtered by the scheduled_timeframe column
 * @method Quizzes findOneByScheduledTimeframeText(string $scheduled_timeframe_text) Return the first Quizzes filtered by the scheduled_timeframe_text column
 * @method Quizzes findOneByDisableAnswerOnselect(int $disable_answer_onselect) Return the first Quizzes filtered by the disable_answer_onselect column
 * @method Quizzes findOneByAjaxShowCorrect(int $ajax_show_correct) Return the first Quizzes filtered by the ajax_show_correct column
 * @method Quizzes findOneByQuizViews(int $quiz_views) Return the first Quizzes filtered by the quiz_views column
 * @method Quizzes findOneByQuizTaken(int $quiz_taken) Return the first Quizzes filtered by the quiz_taken column
 * @method Quizzes findOneByDeleted(int $deleted) Return the first Quizzes filtered by the deleted column
 * @method Quizzes findOneByQuizDescription(string $quiz_description) Return the first Quizzes filtered by the quiz_description column
 *
 * @method array findByQuizId(int $quiz_id) Return Quizzes objects filtered by the quiz_id column
 * @method array findByQuizName(string $quiz_name) Return Quizzes objects filtered by the quiz_name column
 * @method array findByMessageBefore(string $message_before) Return Quizzes objects filtered by the message_before column
 * @method array findByMessageAfter(string $message_after) Return Quizzes objects filtered by the message_after column
 * @method array findByMessageComment(string $message_comment) Return Quizzes objects filtered by the message_comment column
 * @method array findByMessageEndTemplate(string $message_end_template) Return Quizzes objects filtered by the message_end_template column
 * @method array findByUserEmailTemplate(string $user_email_template) Return Quizzes objects filtered by the user_email_template column
 * @method array findByAdminEmailTemplate(string $admin_email_template) Return Quizzes objects filtered by the admin_email_template column
 * @method array findBySubmitButtonText(string $submit_button_text) Return Quizzes objects filtered by the submit_button_text column
 * @method array findByNameFieldText(string $name_field_text) Return Quizzes objects filtered by the name_field_text column
 * @method array findByBusinessFieldText(string $business_field_text) Return Quizzes objects filtered by the business_field_text column
 * @method array findByEmailFieldText(string $email_field_text) Return Quizzes objects filtered by the email_field_text column
 * @method array findByPhoneFieldText(string $phone_field_text) Return Quizzes objects filtered by the phone_field_text column
 * @method array findByCommentFieldText(string $comment_field_text) Return Quizzes objects filtered by the comment_field_text column
 * @method array findByEmailFromText(string $email_from_text) Return Quizzes objects filtered by the email_from_text column
 * @method array findByQuestionAnswerTemplate(string $question_answer_template) Return Quizzes objects filtered by the question_answer_template column
 * @method array findByLeaderboardTemplate(string $leaderboard_template) Return Quizzes objects filtered by the leaderboard_template column
 * @method array findBySystem(int $system) Return Quizzes objects filtered by the system column
 * @method array findByRandomnessOrder(int $randomness_order) Return Quizzes objects filtered by the randomness_order column
 * @method array findByLoggedinUserContact(int $loggedin_user_contact) Return Quizzes objects filtered by the loggedin_user_contact column
 * @method array findByShowScore(int $show_score) Return Quizzes objects filtered by the show_score column
 * @method array findBySendUserEmail(int $send_user_email) Return Quizzes objects filtered by the send_user_email column
 * @method array findBySendAdminEmail(int $send_admin_email) Return Quizzes objects filtered by the send_admin_email column
 * @method array findByContactInfoLocation(int $contact_info_location) Return Quizzes objects filtered by the contact_info_location column
 * @method array findByUserName(int $user_name) Return Quizzes objects filtered by the user_name column
 * @method array findByUserComp(int $user_comp) Return Quizzes objects filtered by the user_comp column
 * @method array findByUserEmail(int $user_email) Return Quizzes objects filtered by the user_email column
 * @method array findByUserPhone(int $user_phone) Return Quizzes objects filtered by the user_phone column
 * @method array findByAdminEmail(string $admin_email) Return Quizzes objects filtered by the admin_email column
 * @method array findByCommentSection(int $comment_section) Return Quizzes objects filtered by the comment_section column
 * @method array findByQuestionFromTotal(int $question_from_total) Return Quizzes objects filtered by the question_from_total column
 * @method array findByTotalUserTries(int $total_user_tries) Return Quizzes objects filtered by the total_user_tries column
 * @method array findByTotalUserTriesText(string $total_user_tries_text) Return Quizzes objects filtered by the total_user_tries_text column
 * @method array findByCertificateTemplate(string $certificate_template) Return Quizzes objects filtered by the certificate_template column
 * @method array findBySocialMedia(int $social_media) Return Quizzes objects filtered by the social_media column
 * @method array findBySocialMediaText(string $social_media_text) Return Quizzes objects filtered by the social_media_text column
 * @method array findByPagination(int $pagination) Return Quizzes objects filtered by the pagination column
 * @method array findByPaginationText(string $pagination_text) Return Quizzes objects filtered by the pagination_text column
 * @method array findByTimerLimit(int $timer_limit) Return Quizzes objects filtered by the timer_limit column
 * @method array findByQuizStye(string $quiz_stye) Return Quizzes objects filtered by the quiz_stye column
 * @method array findByQuestionNumbering(int $question_numbering) Return Quizzes objects filtered by the question_numbering column
 * @method array findByQuizSettings(string $quiz_settings) Return Quizzes objects filtered by the quiz_settings column
 * @method array findByThemeSelected(string $theme_selected) Return Quizzes objects filtered by the theme_selected column
 * @method array findByLastActivity(string $last_activity) Return Quizzes objects filtered by the last_activity column
 * @method array findByRequireLogIn(int $require_log_in) Return Quizzes objects filtered by the require_log_in column
 * @method array findByRequireLogInText(string $require_log_in_text) Return Quizzes objects filtered by the require_log_in_text column
 * @method array findByLimitTotalEntries(int $limit_total_entries) Return Quizzes objects filtered by the limit_total_entries column
 * @method array findByLimitTotalEntriesText(string $limit_total_entries_text) Return Quizzes objects filtered by the limit_total_entries_text column
 * @method array findByScheduledTimeframe(string $scheduled_timeframe) Return Quizzes objects filtered by the scheduled_timeframe column
 * @method array findByScheduledTimeframeText(string $scheduled_timeframe_text) Return Quizzes objects filtered by the scheduled_timeframe_text column
 * @method array findByDisableAnswerOnselect(int $disable_answer_onselect) Return Quizzes objects filtered by the disable_answer_onselect column
 * @method array findByAjaxShowCorrect(int $ajax_show_correct) Return Quizzes objects filtered by the ajax_show_correct column
 * @method array findByQuizViews(int $quiz_views) Return Quizzes objects filtered by the quiz_views column
 * @method array findByQuizTaken(int $quiz_taken) Return Quizzes objects filtered by the quiz_taken column
 * @method array findByDeleted(int $deleted) Return Quizzes objects filtered by the deleted column
 * @method array findByQuizDescription(string $quiz_description) Return Quizzes objects filtered by the quiz_description column
 *
 * @package    propel.generator.quizzes.om
 */
abstract class BaseQuizzesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseQuizzesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'rfn_new2';
        }
        if (null === $modelName) {
            $modelName = 'Quizzes';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new QuizzesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   QuizzesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return QuizzesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof QuizzesQuery) {
            return $criteria;
        }
        $query = new QuizzesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Quizzes|Quizzes[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = QuizzesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Quizzes A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByQuizId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Quizzes A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `quiz_id`, `quiz_name`, `message_before`, `message_after`, `message_comment`, `message_end_template`, `user_email_template`, `admin_email_template`, `submit_button_text`, `name_field_text`, `business_field_text`, `email_field_text`, `phone_field_text`, `comment_field_text`, `email_from_text`, `question_answer_template`, `leaderboard_template`, `system`, `randomness_order`, `loggedin_user_contact`, `show_score`, `send_user_email`, `send_admin_email`, `contact_info_location`, `user_name`, `user_comp`, `user_email`, `user_phone`, `admin_email`, `comment_section`, `question_from_total`, `total_user_tries`, `total_user_tries_text`, `certificate_template`, `social_media`, `social_media_text`, `pagination`, `pagination_text`, `timer_limit`, `quiz_stye`, `question_numbering`, `quiz_settings`, `theme_selected`, `last_activity`, `require_log_in`, `require_log_in_text`, `limit_total_entries`, `limit_total_entries_text`, `scheduled_timeframe`, `scheduled_timeframe_text`, `disable_answer_onselect`, `ajax_show_correct`, `quiz_views`, `quiz_taken`, `deleted`, `quiz_description` FROM `rfn_quizzes` WHERE `quiz_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Quizzes();
            $obj->hydrate($row);
            QuizzesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Quizzes|Quizzes[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Quizzes[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(QuizzesPeer::QUIZ_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(QuizzesPeer::QUIZ_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the quiz_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizId(1234); // WHERE quiz_id = 1234
     * $query->filterByQuizId(array(12, 34)); // WHERE quiz_id IN (12, 34)
     * $query->filterByQuizId(array('min' => 12)); // WHERE quiz_id >= 12
     * $query->filterByQuizId(array('max' => 12)); // WHERE quiz_id <= 12
     * </code>
     *
     * @param     mixed $quizId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByQuizId($quizId = null, $comparison = null)
    {
        if (is_array($quizId)) {
            $useMinMax = false;
            if (isset($quizId['min'])) {
                $this->addUsingAlias(QuizzesPeer::QUIZ_ID, $quizId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quizId['max'])) {
                $this->addUsingAlias(QuizzesPeer::QUIZ_ID, $quizId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::QUIZ_ID, $quizId, $comparison);
    }

    /**
     * Filter the query on the quiz_name column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizName('fooValue');   // WHERE quiz_name = 'fooValue'
     * $query->filterByQuizName('%fooValue%'); // WHERE quiz_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $quizName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByQuizName($quizName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quizName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $quizName)) {
                $quizName = str_replace('*', '%', $quizName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::QUIZ_NAME, $quizName, $comparison);
    }

    /**
     * Filter the query on the message_before column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageBefore('fooValue');   // WHERE message_before = 'fooValue'
     * $query->filterByMessageBefore('%fooValue%'); // WHERE message_before LIKE '%fooValue%'
     * </code>
     *
     * @param     string $messageBefore The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByMessageBefore($messageBefore = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($messageBefore)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $messageBefore)) {
                $messageBefore = str_replace('*', '%', $messageBefore);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::MESSAGE_BEFORE, $messageBefore, $comparison);
    }

    /**
     * Filter the query on the message_after column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageAfter('fooValue');   // WHERE message_after = 'fooValue'
     * $query->filterByMessageAfter('%fooValue%'); // WHERE message_after LIKE '%fooValue%'
     * </code>
     *
     * @param     string $messageAfter The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByMessageAfter($messageAfter = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($messageAfter)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $messageAfter)) {
                $messageAfter = str_replace('*', '%', $messageAfter);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::MESSAGE_AFTER, $messageAfter, $comparison);
    }

    /**
     * Filter the query on the message_comment column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageComment('fooValue');   // WHERE message_comment = 'fooValue'
     * $query->filterByMessageComment('%fooValue%'); // WHERE message_comment LIKE '%fooValue%'
     * </code>
     *
     * @param     string $messageComment The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByMessageComment($messageComment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($messageComment)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $messageComment)) {
                $messageComment = str_replace('*', '%', $messageComment);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::MESSAGE_COMMENT, $messageComment, $comparison);
    }

    /**
     * Filter the query on the message_end_template column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageEndTemplate('fooValue');   // WHERE message_end_template = 'fooValue'
     * $query->filterByMessageEndTemplate('%fooValue%'); // WHERE message_end_template LIKE '%fooValue%'
     * </code>
     *
     * @param     string $messageEndTemplate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByMessageEndTemplate($messageEndTemplate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($messageEndTemplate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $messageEndTemplate)) {
                $messageEndTemplate = str_replace('*', '%', $messageEndTemplate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::MESSAGE_END_TEMPLATE, $messageEndTemplate, $comparison);
    }

    /**
     * Filter the query on the user_email_template column
     *
     * Example usage:
     * <code>
     * $query->filterByUserEmailTemplate('fooValue');   // WHERE user_email_template = 'fooValue'
     * $query->filterByUserEmailTemplate('%fooValue%'); // WHERE user_email_template LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userEmailTemplate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByUserEmailTemplate($userEmailTemplate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userEmailTemplate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $userEmailTemplate)) {
                $userEmailTemplate = str_replace('*', '%', $userEmailTemplate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::USER_EMAIL_TEMPLATE, $userEmailTemplate, $comparison);
    }

    /**
     * Filter the query on the admin_email_template column
     *
     * Example usage:
     * <code>
     * $query->filterByAdminEmailTemplate('fooValue');   // WHERE admin_email_template = 'fooValue'
     * $query->filterByAdminEmailTemplate('%fooValue%'); // WHERE admin_email_template LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adminEmailTemplate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByAdminEmailTemplate($adminEmailTemplate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adminEmailTemplate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $adminEmailTemplate)) {
                $adminEmailTemplate = str_replace('*', '%', $adminEmailTemplate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::ADMIN_EMAIL_TEMPLATE, $adminEmailTemplate, $comparison);
    }

    /**
     * Filter the query on the submit_button_text column
     *
     * Example usage:
     * <code>
     * $query->filterBySubmitButtonText('fooValue');   // WHERE submit_button_text = 'fooValue'
     * $query->filterBySubmitButtonText('%fooValue%'); // WHERE submit_button_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $submitButtonText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterBySubmitButtonText($submitButtonText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($submitButtonText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $submitButtonText)) {
                $submitButtonText = str_replace('*', '%', $submitButtonText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::SUBMIT_BUTTON_TEXT, $submitButtonText, $comparison);
    }

    /**
     * Filter the query on the name_field_text column
     *
     * Example usage:
     * <code>
     * $query->filterByNameFieldText('fooValue');   // WHERE name_field_text = 'fooValue'
     * $query->filterByNameFieldText('%fooValue%'); // WHERE name_field_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nameFieldText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByNameFieldText($nameFieldText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nameFieldText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nameFieldText)) {
                $nameFieldText = str_replace('*', '%', $nameFieldText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::NAME_FIELD_TEXT, $nameFieldText, $comparison);
    }

    /**
     * Filter the query on the business_field_text column
     *
     * Example usage:
     * <code>
     * $query->filterByBusinessFieldText('fooValue');   // WHERE business_field_text = 'fooValue'
     * $query->filterByBusinessFieldText('%fooValue%'); // WHERE business_field_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $businessFieldText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByBusinessFieldText($businessFieldText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($businessFieldText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $businessFieldText)) {
                $businessFieldText = str_replace('*', '%', $businessFieldText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::BUSINESS_FIELD_TEXT, $businessFieldText, $comparison);
    }

    /**
     * Filter the query on the email_field_text column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailFieldText('fooValue');   // WHERE email_field_text = 'fooValue'
     * $query->filterByEmailFieldText('%fooValue%'); // WHERE email_field_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailFieldText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByEmailFieldText($emailFieldText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailFieldText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $emailFieldText)) {
                $emailFieldText = str_replace('*', '%', $emailFieldText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::EMAIL_FIELD_TEXT, $emailFieldText, $comparison);
    }

    /**
     * Filter the query on the phone_field_text column
     *
     * Example usage:
     * <code>
     * $query->filterByPhoneFieldText('fooValue');   // WHERE phone_field_text = 'fooValue'
     * $query->filterByPhoneFieldText('%fooValue%'); // WHERE phone_field_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phoneFieldText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByPhoneFieldText($phoneFieldText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phoneFieldText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phoneFieldText)) {
                $phoneFieldText = str_replace('*', '%', $phoneFieldText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::PHONE_FIELD_TEXT, $phoneFieldText, $comparison);
    }

    /**
     * Filter the query on the comment_field_text column
     *
     * Example usage:
     * <code>
     * $query->filterByCommentFieldText('fooValue');   // WHERE comment_field_text = 'fooValue'
     * $query->filterByCommentFieldText('%fooValue%'); // WHERE comment_field_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $commentFieldText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByCommentFieldText($commentFieldText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($commentFieldText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $commentFieldText)) {
                $commentFieldText = str_replace('*', '%', $commentFieldText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::COMMENT_FIELD_TEXT, $commentFieldText, $comparison);
    }

    /**
     * Filter the query on the email_from_text column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailFromText('fooValue');   // WHERE email_from_text = 'fooValue'
     * $query->filterByEmailFromText('%fooValue%'); // WHERE email_from_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailFromText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByEmailFromText($emailFromText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailFromText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $emailFromText)) {
                $emailFromText = str_replace('*', '%', $emailFromText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::EMAIL_FROM_TEXT, $emailFromText, $comparison);
    }

    /**
     * Filter the query on the question_answer_template column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionAnswerTemplate('fooValue');   // WHERE question_answer_template = 'fooValue'
     * $query->filterByQuestionAnswerTemplate('%fooValue%'); // WHERE question_answer_template LIKE '%fooValue%'
     * </code>
     *
     * @param     string $questionAnswerTemplate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByQuestionAnswerTemplate($questionAnswerTemplate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($questionAnswerTemplate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $questionAnswerTemplate)) {
                $questionAnswerTemplate = str_replace('*', '%', $questionAnswerTemplate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::QUESTION_ANSWER_TEMPLATE, $questionAnswerTemplate, $comparison);
    }

    /**
     * Filter the query on the leaderboard_template column
     *
     * Example usage:
     * <code>
     * $query->filterByLeaderboardTemplate('fooValue');   // WHERE leaderboard_template = 'fooValue'
     * $query->filterByLeaderboardTemplate('%fooValue%'); // WHERE leaderboard_template LIKE '%fooValue%'
     * </code>
     *
     * @param     string $leaderboardTemplate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByLeaderboardTemplate($leaderboardTemplate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($leaderboardTemplate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $leaderboardTemplate)) {
                $leaderboardTemplate = str_replace('*', '%', $leaderboardTemplate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::LEADERBOARD_TEMPLATE, $leaderboardTemplate, $comparison);
    }

    /**
     * Filter the query on the system column
     *
     * Example usage:
     * <code>
     * $query->filterBySystem(1234); // WHERE system = 1234
     * $query->filterBySystem(array(12, 34)); // WHERE system IN (12, 34)
     * $query->filterBySystem(array('min' => 12)); // WHERE system >= 12
     * $query->filterBySystem(array('max' => 12)); // WHERE system <= 12
     * </code>
     *
     * @param     mixed $system The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterBySystem($system = null, $comparison = null)
    {
        if (is_array($system)) {
            $useMinMax = false;
            if (isset($system['min'])) {
                $this->addUsingAlias(QuizzesPeer::SYSTEM, $system['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($system['max'])) {
                $this->addUsingAlias(QuizzesPeer::SYSTEM, $system['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::SYSTEM, $system, $comparison);
    }

    /**
     * Filter the query on the randomness_order column
     *
     * Example usage:
     * <code>
     * $query->filterByRandomnessOrder(1234); // WHERE randomness_order = 1234
     * $query->filterByRandomnessOrder(array(12, 34)); // WHERE randomness_order IN (12, 34)
     * $query->filterByRandomnessOrder(array('min' => 12)); // WHERE randomness_order >= 12
     * $query->filterByRandomnessOrder(array('max' => 12)); // WHERE randomness_order <= 12
     * </code>
     *
     * @param     mixed $randomnessOrder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByRandomnessOrder($randomnessOrder = null, $comparison = null)
    {
        if (is_array($randomnessOrder)) {
            $useMinMax = false;
            if (isset($randomnessOrder['min'])) {
                $this->addUsingAlias(QuizzesPeer::RANDOMNESS_ORDER, $randomnessOrder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($randomnessOrder['max'])) {
                $this->addUsingAlias(QuizzesPeer::RANDOMNESS_ORDER, $randomnessOrder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::RANDOMNESS_ORDER, $randomnessOrder, $comparison);
    }

    /**
     * Filter the query on the loggedin_user_contact column
     *
     * Example usage:
     * <code>
     * $query->filterByLoggedinUserContact(1234); // WHERE loggedin_user_contact = 1234
     * $query->filterByLoggedinUserContact(array(12, 34)); // WHERE loggedin_user_contact IN (12, 34)
     * $query->filterByLoggedinUserContact(array('min' => 12)); // WHERE loggedin_user_contact >= 12
     * $query->filterByLoggedinUserContact(array('max' => 12)); // WHERE loggedin_user_contact <= 12
     * </code>
     *
     * @param     mixed $loggedinUserContact The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByLoggedinUserContact($loggedinUserContact = null, $comparison = null)
    {
        if (is_array($loggedinUserContact)) {
            $useMinMax = false;
            if (isset($loggedinUserContact['min'])) {
                $this->addUsingAlias(QuizzesPeer::LOGGEDIN_USER_CONTACT, $loggedinUserContact['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($loggedinUserContact['max'])) {
                $this->addUsingAlias(QuizzesPeer::LOGGEDIN_USER_CONTACT, $loggedinUserContact['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::LOGGEDIN_USER_CONTACT, $loggedinUserContact, $comparison);
    }

    /**
     * Filter the query on the show_score column
     *
     * Example usage:
     * <code>
     * $query->filterByShowScore(1234); // WHERE show_score = 1234
     * $query->filterByShowScore(array(12, 34)); // WHERE show_score IN (12, 34)
     * $query->filterByShowScore(array('min' => 12)); // WHERE show_score >= 12
     * $query->filterByShowScore(array('max' => 12)); // WHERE show_score <= 12
     * </code>
     *
     * @param     mixed $showScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByShowScore($showScore = null, $comparison = null)
    {
        if (is_array($showScore)) {
            $useMinMax = false;
            if (isset($showScore['min'])) {
                $this->addUsingAlias(QuizzesPeer::SHOW_SCORE, $showScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($showScore['max'])) {
                $this->addUsingAlias(QuizzesPeer::SHOW_SCORE, $showScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::SHOW_SCORE, $showScore, $comparison);
    }

    /**
     * Filter the query on the send_user_email column
     *
     * Example usage:
     * <code>
     * $query->filterBySendUserEmail(1234); // WHERE send_user_email = 1234
     * $query->filterBySendUserEmail(array(12, 34)); // WHERE send_user_email IN (12, 34)
     * $query->filterBySendUserEmail(array('min' => 12)); // WHERE send_user_email >= 12
     * $query->filterBySendUserEmail(array('max' => 12)); // WHERE send_user_email <= 12
     * </code>
     *
     * @param     mixed $sendUserEmail The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterBySendUserEmail($sendUserEmail = null, $comparison = null)
    {
        if (is_array($sendUserEmail)) {
            $useMinMax = false;
            if (isset($sendUserEmail['min'])) {
                $this->addUsingAlias(QuizzesPeer::SEND_USER_EMAIL, $sendUserEmail['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sendUserEmail['max'])) {
                $this->addUsingAlias(QuizzesPeer::SEND_USER_EMAIL, $sendUserEmail['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::SEND_USER_EMAIL, $sendUserEmail, $comparison);
    }

    /**
     * Filter the query on the send_admin_email column
     *
     * Example usage:
     * <code>
     * $query->filterBySendAdminEmail(1234); // WHERE send_admin_email = 1234
     * $query->filterBySendAdminEmail(array(12, 34)); // WHERE send_admin_email IN (12, 34)
     * $query->filterBySendAdminEmail(array('min' => 12)); // WHERE send_admin_email >= 12
     * $query->filterBySendAdminEmail(array('max' => 12)); // WHERE send_admin_email <= 12
     * </code>
     *
     * @param     mixed $sendAdminEmail The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterBySendAdminEmail($sendAdminEmail = null, $comparison = null)
    {
        if (is_array($sendAdminEmail)) {
            $useMinMax = false;
            if (isset($sendAdminEmail['min'])) {
                $this->addUsingAlias(QuizzesPeer::SEND_ADMIN_EMAIL, $sendAdminEmail['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sendAdminEmail['max'])) {
                $this->addUsingAlias(QuizzesPeer::SEND_ADMIN_EMAIL, $sendAdminEmail['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::SEND_ADMIN_EMAIL, $sendAdminEmail, $comparison);
    }

    /**
     * Filter the query on the contact_info_location column
     *
     * Example usage:
     * <code>
     * $query->filterByContactInfoLocation(1234); // WHERE contact_info_location = 1234
     * $query->filterByContactInfoLocation(array(12, 34)); // WHERE contact_info_location IN (12, 34)
     * $query->filterByContactInfoLocation(array('min' => 12)); // WHERE contact_info_location >= 12
     * $query->filterByContactInfoLocation(array('max' => 12)); // WHERE contact_info_location <= 12
     * </code>
     *
     * @param     mixed $contactInfoLocation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByContactInfoLocation($contactInfoLocation = null, $comparison = null)
    {
        if (is_array($contactInfoLocation)) {
            $useMinMax = false;
            if (isset($contactInfoLocation['min'])) {
                $this->addUsingAlias(QuizzesPeer::CONTACT_INFO_LOCATION, $contactInfoLocation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contactInfoLocation['max'])) {
                $this->addUsingAlias(QuizzesPeer::CONTACT_INFO_LOCATION, $contactInfoLocation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::CONTACT_INFO_LOCATION, $contactInfoLocation, $comparison);
    }

    /**
     * Filter the query on the user_name column
     *
     * Example usage:
     * <code>
     * $query->filterByUserName(1234); // WHERE user_name = 1234
     * $query->filterByUserName(array(12, 34)); // WHERE user_name IN (12, 34)
     * $query->filterByUserName(array('min' => 12)); // WHERE user_name >= 12
     * $query->filterByUserName(array('max' => 12)); // WHERE user_name <= 12
     * </code>
     *
     * @param     mixed $userName The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByUserName($userName = null, $comparison = null)
    {
        if (is_array($userName)) {
            $useMinMax = false;
            if (isset($userName['min'])) {
                $this->addUsingAlias(QuizzesPeer::USER_NAME, $userName['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userName['max'])) {
                $this->addUsingAlias(QuizzesPeer::USER_NAME, $userName['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::USER_NAME, $userName, $comparison);
    }

    /**
     * Filter the query on the user_comp column
     *
     * Example usage:
     * <code>
     * $query->filterByUserComp(1234); // WHERE user_comp = 1234
     * $query->filterByUserComp(array(12, 34)); // WHERE user_comp IN (12, 34)
     * $query->filterByUserComp(array('min' => 12)); // WHERE user_comp >= 12
     * $query->filterByUserComp(array('max' => 12)); // WHERE user_comp <= 12
     * </code>
     *
     * @param     mixed $userComp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByUserComp($userComp = null, $comparison = null)
    {
        if (is_array($userComp)) {
            $useMinMax = false;
            if (isset($userComp['min'])) {
                $this->addUsingAlias(QuizzesPeer::USER_COMP, $userComp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userComp['max'])) {
                $this->addUsingAlias(QuizzesPeer::USER_COMP, $userComp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::USER_COMP, $userComp, $comparison);
    }

    /**
     * Filter the query on the user_email column
     *
     * Example usage:
     * <code>
     * $query->filterByUserEmail(1234); // WHERE user_email = 1234
     * $query->filterByUserEmail(array(12, 34)); // WHERE user_email IN (12, 34)
     * $query->filterByUserEmail(array('min' => 12)); // WHERE user_email >= 12
     * $query->filterByUserEmail(array('max' => 12)); // WHERE user_email <= 12
     * </code>
     *
     * @param     mixed $userEmail The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByUserEmail($userEmail = null, $comparison = null)
    {
        if (is_array($userEmail)) {
            $useMinMax = false;
            if (isset($userEmail['min'])) {
                $this->addUsingAlias(QuizzesPeer::USER_EMAIL, $userEmail['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userEmail['max'])) {
                $this->addUsingAlias(QuizzesPeer::USER_EMAIL, $userEmail['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::USER_EMAIL, $userEmail, $comparison);
    }

    /**
     * Filter the query on the user_phone column
     *
     * Example usage:
     * <code>
     * $query->filterByUserPhone(1234); // WHERE user_phone = 1234
     * $query->filterByUserPhone(array(12, 34)); // WHERE user_phone IN (12, 34)
     * $query->filterByUserPhone(array('min' => 12)); // WHERE user_phone >= 12
     * $query->filterByUserPhone(array('max' => 12)); // WHERE user_phone <= 12
     * </code>
     *
     * @param     mixed $userPhone The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByUserPhone($userPhone = null, $comparison = null)
    {
        if (is_array($userPhone)) {
            $useMinMax = false;
            if (isset($userPhone['min'])) {
                $this->addUsingAlias(QuizzesPeer::USER_PHONE, $userPhone['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userPhone['max'])) {
                $this->addUsingAlias(QuizzesPeer::USER_PHONE, $userPhone['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::USER_PHONE, $userPhone, $comparison);
    }

    /**
     * Filter the query on the admin_email column
     *
     * Example usage:
     * <code>
     * $query->filterByAdminEmail('fooValue');   // WHERE admin_email = 'fooValue'
     * $query->filterByAdminEmail('%fooValue%'); // WHERE admin_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adminEmail The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByAdminEmail($adminEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adminEmail)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $adminEmail)) {
                $adminEmail = str_replace('*', '%', $adminEmail);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::ADMIN_EMAIL, $adminEmail, $comparison);
    }

    /**
     * Filter the query on the comment_section column
     *
     * Example usage:
     * <code>
     * $query->filterByCommentSection(1234); // WHERE comment_section = 1234
     * $query->filterByCommentSection(array(12, 34)); // WHERE comment_section IN (12, 34)
     * $query->filterByCommentSection(array('min' => 12)); // WHERE comment_section >= 12
     * $query->filterByCommentSection(array('max' => 12)); // WHERE comment_section <= 12
     * </code>
     *
     * @param     mixed $commentSection The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByCommentSection($commentSection = null, $comparison = null)
    {
        if (is_array($commentSection)) {
            $useMinMax = false;
            if (isset($commentSection['min'])) {
                $this->addUsingAlias(QuizzesPeer::COMMENT_SECTION, $commentSection['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($commentSection['max'])) {
                $this->addUsingAlias(QuizzesPeer::COMMENT_SECTION, $commentSection['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::COMMENT_SECTION, $commentSection, $comparison);
    }

    /**
     * Filter the query on the question_from_total column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionFromTotal(1234); // WHERE question_from_total = 1234
     * $query->filterByQuestionFromTotal(array(12, 34)); // WHERE question_from_total IN (12, 34)
     * $query->filterByQuestionFromTotal(array('min' => 12)); // WHERE question_from_total >= 12
     * $query->filterByQuestionFromTotal(array('max' => 12)); // WHERE question_from_total <= 12
     * </code>
     *
     * @param     mixed $questionFromTotal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByQuestionFromTotal($questionFromTotal = null, $comparison = null)
    {
        if (is_array($questionFromTotal)) {
            $useMinMax = false;
            if (isset($questionFromTotal['min'])) {
                $this->addUsingAlias(QuizzesPeer::QUESTION_FROM_TOTAL, $questionFromTotal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionFromTotal['max'])) {
                $this->addUsingAlias(QuizzesPeer::QUESTION_FROM_TOTAL, $questionFromTotal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::QUESTION_FROM_TOTAL, $questionFromTotal, $comparison);
    }

    /**
     * Filter the query on the total_user_tries column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalUserTries(1234); // WHERE total_user_tries = 1234
     * $query->filterByTotalUserTries(array(12, 34)); // WHERE total_user_tries IN (12, 34)
     * $query->filterByTotalUserTries(array('min' => 12)); // WHERE total_user_tries >= 12
     * $query->filterByTotalUserTries(array('max' => 12)); // WHERE total_user_tries <= 12
     * </code>
     *
     * @param     mixed $totalUserTries The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByTotalUserTries($totalUserTries = null, $comparison = null)
    {
        if (is_array($totalUserTries)) {
            $useMinMax = false;
            if (isset($totalUserTries['min'])) {
                $this->addUsingAlias(QuizzesPeer::TOTAL_USER_TRIES, $totalUserTries['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalUserTries['max'])) {
                $this->addUsingAlias(QuizzesPeer::TOTAL_USER_TRIES, $totalUserTries['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::TOTAL_USER_TRIES, $totalUserTries, $comparison);
    }

    /**
     * Filter the query on the total_user_tries_text column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalUserTriesText('fooValue');   // WHERE total_user_tries_text = 'fooValue'
     * $query->filterByTotalUserTriesText('%fooValue%'); // WHERE total_user_tries_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $totalUserTriesText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByTotalUserTriesText($totalUserTriesText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($totalUserTriesText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $totalUserTriesText)) {
                $totalUserTriesText = str_replace('*', '%', $totalUserTriesText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::TOTAL_USER_TRIES_TEXT, $totalUserTriesText, $comparison);
    }

    /**
     * Filter the query on the certificate_template column
     *
     * Example usage:
     * <code>
     * $query->filterByCertificateTemplate('fooValue');   // WHERE certificate_template = 'fooValue'
     * $query->filterByCertificateTemplate('%fooValue%'); // WHERE certificate_template LIKE '%fooValue%'
     * </code>
     *
     * @param     string $certificateTemplate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByCertificateTemplate($certificateTemplate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($certificateTemplate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $certificateTemplate)) {
                $certificateTemplate = str_replace('*', '%', $certificateTemplate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::CERTIFICATE_TEMPLATE, $certificateTemplate, $comparison);
    }

    /**
     * Filter the query on the social_media column
     *
     * Example usage:
     * <code>
     * $query->filterBySocialMedia(1234); // WHERE social_media = 1234
     * $query->filterBySocialMedia(array(12, 34)); // WHERE social_media IN (12, 34)
     * $query->filterBySocialMedia(array('min' => 12)); // WHERE social_media >= 12
     * $query->filterBySocialMedia(array('max' => 12)); // WHERE social_media <= 12
     * </code>
     *
     * @param     mixed $socialMedia The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterBySocialMedia($socialMedia = null, $comparison = null)
    {
        if (is_array($socialMedia)) {
            $useMinMax = false;
            if (isset($socialMedia['min'])) {
                $this->addUsingAlias(QuizzesPeer::SOCIAL_MEDIA, $socialMedia['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($socialMedia['max'])) {
                $this->addUsingAlias(QuizzesPeer::SOCIAL_MEDIA, $socialMedia['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::SOCIAL_MEDIA, $socialMedia, $comparison);
    }

    /**
     * Filter the query on the social_media_text column
     *
     * Example usage:
     * <code>
     * $query->filterBySocialMediaText('fooValue');   // WHERE social_media_text = 'fooValue'
     * $query->filterBySocialMediaText('%fooValue%'); // WHERE social_media_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $socialMediaText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterBySocialMediaText($socialMediaText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($socialMediaText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $socialMediaText)) {
                $socialMediaText = str_replace('*', '%', $socialMediaText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::SOCIAL_MEDIA_TEXT, $socialMediaText, $comparison);
    }

    /**
     * Filter the query on the pagination column
     *
     * Example usage:
     * <code>
     * $query->filterByPagination(1234); // WHERE pagination = 1234
     * $query->filterByPagination(array(12, 34)); // WHERE pagination IN (12, 34)
     * $query->filterByPagination(array('min' => 12)); // WHERE pagination >= 12
     * $query->filterByPagination(array('max' => 12)); // WHERE pagination <= 12
     * </code>
     *
     * @param     mixed $pagination The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByPagination($pagination = null, $comparison = null)
    {
        if (is_array($pagination)) {
            $useMinMax = false;
            if (isset($pagination['min'])) {
                $this->addUsingAlias(QuizzesPeer::PAGINATION, $pagination['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pagination['max'])) {
                $this->addUsingAlias(QuizzesPeer::PAGINATION, $pagination['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::PAGINATION, $pagination, $comparison);
    }

    /**
     * Filter the query on the pagination_text column
     *
     * Example usage:
     * <code>
     * $query->filterByPaginationText('fooValue');   // WHERE pagination_text = 'fooValue'
     * $query->filterByPaginationText('%fooValue%'); // WHERE pagination_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paginationText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByPaginationText($paginationText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paginationText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paginationText)) {
                $paginationText = str_replace('*', '%', $paginationText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::PAGINATION_TEXT, $paginationText, $comparison);
    }

    /**
     * Filter the query on the timer_limit column
     *
     * Example usage:
     * <code>
     * $query->filterByTimerLimit(1234); // WHERE timer_limit = 1234
     * $query->filterByTimerLimit(array(12, 34)); // WHERE timer_limit IN (12, 34)
     * $query->filterByTimerLimit(array('min' => 12)); // WHERE timer_limit >= 12
     * $query->filterByTimerLimit(array('max' => 12)); // WHERE timer_limit <= 12
     * </code>
     *
     * @param     mixed $timerLimit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByTimerLimit($timerLimit = null, $comparison = null)
    {
        if (is_array($timerLimit)) {
            $useMinMax = false;
            if (isset($timerLimit['min'])) {
                $this->addUsingAlias(QuizzesPeer::TIMER_LIMIT, $timerLimit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($timerLimit['max'])) {
                $this->addUsingAlias(QuizzesPeer::TIMER_LIMIT, $timerLimit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::TIMER_LIMIT, $timerLimit, $comparison);
    }

    /**
     * Filter the query on the quiz_stye column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizStye('fooValue');   // WHERE quiz_stye = 'fooValue'
     * $query->filterByQuizStye('%fooValue%'); // WHERE quiz_stye LIKE '%fooValue%'
     * </code>
     *
     * @param     string $quizStye The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByQuizStye($quizStye = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quizStye)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $quizStye)) {
                $quizStye = str_replace('*', '%', $quizStye);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::QUIZ_STYE, $quizStye, $comparison);
    }

    /**
     * Filter the query on the question_numbering column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionNumbering(1234); // WHERE question_numbering = 1234
     * $query->filterByQuestionNumbering(array(12, 34)); // WHERE question_numbering IN (12, 34)
     * $query->filterByQuestionNumbering(array('min' => 12)); // WHERE question_numbering >= 12
     * $query->filterByQuestionNumbering(array('max' => 12)); // WHERE question_numbering <= 12
     * </code>
     *
     * @param     mixed $questionNumbering The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByQuestionNumbering($questionNumbering = null, $comparison = null)
    {
        if (is_array($questionNumbering)) {
            $useMinMax = false;
            if (isset($questionNumbering['min'])) {
                $this->addUsingAlias(QuizzesPeer::QUESTION_NUMBERING, $questionNumbering['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionNumbering['max'])) {
                $this->addUsingAlias(QuizzesPeer::QUESTION_NUMBERING, $questionNumbering['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::QUESTION_NUMBERING, $questionNumbering, $comparison);
    }

    /**
     * Filter the query on the quiz_settings column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizSettings('fooValue');   // WHERE quiz_settings = 'fooValue'
     * $query->filterByQuizSettings('%fooValue%'); // WHERE quiz_settings LIKE '%fooValue%'
     * </code>
     *
     * @param     string $quizSettings The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByQuizSettings($quizSettings = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quizSettings)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $quizSettings)) {
                $quizSettings = str_replace('*', '%', $quizSettings);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::QUIZ_SETTINGS, $quizSettings, $comparison);
    }

    /**
     * Filter the query on the theme_selected column
     *
     * Example usage:
     * <code>
     * $query->filterByThemeSelected('fooValue');   // WHERE theme_selected = 'fooValue'
     * $query->filterByThemeSelected('%fooValue%'); // WHERE theme_selected LIKE '%fooValue%'
     * </code>
     *
     * @param     string $themeSelected The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByThemeSelected($themeSelected = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($themeSelected)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $themeSelected)) {
                $themeSelected = str_replace('*', '%', $themeSelected);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::THEME_SELECTED, $themeSelected, $comparison);
    }

    /**
     * Filter the query on the last_activity column
     *
     * Example usage:
     * <code>
     * $query->filterByLastActivity('2011-03-14'); // WHERE last_activity = '2011-03-14'
     * $query->filterByLastActivity('now'); // WHERE last_activity = '2011-03-14'
     * $query->filterByLastActivity(array('max' => 'yesterday')); // WHERE last_activity < '2011-03-13'
     * </code>
     *
     * @param     mixed $lastActivity The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByLastActivity($lastActivity = null, $comparison = null)
    {
        if (is_array($lastActivity)) {
            $useMinMax = false;
            if (isset($lastActivity['min'])) {
                $this->addUsingAlias(QuizzesPeer::LAST_ACTIVITY, $lastActivity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastActivity['max'])) {
                $this->addUsingAlias(QuizzesPeer::LAST_ACTIVITY, $lastActivity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::LAST_ACTIVITY, $lastActivity, $comparison);
    }

    /**
     * Filter the query on the require_log_in column
     *
     * Example usage:
     * <code>
     * $query->filterByRequireLogIn(1234); // WHERE require_log_in = 1234
     * $query->filterByRequireLogIn(array(12, 34)); // WHERE require_log_in IN (12, 34)
     * $query->filterByRequireLogIn(array('min' => 12)); // WHERE require_log_in >= 12
     * $query->filterByRequireLogIn(array('max' => 12)); // WHERE require_log_in <= 12
     * </code>
     *
     * @param     mixed $requireLogIn The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByRequireLogIn($requireLogIn = null, $comparison = null)
    {
        if (is_array($requireLogIn)) {
            $useMinMax = false;
            if (isset($requireLogIn['min'])) {
                $this->addUsingAlias(QuizzesPeer::REQUIRE_LOG_IN, $requireLogIn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($requireLogIn['max'])) {
                $this->addUsingAlias(QuizzesPeer::REQUIRE_LOG_IN, $requireLogIn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::REQUIRE_LOG_IN, $requireLogIn, $comparison);
    }

    /**
     * Filter the query on the require_log_in_text column
     *
     * Example usage:
     * <code>
     * $query->filterByRequireLogInText('fooValue');   // WHERE require_log_in_text = 'fooValue'
     * $query->filterByRequireLogInText('%fooValue%'); // WHERE require_log_in_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $requireLogInText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByRequireLogInText($requireLogInText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($requireLogInText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $requireLogInText)) {
                $requireLogInText = str_replace('*', '%', $requireLogInText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::REQUIRE_LOG_IN_TEXT, $requireLogInText, $comparison);
    }

    /**
     * Filter the query on the limit_total_entries column
     *
     * Example usage:
     * <code>
     * $query->filterByLimitTotalEntries(1234); // WHERE limit_total_entries = 1234
     * $query->filterByLimitTotalEntries(array(12, 34)); // WHERE limit_total_entries IN (12, 34)
     * $query->filterByLimitTotalEntries(array('min' => 12)); // WHERE limit_total_entries >= 12
     * $query->filterByLimitTotalEntries(array('max' => 12)); // WHERE limit_total_entries <= 12
     * </code>
     *
     * @param     mixed $limitTotalEntries The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByLimitTotalEntries($limitTotalEntries = null, $comparison = null)
    {
        if (is_array($limitTotalEntries)) {
            $useMinMax = false;
            if (isset($limitTotalEntries['min'])) {
                $this->addUsingAlias(QuizzesPeer::LIMIT_TOTAL_ENTRIES, $limitTotalEntries['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($limitTotalEntries['max'])) {
                $this->addUsingAlias(QuizzesPeer::LIMIT_TOTAL_ENTRIES, $limitTotalEntries['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::LIMIT_TOTAL_ENTRIES, $limitTotalEntries, $comparison);
    }

    /**
     * Filter the query on the limit_total_entries_text column
     *
     * Example usage:
     * <code>
     * $query->filterByLimitTotalEntriesText('fooValue');   // WHERE limit_total_entries_text = 'fooValue'
     * $query->filterByLimitTotalEntriesText('%fooValue%'); // WHERE limit_total_entries_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $limitTotalEntriesText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByLimitTotalEntriesText($limitTotalEntriesText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($limitTotalEntriesText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $limitTotalEntriesText)) {
                $limitTotalEntriesText = str_replace('*', '%', $limitTotalEntriesText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::LIMIT_TOTAL_ENTRIES_TEXT, $limitTotalEntriesText, $comparison);
    }

    /**
     * Filter the query on the scheduled_timeframe column
     *
     * Example usage:
     * <code>
     * $query->filterByScheduledTimeframe('fooValue');   // WHERE scheduled_timeframe = 'fooValue'
     * $query->filterByScheduledTimeframe('%fooValue%'); // WHERE scheduled_timeframe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $scheduledTimeframe The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByScheduledTimeframe($scheduledTimeframe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($scheduledTimeframe)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $scheduledTimeframe)) {
                $scheduledTimeframe = str_replace('*', '%', $scheduledTimeframe);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::SCHEDULED_TIMEFRAME, $scheduledTimeframe, $comparison);
    }

    /**
     * Filter the query on the scheduled_timeframe_text column
     *
     * Example usage:
     * <code>
     * $query->filterByScheduledTimeframeText('fooValue');   // WHERE scheduled_timeframe_text = 'fooValue'
     * $query->filterByScheduledTimeframeText('%fooValue%'); // WHERE scheduled_timeframe_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $scheduledTimeframeText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByScheduledTimeframeText($scheduledTimeframeText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($scheduledTimeframeText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $scheduledTimeframeText)) {
                $scheduledTimeframeText = str_replace('*', '%', $scheduledTimeframeText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::SCHEDULED_TIMEFRAME_TEXT, $scheduledTimeframeText, $comparison);
    }

    /**
     * Filter the query on the disable_answer_onselect column
     *
     * Example usage:
     * <code>
     * $query->filterByDisableAnswerOnselect(1234); // WHERE disable_answer_onselect = 1234
     * $query->filterByDisableAnswerOnselect(array(12, 34)); // WHERE disable_answer_onselect IN (12, 34)
     * $query->filterByDisableAnswerOnselect(array('min' => 12)); // WHERE disable_answer_onselect >= 12
     * $query->filterByDisableAnswerOnselect(array('max' => 12)); // WHERE disable_answer_onselect <= 12
     * </code>
     *
     * @param     mixed $disableAnswerOnselect The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByDisableAnswerOnselect($disableAnswerOnselect = null, $comparison = null)
    {
        if (is_array($disableAnswerOnselect)) {
            $useMinMax = false;
            if (isset($disableAnswerOnselect['min'])) {
                $this->addUsingAlias(QuizzesPeer::DISABLE_ANSWER_ONSELECT, $disableAnswerOnselect['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($disableAnswerOnselect['max'])) {
                $this->addUsingAlias(QuizzesPeer::DISABLE_ANSWER_ONSELECT, $disableAnswerOnselect['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::DISABLE_ANSWER_ONSELECT, $disableAnswerOnselect, $comparison);
    }

    /**
     * Filter the query on the ajax_show_correct column
     *
     * Example usage:
     * <code>
     * $query->filterByAjaxShowCorrect(1234); // WHERE ajax_show_correct = 1234
     * $query->filterByAjaxShowCorrect(array(12, 34)); // WHERE ajax_show_correct IN (12, 34)
     * $query->filterByAjaxShowCorrect(array('min' => 12)); // WHERE ajax_show_correct >= 12
     * $query->filterByAjaxShowCorrect(array('max' => 12)); // WHERE ajax_show_correct <= 12
     * </code>
     *
     * @param     mixed $ajaxShowCorrect The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByAjaxShowCorrect($ajaxShowCorrect = null, $comparison = null)
    {
        if (is_array($ajaxShowCorrect)) {
            $useMinMax = false;
            if (isset($ajaxShowCorrect['min'])) {
                $this->addUsingAlias(QuizzesPeer::AJAX_SHOW_CORRECT, $ajaxShowCorrect['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ajaxShowCorrect['max'])) {
                $this->addUsingAlias(QuizzesPeer::AJAX_SHOW_CORRECT, $ajaxShowCorrect['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::AJAX_SHOW_CORRECT, $ajaxShowCorrect, $comparison);
    }

    /**
     * Filter the query on the quiz_views column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizViews(1234); // WHERE quiz_views = 1234
     * $query->filterByQuizViews(array(12, 34)); // WHERE quiz_views IN (12, 34)
     * $query->filterByQuizViews(array('min' => 12)); // WHERE quiz_views >= 12
     * $query->filterByQuizViews(array('max' => 12)); // WHERE quiz_views <= 12
     * </code>
     *
     * @param     mixed $quizViews The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByQuizViews($quizViews = null, $comparison = null)
    {
        if (is_array($quizViews)) {
            $useMinMax = false;
            if (isset($quizViews['min'])) {
                $this->addUsingAlias(QuizzesPeer::QUIZ_VIEWS, $quizViews['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quizViews['max'])) {
                $this->addUsingAlias(QuizzesPeer::QUIZ_VIEWS, $quizViews['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::QUIZ_VIEWS, $quizViews, $comparison);
    }

    /**
     * Filter the query on the quiz_taken column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizTaken(1234); // WHERE quiz_taken = 1234
     * $query->filterByQuizTaken(array(12, 34)); // WHERE quiz_taken IN (12, 34)
     * $query->filterByQuizTaken(array('min' => 12)); // WHERE quiz_taken >= 12
     * $query->filterByQuizTaken(array('max' => 12)); // WHERE quiz_taken <= 12
     * </code>
     *
     * @param     mixed $quizTaken The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByQuizTaken($quizTaken = null, $comparison = null)
    {
        if (is_array($quizTaken)) {
            $useMinMax = false;
            if (isset($quizTaken['min'])) {
                $this->addUsingAlias(QuizzesPeer::QUIZ_TAKEN, $quizTaken['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quizTaken['max'])) {
                $this->addUsingAlias(QuizzesPeer::QUIZ_TAKEN, $quizTaken['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::QUIZ_TAKEN, $quizTaken, $comparison);
    }

    /**
     * Filter the query on the deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByDeleted(1234); // WHERE deleted = 1234
     * $query->filterByDeleted(array(12, 34)); // WHERE deleted IN (12, 34)
     * $query->filterByDeleted(array('min' => 12)); // WHERE deleted >= 12
     * $query->filterByDeleted(array('max' => 12)); // WHERE deleted <= 12
     * </code>
     *
     * @param     mixed $deleted The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByDeleted($deleted = null, $comparison = null)
    {
        if (is_array($deleted)) {
            $useMinMax = false;
            if (isset($deleted['min'])) {
                $this->addUsingAlias(QuizzesPeer::DELETED, $deleted['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deleted['max'])) {
                $this->addUsingAlias(QuizzesPeer::DELETED, $deleted['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::DELETED, $deleted, $comparison);
    }

    /**
     * Filter the query on the quiz_description column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizDescription('fooValue');   // WHERE quiz_description = 'fooValue'
     * $query->filterByQuizDescription('%fooValue%'); // WHERE quiz_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $quizDescription The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function filterByQuizDescription($quizDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quizDescription)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $quizDescription)) {
                $quizDescription = str_replace('*', '%', $quizDescription);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuizzesPeer::QUIZ_DESCRIPTION, $quizDescription, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Quizzes $quizzes Object to remove from the list of results
     *
     * @return QuizzesQuery The current query, for fluid interface
     */
    public function prune($quizzes = null)
    {
        if ($quizzes) {
            $this->addUsingAlias(QuizzesPeer::QUIZ_ID, $quizzes->getQuizId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
