<?php


/**
 * Base class that represents a query for the 'rfn_questions' table.
 *
 *
 *
 * @method QuestionsQuery orderByQuestionId($order = Criteria::ASC) Order by the question_id column
 * @method QuestionsQuery orderByQuizId($order = Criteria::ASC) Order by the quiz_id column
 * @method QuestionsQuery orderByQuestionName($order = Criteria::ASC) Order by the question_name column
 * @method QuestionsQuery orderByAnswerArray($order = Criteria::ASC) Order by the answer_array column
 * @method QuestionsQuery orderByAnswerOne($order = Criteria::ASC) Order by the answer_one column
 * @method QuestionsQuery orderByAnswerOnePoints($order = Criteria::ASC) Order by the answer_one_points column
 * @method QuestionsQuery orderByAnswerTwo($order = Criteria::ASC) Order by the answer_two column
 * @method QuestionsQuery orderByAnswerTwoPoints($order = Criteria::ASC) Order by the answer_two_points column
 * @method QuestionsQuery orderByAnswerThree($order = Criteria::ASC) Order by the answer_three column
 * @method QuestionsQuery orderByAnswerThreePoints($order = Criteria::ASC) Order by the answer_three_points column
 * @method QuestionsQuery orderByAnswerFour($order = Criteria::ASC) Order by the answer_four column
 * @method QuestionsQuery orderByAnswerFourPoints($order = Criteria::ASC) Order by the answer_four_points column
 * @method QuestionsQuery orderByAnswerFive($order = Criteria::ASC) Order by the answer_five column
 * @method QuestionsQuery orderByAnswerFivePoints($order = Criteria::ASC) Order by the answer_five_points column
 * @method QuestionsQuery orderByAnswerSix($order = Criteria::ASC) Order by the answer_six column
 * @method QuestionsQuery orderByAnswerSixPoints($order = Criteria::ASC) Order by the answer_six_points column
 * @method QuestionsQuery orderByCorrectAnswer($order = Criteria::ASC) Order by the correct_answer column
 * @method QuestionsQuery orderByQuestionAnswerInfo($order = Criteria::ASC) Order by the question_answer_info column
 * @method QuestionsQuery orderByComments($order = Criteria::ASC) Order by the comments column
 * @method QuestionsQuery orderByHints($order = Criteria::ASC) Order by the hints column
 * @method QuestionsQuery orderByQuestionOrder($order = Criteria::ASC) Order by the question_order column
 * @method QuestionsQuery orderByQuestionType($order = Criteria::ASC) Order by the question_type column
 * @method QuestionsQuery orderByQuestionTypeNew($order = Criteria::ASC) Order by the question_type_new column
 * @method QuestionsQuery orderByQuestionSettings($order = Criteria::ASC) Order by the question_settings column
 * @method QuestionsQuery orderByCategory($order = Criteria::ASC) Order by the category column
 * @method QuestionsQuery orderByDeleted($order = Criteria::ASC) Order by the deleted column
 *
 * @method QuestionsQuery groupByQuestionId() Group by the question_id column
 * @method QuestionsQuery groupByQuizId() Group by the quiz_id column
 * @method QuestionsQuery groupByQuestionName() Group by the question_name column
 * @method QuestionsQuery groupByAnswerArray() Group by the answer_array column
 * @method QuestionsQuery groupByAnswerOne() Group by the answer_one column
 * @method QuestionsQuery groupByAnswerOnePoints() Group by the answer_one_points column
 * @method QuestionsQuery groupByAnswerTwo() Group by the answer_two column
 * @method QuestionsQuery groupByAnswerTwoPoints() Group by the answer_two_points column
 * @method QuestionsQuery groupByAnswerThree() Group by the answer_three column
 * @method QuestionsQuery groupByAnswerThreePoints() Group by the answer_three_points column
 * @method QuestionsQuery groupByAnswerFour() Group by the answer_four column
 * @method QuestionsQuery groupByAnswerFourPoints() Group by the answer_four_points column
 * @method QuestionsQuery groupByAnswerFive() Group by the answer_five column
 * @method QuestionsQuery groupByAnswerFivePoints() Group by the answer_five_points column
 * @method QuestionsQuery groupByAnswerSix() Group by the answer_six column
 * @method QuestionsQuery groupByAnswerSixPoints() Group by the answer_six_points column
 * @method QuestionsQuery groupByCorrectAnswer() Group by the correct_answer column
 * @method QuestionsQuery groupByQuestionAnswerInfo() Group by the question_answer_info column
 * @method QuestionsQuery groupByComments() Group by the comments column
 * @method QuestionsQuery groupByHints() Group by the hints column
 * @method QuestionsQuery groupByQuestionOrder() Group by the question_order column
 * @method QuestionsQuery groupByQuestionType() Group by the question_type column
 * @method QuestionsQuery groupByQuestionTypeNew() Group by the question_type_new column
 * @method QuestionsQuery groupByQuestionSettings() Group by the question_settings column
 * @method QuestionsQuery groupByCategory() Group by the category column
 * @method QuestionsQuery groupByDeleted() Group by the deleted column
 *
 * @method QuestionsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method QuestionsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method QuestionsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Questions findOne(PropelPDO $con = null) Return the first Questions matching the query
 * @method Questions findOneOrCreate(PropelPDO $con = null) Return the first Questions matching the query, or a new Questions object populated from the query conditions when no match is found
 *
 * @method Questions findOneByQuizId(int $quiz_id) Return the first Questions filtered by the quiz_id column
 * @method Questions findOneByQuestionName(string $question_name) Return the first Questions filtered by the question_name column
 * @method Questions findOneByAnswerArray(string $answer_array) Return the first Questions filtered by the answer_array column
 * @method Questions findOneByAnswerOne(string $answer_one) Return the first Questions filtered by the answer_one column
 * @method Questions findOneByAnswerOnePoints(int $answer_one_points) Return the first Questions filtered by the answer_one_points column
 * @method Questions findOneByAnswerTwo(string $answer_two) Return the first Questions filtered by the answer_two column
 * @method Questions findOneByAnswerTwoPoints(int $answer_two_points) Return the first Questions filtered by the answer_two_points column
 * @method Questions findOneByAnswerThree(string $answer_three) Return the first Questions filtered by the answer_three column
 * @method Questions findOneByAnswerThreePoints(int $answer_three_points) Return the first Questions filtered by the answer_three_points column
 * @method Questions findOneByAnswerFour(string $answer_four) Return the first Questions filtered by the answer_four column
 * @method Questions findOneByAnswerFourPoints(int $answer_four_points) Return the first Questions filtered by the answer_four_points column
 * @method Questions findOneByAnswerFive(string $answer_five) Return the first Questions filtered by the answer_five column
 * @method Questions findOneByAnswerFivePoints(int $answer_five_points) Return the first Questions filtered by the answer_five_points column
 * @method Questions findOneByAnswerSix(string $answer_six) Return the first Questions filtered by the answer_six column
 * @method Questions findOneByAnswerSixPoints(int $answer_six_points) Return the first Questions filtered by the answer_six_points column
 * @method Questions findOneByCorrectAnswer(int $correct_answer) Return the first Questions filtered by the correct_answer column
 * @method Questions findOneByQuestionAnswerInfo(string $question_answer_info) Return the first Questions filtered by the question_answer_info column
 * @method Questions findOneByComments(int $comments) Return the first Questions filtered by the comments column
 * @method Questions findOneByHints(string $hints) Return the first Questions filtered by the hints column
 * @method Questions findOneByQuestionOrder(int $question_order) Return the first Questions filtered by the question_order column
 * @method Questions findOneByQuestionType(int $question_type) Return the first Questions filtered by the question_type column
 * @method Questions findOneByQuestionTypeNew(string $question_type_new) Return the first Questions filtered by the question_type_new column
 * @method Questions findOneByQuestionSettings(string $question_settings) Return the first Questions filtered by the question_settings column
 * @method Questions findOneByCategory(string $category) Return the first Questions filtered by the category column
 * @method Questions findOneByDeleted(int $deleted) Return the first Questions filtered by the deleted column
 *
 * @method array findByQuestionId(int $question_id) Return Questions objects filtered by the question_id column
 * @method array findByQuizId(int $quiz_id) Return Questions objects filtered by the quiz_id column
 * @method array findByQuestionName(string $question_name) Return Questions objects filtered by the question_name column
 * @method array findByAnswerArray(string $answer_array) Return Questions objects filtered by the answer_array column
 * @method array findByAnswerOne(string $answer_one) Return Questions objects filtered by the answer_one column
 * @method array findByAnswerOnePoints(int $answer_one_points) Return Questions objects filtered by the answer_one_points column
 * @method array findByAnswerTwo(string $answer_two) Return Questions objects filtered by the answer_two column
 * @method array findByAnswerTwoPoints(int $answer_two_points) Return Questions objects filtered by the answer_two_points column
 * @method array findByAnswerThree(string $answer_three) Return Questions objects filtered by the answer_three column
 * @method array findByAnswerThreePoints(int $answer_three_points) Return Questions objects filtered by the answer_three_points column
 * @method array findByAnswerFour(string $answer_four) Return Questions objects filtered by the answer_four column
 * @method array findByAnswerFourPoints(int $answer_four_points) Return Questions objects filtered by the answer_four_points column
 * @method array findByAnswerFive(string $answer_five) Return Questions objects filtered by the answer_five column
 * @method array findByAnswerFivePoints(int $answer_five_points) Return Questions objects filtered by the answer_five_points column
 * @method array findByAnswerSix(string $answer_six) Return Questions objects filtered by the answer_six column
 * @method array findByAnswerSixPoints(int $answer_six_points) Return Questions objects filtered by the answer_six_points column
 * @method array findByCorrectAnswer(int $correct_answer) Return Questions objects filtered by the correct_answer column
 * @method array findByQuestionAnswerInfo(string $question_answer_info) Return Questions objects filtered by the question_answer_info column
 * @method array findByComments(int $comments) Return Questions objects filtered by the comments column
 * @method array findByHints(string $hints) Return Questions objects filtered by the hints column
 * @method array findByQuestionOrder(int $question_order) Return Questions objects filtered by the question_order column
 * @method array findByQuestionType(int $question_type) Return Questions objects filtered by the question_type column
 * @method array findByQuestionTypeNew(string $question_type_new) Return Questions objects filtered by the question_type_new column
 * @method array findByQuestionSettings(string $question_settings) Return Questions objects filtered by the question_settings column
 * @method array findByCategory(string $category) Return Questions objects filtered by the category column
 * @method array findByDeleted(int $deleted) Return Questions objects filtered by the deleted column
 *
 * @package    propel.generator.quizzes.om
 */
abstract class BaseQuestionsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseQuestionsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'rfn_new2';
        }
        if (null === $modelName) {
            $modelName = 'Questions';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new QuestionsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   QuestionsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return QuestionsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof QuestionsQuery) {
            return $criteria;
        }
        $query = new QuestionsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Questions|Questions[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = QuestionsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Questions A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByQuestionId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Questions A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `question_id`, `quiz_id`, `question_name`, `answer_array`, `answer_one`, `answer_one_points`, `answer_two`, `answer_two_points`, `answer_three`, `answer_three_points`, `answer_four`, `answer_four_points`, `answer_five`, `answer_five_points`, `answer_six`, `answer_six_points`, `correct_answer`, `question_answer_info`, `comments`, `hints`, `question_order`, `question_type`, `question_type_new`, `question_settings`, `category`, `deleted` FROM `rfn_questions` WHERE `question_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Questions();
            $obj->hydrate($row);
            QuestionsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Questions|Questions[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Questions[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(QuestionsPeer::QUESTION_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(QuestionsPeer::QUESTION_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the question_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionId(1234); // WHERE question_id = 1234
     * $query->filterByQuestionId(array(12, 34)); // WHERE question_id IN (12, 34)
     * $query->filterByQuestionId(array('min' => 12)); // WHERE question_id >= 12
     * $query->filterByQuestionId(array('max' => 12)); // WHERE question_id <= 12
     * </code>
     *
     * @param     mixed $questionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByQuestionId($questionId = null, $comparison = null)
    {
        if (is_array($questionId)) {
            $useMinMax = false;
            if (isset($questionId['min'])) {
                $this->addUsingAlias(QuestionsPeer::QUESTION_ID, $questionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionId['max'])) {
                $this->addUsingAlias(QuestionsPeer::QUESTION_ID, $questionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::QUESTION_ID, $questionId, $comparison);
    }

    /**
     * Filter the query on the quiz_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizId(1234); // WHERE quiz_id = 1234
     * $query->filterByQuizId(array(12, 34)); // WHERE quiz_id IN (12, 34)
     * $query->filterByQuizId(array('min' => 12)); // WHERE quiz_id >= 12
     * $query->filterByQuizId(array('max' => 12)); // WHERE quiz_id <= 12
     * </code>
     *
     * @param     mixed $quizId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByQuizId($quizId = null, $comparison = null)
    {
        if (is_array($quizId)) {
            $useMinMax = false;
            if (isset($quizId['min'])) {
                $this->addUsingAlias(QuestionsPeer::QUIZ_ID, $quizId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quizId['max'])) {
                $this->addUsingAlias(QuestionsPeer::QUIZ_ID, $quizId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::QUIZ_ID, $quizId, $comparison);
    }

    /**
     * Filter the query on the question_name column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionName('fooValue');   // WHERE question_name = 'fooValue'
     * $query->filterByQuestionName('%fooValue%'); // WHERE question_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $questionName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByQuestionName($questionName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($questionName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $questionName)) {
                $questionName = str_replace('*', '%', $questionName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::QUESTION_NAME, $questionName, $comparison);
    }

    /**
     * Filter the query on the answer_array column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerArray('fooValue');   // WHERE answer_array = 'fooValue'
     * $query->filterByAnswerArray('%fooValue%'); // WHERE answer_array LIKE '%fooValue%'
     * </code>
     *
     * @param     string $answerArray The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerArray($answerArray = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($answerArray)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $answerArray)) {
                $answerArray = str_replace('*', '%', $answerArray);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_ARRAY, $answerArray, $comparison);
    }

    /**
     * Filter the query on the answer_one column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerOne('fooValue');   // WHERE answer_one = 'fooValue'
     * $query->filterByAnswerOne('%fooValue%'); // WHERE answer_one LIKE '%fooValue%'
     * </code>
     *
     * @param     string $answerOne The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerOne($answerOne = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($answerOne)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $answerOne)) {
                $answerOne = str_replace('*', '%', $answerOne);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_ONE, $answerOne, $comparison);
    }

    /**
     * Filter the query on the answer_one_points column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerOnePoints(1234); // WHERE answer_one_points = 1234
     * $query->filterByAnswerOnePoints(array(12, 34)); // WHERE answer_one_points IN (12, 34)
     * $query->filterByAnswerOnePoints(array('min' => 12)); // WHERE answer_one_points >= 12
     * $query->filterByAnswerOnePoints(array('max' => 12)); // WHERE answer_one_points <= 12
     * </code>
     *
     * @param     mixed $answerOnePoints The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerOnePoints($answerOnePoints = null, $comparison = null)
    {
        if (is_array($answerOnePoints)) {
            $useMinMax = false;
            if (isset($answerOnePoints['min'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_ONE_POINTS, $answerOnePoints['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($answerOnePoints['max'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_ONE_POINTS, $answerOnePoints['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_ONE_POINTS, $answerOnePoints, $comparison);
    }

    /**
     * Filter the query on the answer_two column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerTwo('fooValue');   // WHERE answer_two = 'fooValue'
     * $query->filterByAnswerTwo('%fooValue%'); // WHERE answer_two LIKE '%fooValue%'
     * </code>
     *
     * @param     string $answerTwo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerTwo($answerTwo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($answerTwo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $answerTwo)) {
                $answerTwo = str_replace('*', '%', $answerTwo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_TWO, $answerTwo, $comparison);
    }

    /**
     * Filter the query on the answer_two_points column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerTwoPoints(1234); // WHERE answer_two_points = 1234
     * $query->filterByAnswerTwoPoints(array(12, 34)); // WHERE answer_two_points IN (12, 34)
     * $query->filterByAnswerTwoPoints(array('min' => 12)); // WHERE answer_two_points >= 12
     * $query->filterByAnswerTwoPoints(array('max' => 12)); // WHERE answer_two_points <= 12
     * </code>
     *
     * @param     mixed $answerTwoPoints The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerTwoPoints($answerTwoPoints = null, $comparison = null)
    {
        if (is_array($answerTwoPoints)) {
            $useMinMax = false;
            if (isset($answerTwoPoints['min'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_TWO_POINTS, $answerTwoPoints['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($answerTwoPoints['max'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_TWO_POINTS, $answerTwoPoints['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_TWO_POINTS, $answerTwoPoints, $comparison);
    }

    /**
     * Filter the query on the answer_three column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerThree('fooValue');   // WHERE answer_three = 'fooValue'
     * $query->filterByAnswerThree('%fooValue%'); // WHERE answer_three LIKE '%fooValue%'
     * </code>
     *
     * @param     string $answerThree The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerThree($answerThree = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($answerThree)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $answerThree)) {
                $answerThree = str_replace('*', '%', $answerThree);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_THREE, $answerThree, $comparison);
    }

    /**
     * Filter the query on the answer_three_points column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerThreePoints(1234); // WHERE answer_three_points = 1234
     * $query->filterByAnswerThreePoints(array(12, 34)); // WHERE answer_three_points IN (12, 34)
     * $query->filterByAnswerThreePoints(array('min' => 12)); // WHERE answer_three_points >= 12
     * $query->filterByAnswerThreePoints(array('max' => 12)); // WHERE answer_three_points <= 12
     * </code>
     *
     * @param     mixed $answerThreePoints The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerThreePoints($answerThreePoints = null, $comparison = null)
    {
        if (is_array($answerThreePoints)) {
            $useMinMax = false;
            if (isset($answerThreePoints['min'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_THREE_POINTS, $answerThreePoints['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($answerThreePoints['max'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_THREE_POINTS, $answerThreePoints['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_THREE_POINTS, $answerThreePoints, $comparison);
    }

    /**
     * Filter the query on the answer_four column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerFour('fooValue');   // WHERE answer_four = 'fooValue'
     * $query->filterByAnswerFour('%fooValue%'); // WHERE answer_four LIKE '%fooValue%'
     * </code>
     *
     * @param     string $answerFour The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerFour($answerFour = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($answerFour)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $answerFour)) {
                $answerFour = str_replace('*', '%', $answerFour);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_FOUR, $answerFour, $comparison);
    }

    /**
     * Filter the query on the answer_four_points column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerFourPoints(1234); // WHERE answer_four_points = 1234
     * $query->filterByAnswerFourPoints(array(12, 34)); // WHERE answer_four_points IN (12, 34)
     * $query->filterByAnswerFourPoints(array('min' => 12)); // WHERE answer_four_points >= 12
     * $query->filterByAnswerFourPoints(array('max' => 12)); // WHERE answer_four_points <= 12
     * </code>
     *
     * @param     mixed $answerFourPoints The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerFourPoints($answerFourPoints = null, $comparison = null)
    {
        if (is_array($answerFourPoints)) {
            $useMinMax = false;
            if (isset($answerFourPoints['min'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_FOUR_POINTS, $answerFourPoints['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($answerFourPoints['max'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_FOUR_POINTS, $answerFourPoints['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_FOUR_POINTS, $answerFourPoints, $comparison);
    }

    /**
     * Filter the query on the answer_five column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerFive('fooValue');   // WHERE answer_five = 'fooValue'
     * $query->filterByAnswerFive('%fooValue%'); // WHERE answer_five LIKE '%fooValue%'
     * </code>
     *
     * @param     string $answerFive The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerFive($answerFive = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($answerFive)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $answerFive)) {
                $answerFive = str_replace('*', '%', $answerFive);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_FIVE, $answerFive, $comparison);
    }

    /**
     * Filter the query on the answer_five_points column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerFivePoints(1234); // WHERE answer_five_points = 1234
     * $query->filterByAnswerFivePoints(array(12, 34)); // WHERE answer_five_points IN (12, 34)
     * $query->filterByAnswerFivePoints(array('min' => 12)); // WHERE answer_five_points >= 12
     * $query->filterByAnswerFivePoints(array('max' => 12)); // WHERE answer_five_points <= 12
     * </code>
     *
     * @param     mixed $answerFivePoints The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerFivePoints($answerFivePoints = null, $comparison = null)
    {
        if (is_array($answerFivePoints)) {
            $useMinMax = false;
            if (isset($answerFivePoints['min'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_FIVE_POINTS, $answerFivePoints['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($answerFivePoints['max'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_FIVE_POINTS, $answerFivePoints['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_FIVE_POINTS, $answerFivePoints, $comparison);
    }

    /**
     * Filter the query on the answer_six column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerSix('fooValue');   // WHERE answer_six = 'fooValue'
     * $query->filterByAnswerSix('%fooValue%'); // WHERE answer_six LIKE '%fooValue%'
     * </code>
     *
     * @param     string $answerSix The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerSix($answerSix = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($answerSix)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $answerSix)) {
                $answerSix = str_replace('*', '%', $answerSix);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_SIX, $answerSix, $comparison);
    }

    /**
     * Filter the query on the answer_six_points column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerSixPoints(1234); // WHERE answer_six_points = 1234
     * $query->filterByAnswerSixPoints(array(12, 34)); // WHERE answer_six_points IN (12, 34)
     * $query->filterByAnswerSixPoints(array('min' => 12)); // WHERE answer_six_points >= 12
     * $query->filterByAnswerSixPoints(array('max' => 12)); // WHERE answer_six_points <= 12
     * </code>
     *
     * @param     mixed $answerSixPoints The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByAnswerSixPoints($answerSixPoints = null, $comparison = null)
    {
        if (is_array($answerSixPoints)) {
            $useMinMax = false;
            if (isset($answerSixPoints['min'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_SIX_POINTS, $answerSixPoints['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($answerSixPoints['max'])) {
                $this->addUsingAlias(QuestionsPeer::ANSWER_SIX_POINTS, $answerSixPoints['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::ANSWER_SIX_POINTS, $answerSixPoints, $comparison);
    }

    /**
     * Filter the query on the correct_answer column
     *
     * Example usage:
     * <code>
     * $query->filterByCorrectAnswer(1234); // WHERE correct_answer = 1234
     * $query->filterByCorrectAnswer(array(12, 34)); // WHERE correct_answer IN (12, 34)
     * $query->filterByCorrectAnswer(array('min' => 12)); // WHERE correct_answer >= 12
     * $query->filterByCorrectAnswer(array('max' => 12)); // WHERE correct_answer <= 12
     * </code>
     *
     * @param     mixed $correctAnswer The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByCorrectAnswer($correctAnswer = null, $comparison = null)
    {
        if (is_array($correctAnswer)) {
            $useMinMax = false;
            if (isset($correctAnswer['min'])) {
                $this->addUsingAlias(QuestionsPeer::CORRECT_ANSWER, $correctAnswer['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($correctAnswer['max'])) {
                $this->addUsingAlias(QuestionsPeer::CORRECT_ANSWER, $correctAnswer['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::CORRECT_ANSWER, $correctAnswer, $comparison);
    }

    /**
     * Filter the query on the question_answer_info column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionAnswerInfo('fooValue');   // WHERE question_answer_info = 'fooValue'
     * $query->filterByQuestionAnswerInfo('%fooValue%'); // WHERE question_answer_info LIKE '%fooValue%'
     * </code>
     *
     * @param     string $questionAnswerInfo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByQuestionAnswerInfo($questionAnswerInfo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($questionAnswerInfo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $questionAnswerInfo)) {
                $questionAnswerInfo = str_replace('*', '%', $questionAnswerInfo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::QUESTION_ANSWER_INFO, $questionAnswerInfo, $comparison);
    }

    /**
     * Filter the query on the comments column
     *
     * Example usage:
     * <code>
     * $query->filterByComments(1234); // WHERE comments = 1234
     * $query->filterByComments(array(12, 34)); // WHERE comments IN (12, 34)
     * $query->filterByComments(array('min' => 12)); // WHERE comments >= 12
     * $query->filterByComments(array('max' => 12)); // WHERE comments <= 12
     * </code>
     *
     * @param     mixed $comments The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByComments($comments = null, $comparison = null)
    {
        if (is_array($comments)) {
            $useMinMax = false;
            if (isset($comments['min'])) {
                $this->addUsingAlias(QuestionsPeer::COMMENTS, $comments['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($comments['max'])) {
                $this->addUsingAlias(QuestionsPeer::COMMENTS, $comments['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::COMMENTS, $comments, $comparison);
    }

    /**
     * Filter the query on the hints column
     *
     * Example usage:
     * <code>
     * $query->filterByHints('fooValue');   // WHERE hints = 'fooValue'
     * $query->filterByHints('%fooValue%'); // WHERE hints LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hints The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByHints($hints = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hints)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $hints)) {
                $hints = str_replace('*', '%', $hints);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::HINTS, $hints, $comparison);
    }

    /**
     * Filter the query on the question_order column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionOrder(1234); // WHERE question_order = 1234
     * $query->filterByQuestionOrder(array(12, 34)); // WHERE question_order IN (12, 34)
     * $query->filterByQuestionOrder(array('min' => 12)); // WHERE question_order >= 12
     * $query->filterByQuestionOrder(array('max' => 12)); // WHERE question_order <= 12
     * </code>
     *
     * @param     mixed $questionOrder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByQuestionOrder($questionOrder = null, $comparison = null)
    {
        if (is_array($questionOrder)) {
            $useMinMax = false;
            if (isset($questionOrder['min'])) {
                $this->addUsingAlias(QuestionsPeer::QUESTION_ORDER, $questionOrder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionOrder['max'])) {
                $this->addUsingAlias(QuestionsPeer::QUESTION_ORDER, $questionOrder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::QUESTION_ORDER, $questionOrder, $comparison);
    }

    /**
     * Filter the query on the question_type column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionType(1234); // WHERE question_type = 1234
     * $query->filterByQuestionType(array(12, 34)); // WHERE question_type IN (12, 34)
     * $query->filterByQuestionType(array('min' => 12)); // WHERE question_type >= 12
     * $query->filterByQuestionType(array('max' => 12)); // WHERE question_type <= 12
     * </code>
     *
     * @param     mixed $questionType The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByQuestionType($questionType = null, $comparison = null)
    {
        if (is_array($questionType)) {
            $useMinMax = false;
            if (isset($questionType['min'])) {
                $this->addUsingAlias(QuestionsPeer::QUESTION_TYPE, $questionType['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionType['max'])) {
                $this->addUsingAlias(QuestionsPeer::QUESTION_TYPE, $questionType['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::QUESTION_TYPE, $questionType, $comparison);
    }

    /**
     * Filter the query on the question_type_new column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionTypeNew('fooValue');   // WHERE question_type_new = 'fooValue'
     * $query->filterByQuestionTypeNew('%fooValue%'); // WHERE question_type_new LIKE '%fooValue%'
     * </code>
     *
     * @param     string $questionTypeNew The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByQuestionTypeNew($questionTypeNew = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($questionTypeNew)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $questionTypeNew)) {
                $questionTypeNew = str_replace('*', '%', $questionTypeNew);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::QUESTION_TYPE_NEW, $questionTypeNew, $comparison);
    }

    /**
     * Filter the query on the question_settings column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionSettings('fooValue');   // WHERE question_settings = 'fooValue'
     * $query->filterByQuestionSettings('%fooValue%'); // WHERE question_settings LIKE '%fooValue%'
     * </code>
     *
     * @param     string $questionSettings The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByQuestionSettings($questionSettings = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($questionSettings)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $questionSettings)) {
                $questionSettings = str_replace('*', '%', $questionSettings);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::QUESTION_SETTINGS, $questionSettings, $comparison);
    }

    /**
     * Filter the query on the category column
     *
     * Example usage:
     * <code>
     * $query->filterByCategory('fooValue');   // WHERE category = 'fooValue'
     * $query->filterByCategory('%fooValue%'); // WHERE category LIKE '%fooValue%'
     * </code>
     *
     * @param     string $category The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByCategory($category = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($category)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $category)) {
                $category = str_replace('*', '%', $category);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::CATEGORY, $category, $comparison);
    }

    /**
     * Filter the query on the deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByDeleted(1234); // WHERE deleted = 1234
     * $query->filterByDeleted(array(12, 34)); // WHERE deleted IN (12, 34)
     * $query->filterByDeleted(array('min' => 12)); // WHERE deleted >= 12
     * $query->filterByDeleted(array('max' => 12)); // WHERE deleted <= 12
     * </code>
     *
     * @param     mixed $deleted The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function filterByDeleted($deleted = null, $comparison = null)
    {
        if (is_array($deleted)) {
            $useMinMax = false;
            if (isset($deleted['min'])) {
                $this->addUsingAlias(QuestionsPeer::DELETED, $deleted['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deleted['max'])) {
                $this->addUsingAlias(QuestionsPeer::DELETED, $deleted['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionsPeer::DELETED, $deleted, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Questions $questions Object to remove from the list of results
     *
     * @return QuestionsQuery The current query, for fluid interface
     */
    public function prune($questions = null)
    {
        if ($questions) {
            $this->addUsingAlias(QuestionsPeer::QUESTION_ID, $questions->getQuestionId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
