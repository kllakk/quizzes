<?php


/**
 * Base static class for performing query and update operations on the 'rfn_quizzes' table.
 *
 *
 *
 * @package propel.generator.quizzes.om
 */
abstract class BaseQuizzesPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'rfn_new2';

    /** the table name for this class */
    const TABLE_NAME = 'rfn_quizzes';

    /** the related Propel class for this table */
    const OM_CLASS = 'Quizzes';

    /** the related TableMap class for this table */
    const TM_CLASS = 'QuizzesTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 56;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 56;

    /** the column name for the quiz_id field */
    const QUIZ_ID = 'rfn_quizzes.quiz_id';

    /** the column name for the quiz_name field */
    const QUIZ_NAME = 'rfn_quizzes.quiz_name';

    /** the column name for the message_before field */
    const MESSAGE_BEFORE = 'rfn_quizzes.message_before';

    /** the column name for the message_after field */
    const MESSAGE_AFTER = 'rfn_quizzes.message_after';

    /** the column name for the message_comment field */
    const MESSAGE_COMMENT = 'rfn_quizzes.message_comment';

    /** the column name for the message_end_template field */
    const MESSAGE_END_TEMPLATE = 'rfn_quizzes.message_end_template';

    /** the column name for the user_email_template field */
    const USER_EMAIL_TEMPLATE = 'rfn_quizzes.user_email_template';

    /** the column name for the admin_email_template field */
    const ADMIN_EMAIL_TEMPLATE = 'rfn_quizzes.admin_email_template';

    /** the column name for the submit_button_text field */
    const SUBMIT_BUTTON_TEXT = 'rfn_quizzes.submit_button_text';

    /** the column name for the name_field_text field */
    const NAME_FIELD_TEXT = 'rfn_quizzes.name_field_text';

    /** the column name for the business_field_text field */
    const BUSINESS_FIELD_TEXT = 'rfn_quizzes.business_field_text';

    /** the column name for the email_field_text field */
    const EMAIL_FIELD_TEXT = 'rfn_quizzes.email_field_text';

    /** the column name for the phone_field_text field */
    const PHONE_FIELD_TEXT = 'rfn_quizzes.phone_field_text';

    /** the column name for the comment_field_text field */
    const COMMENT_FIELD_TEXT = 'rfn_quizzes.comment_field_text';

    /** the column name for the email_from_text field */
    const EMAIL_FROM_TEXT = 'rfn_quizzes.email_from_text';

    /** the column name for the question_answer_template field */
    const QUESTION_ANSWER_TEMPLATE = 'rfn_quizzes.question_answer_template';

    /** the column name for the leaderboard_template field */
    const LEADERBOARD_TEMPLATE = 'rfn_quizzes.leaderboard_template';

    /** the column name for the system field */
    const SYSTEM = 'rfn_quizzes.system';

    /** the column name for the randomness_order field */
    const RANDOMNESS_ORDER = 'rfn_quizzes.randomness_order';

    /** the column name for the loggedin_user_contact field */
    const LOGGEDIN_USER_CONTACT = 'rfn_quizzes.loggedin_user_contact';

    /** the column name for the show_score field */
    const SHOW_SCORE = 'rfn_quizzes.show_score';

    /** the column name for the send_user_email field */
    const SEND_USER_EMAIL = 'rfn_quizzes.send_user_email';

    /** the column name for the send_admin_email field */
    const SEND_ADMIN_EMAIL = 'rfn_quizzes.send_admin_email';

    /** the column name for the contact_info_location field */
    const CONTACT_INFO_LOCATION = 'rfn_quizzes.contact_info_location';

    /** the column name for the user_name field */
    const USER_NAME = 'rfn_quizzes.user_name';

    /** the column name for the user_comp field */
    const USER_COMP = 'rfn_quizzes.user_comp';

    /** the column name for the user_email field */
    const USER_EMAIL = 'rfn_quizzes.user_email';

    /** the column name for the user_phone field */
    const USER_PHONE = 'rfn_quizzes.user_phone';

    /** the column name for the admin_email field */
    const ADMIN_EMAIL = 'rfn_quizzes.admin_email';

    /** the column name for the comment_section field */
    const COMMENT_SECTION = 'rfn_quizzes.comment_section';

    /** the column name for the question_from_total field */
    const QUESTION_FROM_TOTAL = 'rfn_quizzes.question_from_total';

    /** the column name for the total_user_tries field */
    const TOTAL_USER_TRIES = 'rfn_quizzes.total_user_tries';

    /** the column name for the total_user_tries_text field */
    const TOTAL_USER_TRIES_TEXT = 'rfn_quizzes.total_user_tries_text';

    /** the column name for the certificate_template field */
    const CERTIFICATE_TEMPLATE = 'rfn_quizzes.certificate_template';

    /** the column name for the social_media field */
    const SOCIAL_MEDIA = 'rfn_quizzes.social_media';

    /** the column name for the social_media_text field */
    const SOCIAL_MEDIA_TEXT = 'rfn_quizzes.social_media_text';

    /** the column name for the pagination field */
    const PAGINATION = 'rfn_quizzes.pagination';

    /** the column name for the pagination_text field */
    const PAGINATION_TEXT = 'rfn_quizzes.pagination_text';

    /** the column name for the timer_limit field */
    const TIMER_LIMIT = 'rfn_quizzes.timer_limit';

    /** the column name for the quiz_stye field */
    const QUIZ_STYE = 'rfn_quizzes.quiz_stye';

    /** the column name for the question_numbering field */
    const QUESTION_NUMBERING = 'rfn_quizzes.question_numbering';

    /** the column name for the quiz_settings field */
    const QUIZ_SETTINGS = 'rfn_quizzes.quiz_settings';

    /** the column name for the theme_selected field */
    const THEME_SELECTED = 'rfn_quizzes.theme_selected';

    /** the column name for the last_activity field */
    const LAST_ACTIVITY = 'rfn_quizzes.last_activity';

    /** the column name for the require_log_in field */
    const REQUIRE_LOG_IN = 'rfn_quizzes.require_log_in';

    /** the column name for the require_log_in_text field */
    const REQUIRE_LOG_IN_TEXT = 'rfn_quizzes.require_log_in_text';

    /** the column name for the limit_total_entries field */
    const LIMIT_TOTAL_ENTRIES = 'rfn_quizzes.limit_total_entries';

    /** the column name for the limit_total_entries_text field */
    const LIMIT_TOTAL_ENTRIES_TEXT = 'rfn_quizzes.limit_total_entries_text';

    /** the column name for the scheduled_timeframe field */
    const SCHEDULED_TIMEFRAME = 'rfn_quizzes.scheduled_timeframe';

    /** the column name for the scheduled_timeframe_text field */
    const SCHEDULED_TIMEFRAME_TEXT = 'rfn_quizzes.scheduled_timeframe_text';

    /** the column name for the disable_answer_onselect field */
    const DISABLE_ANSWER_ONSELECT = 'rfn_quizzes.disable_answer_onselect';

    /** the column name for the ajax_show_correct field */
    const AJAX_SHOW_CORRECT = 'rfn_quizzes.ajax_show_correct';

    /** the column name for the quiz_views field */
    const QUIZ_VIEWS = 'rfn_quizzes.quiz_views';

    /** the column name for the quiz_taken field */
    const QUIZ_TAKEN = 'rfn_quizzes.quiz_taken';

    /** the column name for the deleted field */
    const DELETED = 'rfn_quizzes.deleted';

    /** the column name for the quiz_description field */
    const QUIZ_DESCRIPTION = 'rfn_quizzes.quiz_description';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of Quizzes objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Quizzes[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. QuizzesPeer::$fieldNames[QuizzesPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('QuizId', 'QuizName', 'MessageBefore', 'MessageAfter', 'MessageComment', 'MessageEndTemplate', 'UserEmailTemplate', 'AdminEmailTemplate', 'SubmitButtonText', 'NameFieldText', 'BusinessFieldText', 'EmailFieldText', 'PhoneFieldText', 'CommentFieldText', 'EmailFromText', 'QuestionAnswerTemplate', 'LeaderboardTemplate', 'System', 'RandomnessOrder', 'LoggedinUserContact', 'ShowScore', 'SendUserEmail', 'SendAdminEmail', 'ContactInfoLocation', 'UserName', 'UserComp', 'UserEmail', 'UserPhone', 'AdminEmail', 'CommentSection', 'QuestionFromTotal', 'TotalUserTries', 'TotalUserTriesText', 'CertificateTemplate', 'SocialMedia', 'SocialMediaText', 'Pagination', 'PaginationText', 'TimerLimit', 'QuizStye', 'QuestionNumbering', 'QuizSettings', 'ThemeSelected', 'LastActivity', 'RequireLogIn', 'RequireLogInText', 'LimitTotalEntries', 'LimitTotalEntriesText', 'ScheduledTimeframe', 'ScheduledTimeframeText', 'DisableAnswerOnselect', 'AjaxShowCorrect', 'QuizViews', 'QuizTaken', 'Deleted', 'QuizDescription', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('quizId', 'quizName', 'messageBefore', 'messageAfter', 'messageComment', 'messageEndTemplate', 'userEmailTemplate', 'adminEmailTemplate', 'submitButtonText', 'nameFieldText', 'businessFieldText', 'emailFieldText', 'phoneFieldText', 'commentFieldText', 'emailFromText', 'questionAnswerTemplate', 'leaderboardTemplate', 'system', 'randomnessOrder', 'loggedinUserContact', 'showScore', 'sendUserEmail', 'sendAdminEmail', 'contactInfoLocation', 'userName', 'userComp', 'userEmail', 'userPhone', 'adminEmail', 'commentSection', 'questionFromTotal', 'totalUserTries', 'totalUserTriesText', 'certificateTemplate', 'socialMedia', 'socialMediaText', 'pagination', 'paginationText', 'timerLimit', 'quizStye', 'questionNumbering', 'quizSettings', 'themeSelected', 'lastActivity', 'requireLogIn', 'requireLogInText', 'limitTotalEntries', 'limitTotalEntriesText', 'scheduledTimeframe', 'scheduledTimeframeText', 'disableAnswerOnselect', 'ajaxShowCorrect', 'quizViews', 'quizTaken', 'deleted', 'quizDescription', ),
        BasePeer::TYPE_COLNAME => array (QuizzesPeer::QUIZ_ID, QuizzesPeer::QUIZ_NAME, QuizzesPeer::MESSAGE_BEFORE, QuizzesPeer::MESSAGE_AFTER, QuizzesPeer::MESSAGE_COMMENT, QuizzesPeer::MESSAGE_END_TEMPLATE, QuizzesPeer::USER_EMAIL_TEMPLATE, QuizzesPeer::ADMIN_EMAIL_TEMPLATE, QuizzesPeer::SUBMIT_BUTTON_TEXT, QuizzesPeer::NAME_FIELD_TEXT, QuizzesPeer::BUSINESS_FIELD_TEXT, QuizzesPeer::EMAIL_FIELD_TEXT, QuizzesPeer::PHONE_FIELD_TEXT, QuizzesPeer::COMMENT_FIELD_TEXT, QuizzesPeer::EMAIL_FROM_TEXT, QuizzesPeer::QUESTION_ANSWER_TEMPLATE, QuizzesPeer::LEADERBOARD_TEMPLATE, QuizzesPeer::SYSTEM, QuizzesPeer::RANDOMNESS_ORDER, QuizzesPeer::LOGGEDIN_USER_CONTACT, QuizzesPeer::SHOW_SCORE, QuizzesPeer::SEND_USER_EMAIL, QuizzesPeer::SEND_ADMIN_EMAIL, QuizzesPeer::CONTACT_INFO_LOCATION, QuizzesPeer::USER_NAME, QuizzesPeer::USER_COMP, QuizzesPeer::USER_EMAIL, QuizzesPeer::USER_PHONE, QuizzesPeer::ADMIN_EMAIL, QuizzesPeer::COMMENT_SECTION, QuizzesPeer::QUESTION_FROM_TOTAL, QuizzesPeer::TOTAL_USER_TRIES, QuizzesPeer::TOTAL_USER_TRIES_TEXT, QuizzesPeer::CERTIFICATE_TEMPLATE, QuizzesPeer::SOCIAL_MEDIA, QuizzesPeer::SOCIAL_MEDIA_TEXT, QuizzesPeer::PAGINATION, QuizzesPeer::PAGINATION_TEXT, QuizzesPeer::TIMER_LIMIT, QuizzesPeer::QUIZ_STYE, QuizzesPeer::QUESTION_NUMBERING, QuizzesPeer::QUIZ_SETTINGS, QuizzesPeer::THEME_SELECTED, QuizzesPeer::LAST_ACTIVITY, QuizzesPeer::REQUIRE_LOG_IN, QuizzesPeer::REQUIRE_LOG_IN_TEXT, QuizzesPeer::LIMIT_TOTAL_ENTRIES, QuizzesPeer::LIMIT_TOTAL_ENTRIES_TEXT, QuizzesPeer::SCHEDULED_TIMEFRAME, QuizzesPeer::SCHEDULED_TIMEFRAME_TEXT, QuizzesPeer::DISABLE_ANSWER_ONSELECT, QuizzesPeer::AJAX_SHOW_CORRECT, QuizzesPeer::QUIZ_VIEWS, QuizzesPeer::QUIZ_TAKEN, QuizzesPeer::DELETED, QuizzesPeer::QUIZ_DESCRIPTION, ),
        BasePeer::TYPE_RAW_COLNAME => array ('QUIZ_ID', 'QUIZ_NAME', 'MESSAGE_BEFORE', 'MESSAGE_AFTER', 'MESSAGE_COMMENT', 'MESSAGE_END_TEMPLATE', 'USER_EMAIL_TEMPLATE', 'ADMIN_EMAIL_TEMPLATE', 'SUBMIT_BUTTON_TEXT', 'NAME_FIELD_TEXT', 'BUSINESS_FIELD_TEXT', 'EMAIL_FIELD_TEXT', 'PHONE_FIELD_TEXT', 'COMMENT_FIELD_TEXT', 'EMAIL_FROM_TEXT', 'QUESTION_ANSWER_TEMPLATE', 'LEADERBOARD_TEMPLATE', 'SYSTEM', 'RANDOMNESS_ORDER', 'LOGGEDIN_USER_CONTACT', 'SHOW_SCORE', 'SEND_USER_EMAIL', 'SEND_ADMIN_EMAIL', 'CONTACT_INFO_LOCATION', 'USER_NAME', 'USER_COMP', 'USER_EMAIL', 'USER_PHONE', 'ADMIN_EMAIL', 'COMMENT_SECTION', 'QUESTION_FROM_TOTAL', 'TOTAL_USER_TRIES', 'TOTAL_USER_TRIES_TEXT', 'CERTIFICATE_TEMPLATE', 'SOCIAL_MEDIA', 'SOCIAL_MEDIA_TEXT', 'PAGINATION', 'PAGINATION_TEXT', 'TIMER_LIMIT', 'QUIZ_STYE', 'QUESTION_NUMBERING', 'QUIZ_SETTINGS', 'THEME_SELECTED', 'LAST_ACTIVITY', 'REQUIRE_LOG_IN', 'REQUIRE_LOG_IN_TEXT', 'LIMIT_TOTAL_ENTRIES', 'LIMIT_TOTAL_ENTRIES_TEXT', 'SCHEDULED_TIMEFRAME', 'SCHEDULED_TIMEFRAME_TEXT', 'DISABLE_ANSWER_ONSELECT', 'AJAX_SHOW_CORRECT', 'QUIZ_VIEWS', 'QUIZ_TAKEN', 'DELETED', 'QUIZ_DESCRIPTION', ),
        BasePeer::TYPE_FIELDNAME => array ('quiz_id', 'quiz_name', 'message_before', 'message_after', 'message_comment', 'message_end_template', 'user_email_template', 'admin_email_template', 'submit_button_text', 'name_field_text', 'business_field_text', 'email_field_text', 'phone_field_text', 'comment_field_text', 'email_from_text', 'question_answer_template', 'leaderboard_template', 'system', 'randomness_order', 'loggedin_user_contact', 'show_score', 'send_user_email', 'send_admin_email', 'contact_info_location', 'user_name', 'user_comp', 'user_email', 'user_phone', 'admin_email', 'comment_section', 'question_from_total', 'total_user_tries', 'total_user_tries_text', 'certificate_template', 'social_media', 'social_media_text', 'pagination', 'pagination_text', 'timer_limit', 'quiz_stye', 'question_numbering', 'quiz_settings', 'theme_selected', 'last_activity', 'require_log_in', 'require_log_in_text', 'limit_total_entries', 'limit_total_entries_text', 'scheduled_timeframe', 'scheduled_timeframe_text', 'disable_answer_onselect', 'ajax_show_correct', 'quiz_views', 'quiz_taken', 'deleted', 'quiz_description', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. QuizzesPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('QuizId' => 0, 'QuizName' => 1, 'MessageBefore' => 2, 'MessageAfter' => 3, 'MessageComment' => 4, 'MessageEndTemplate' => 5, 'UserEmailTemplate' => 6, 'AdminEmailTemplate' => 7, 'SubmitButtonText' => 8, 'NameFieldText' => 9, 'BusinessFieldText' => 10, 'EmailFieldText' => 11, 'PhoneFieldText' => 12, 'CommentFieldText' => 13, 'EmailFromText' => 14, 'QuestionAnswerTemplate' => 15, 'LeaderboardTemplate' => 16, 'System' => 17, 'RandomnessOrder' => 18, 'LoggedinUserContact' => 19, 'ShowScore' => 20, 'SendUserEmail' => 21, 'SendAdminEmail' => 22, 'ContactInfoLocation' => 23, 'UserName' => 24, 'UserComp' => 25, 'UserEmail' => 26, 'UserPhone' => 27, 'AdminEmail' => 28, 'CommentSection' => 29, 'QuestionFromTotal' => 30, 'TotalUserTries' => 31, 'TotalUserTriesText' => 32, 'CertificateTemplate' => 33, 'SocialMedia' => 34, 'SocialMediaText' => 35, 'Pagination' => 36, 'PaginationText' => 37, 'TimerLimit' => 38, 'QuizStye' => 39, 'QuestionNumbering' => 40, 'QuizSettings' => 41, 'ThemeSelected' => 42, 'LastActivity' => 43, 'RequireLogIn' => 44, 'RequireLogInText' => 45, 'LimitTotalEntries' => 46, 'LimitTotalEntriesText' => 47, 'ScheduledTimeframe' => 48, 'ScheduledTimeframeText' => 49, 'DisableAnswerOnselect' => 50, 'AjaxShowCorrect' => 51, 'QuizViews' => 52, 'QuizTaken' => 53, 'Deleted' => 54, 'QuizDescription' => 55, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('quizId' => 0, 'quizName' => 1, 'messageBefore' => 2, 'messageAfter' => 3, 'messageComment' => 4, 'messageEndTemplate' => 5, 'userEmailTemplate' => 6, 'adminEmailTemplate' => 7, 'submitButtonText' => 8, 'nameFieldText' => 9, 'businessFieldText' => 10, 'emailFieldText' => 11, 'phoneFieldText' => 12, 'commentFieldText' => 13, 'emailFromText' => 14, 'questionAnswerTemplate' => 15, 'leaderboardTemplate' => 16, 'system' => 17, 'randomnessOrder' => 18, 'loggedinUserContact' => 19, 'showScore' => 20, 'sendUserEmail' => 21, 'sendAdminEmail' => 22, 'contactInfoLocation' => 23, 'userName' => 24, 'userComp' => 25, 'userEmail' => 26, 'userPhone' => 27, 'adminEmail' => 28, 'commentSection' => 29, 'questionFromTotal' => 30, 'totalUserTries' => 31, 'totalUserTriesText' => 32, 'certificateTemplate' => 33, 'socialMedia' => 34, 'socialMediaText' => 35, 'pagination' => 36, 'paginationText' => 37, 'timerLimit' => 38, 'quizStye' => 39, 'questionNumbering' => 40, 'quizSettings' => 41, 'themeSelected' => 42, 'lastActivity' => 43, 'requireLogIn' => 44, 'requireLogInText' => 45, 'limitTotalEntries' => 46, 'limitTotalEntriesText' => 47, 'scheduledTimeframe' => 48, 'scheduledTimeframeText' => 49, 'disableAnswerOnselect' => 50, 'ajaxShowCorrect' => 51, 'quizViews' => 52, 'quizTaken' => 53, 'deleted' => 54, 'quizDescription' => 55, ),
        BasePeer::TYPE_COLNAME => array (QuizzesPeer::QUIZ_ID => 0, QuizzesPeer::QUIZ_NAME => 1, QuizzesPeer::MESSAGE_BEFORE => 2, QuizzesPeer::MESSAGE_AFTER => 3, QuizzesPeer::MESSAGE_COMMENT => 4, QuizzesPeer::MESSAGE_END_TEMPLATE => 5, QuizzesPeer::USER_EMAIL_TEMPLATE => 6, QuizzesPeer::ADMIN_EMAIL_TEMPLATE => 7, QuizzesPeer::SUBMIT_BUTTON_TEXT => 8, QuizzesPeer::NAME_FIELD_TEXT => 9, QuizzesPeer::BUSINESS_FIELD_TEXT => 10, QuizzesPeer::EMAIL_FIELD_TEXT => 11, QuizzesPeer::PHONE_FIELD_TEXT => 12, QuizzesPeer::COMMENT_FIELD_TEXT => 13, QuizzesPeer::EMAIL_FROM_TEXT => 14, QuizzesPeer::QUESTION_ANSWER_TEMPLATE => 15, QuizzesPeer::LEADERBOARD_TEMPLATE => 16, QuizzesPeer::SYSTEM => 17, QuizzesPeer::RANDOMNESS_ORDER => 18, QuizzesPeer::LOGGEDIN_USER_CONTACT => 19, QuizzesPeer::SHOW_SCORE => 20, QuizzesPeer::SEND_USER_EMAIL => 21, QuizzesPeer::SEND_ADMIN_EMAIL => 22, QuizzesPeer::CONTACT_INFO_LOCATION => 23, QuizzesPeer::USER_NAME => 24, QuizzesPeer::USER_COMP => 25, QuizzesPeer::USER_EMAIL => 26, QuizzesPeer::USER_PHONE => 27, QuizzesPeer::ADMIN_EMAIL => 28, QuizzesPeer::COMMENT_SECTION => 29, QuizzesPeer::QUESTION_FROM_TOTAL => 30, QuizzesPeer::TOTAL_USER_TRIES => 31, QuizzesPeer::TOTAL_USER_TRIES_TEXT => 32, QuizzesPeer::CERTIFICATE_TEMPLATE => 33, QuizzesPeer::SOCIAL_MEDIA => 34, QuizzesPeer::SOCIAL_MEDIA_TEXT => 35, QuizzesPeer::PAGINATION => 36, QuizzesPeer::PAGINATION_TEXT => 37, QuizzesPeer::TIMER_LIMIT => 38, QuizzesPeer::QUIZ_STYE => 39, QuizzesPeer::QUESTION_NUMBERING => 40, QuizzesPeer::QUIZ_SETTINGS => 41, QuizzesPeer::THEME_SELECTED => 42, QuizzesPeer::LAST_ACTIVITY => 43, QuizzesPeer::REQUIRE_LOG_IN => 44, QuizzesPeer::REQUIRE_LOG_IN_TEXT => 45, QuizzesPeer::LIMIT_TOTAL_ENTRIES => 46, QuizzesPeer::LIMIT_TOTAL_ENTRIES_TEXT => 47, QuizzesPeer::SCHEDULED_TIMEFRAME => 48, QuizzesPeer::SCHEDULED_TIMEFRAME_TEXT => 49, QuizzesPeer::DISABLE_ANSWER_ONSELECT => 50, QuizzesPeer::AJAX_SHOW_CORRECT => 51, QuizzesPeer::QUIZ_VIEWS => 52, QuizzesPeer::QUIZ_TAKEN => 53, QuizzesPeer::DELETED => 54, QuizzesPeer::QUIZ_DESCRIPTION => 55, ),
        BasePeer::TYPE_RAW_COLNAME => array ('QUIZ_ID' => 0, 'QUIZ_NAME' => 1, 'MESSAGE_BEFORE' => 2, 'MESSAGE_AFTER' => 3, 'MESSAGE_COMMENT' => 4, 'MESSAGE_END_TEMPLATE' => 5, 'USER_EMAIL_TEMPLATE' => 6, 'ADMIN_EMAIL_TEMPLATE' => 7, 'SUBMIT_BUTTON_TEXT' => 8, 'NAME_FIELD_TEXT' => 9, 'BUSINESS_FIELD_TEXT' => 10, 'EMAIL_FIELD_TEXT' => 11, 'PHONE_FIELD_TEXT' => 12, 'COMMENT_FIELD_TEXT' => 13, 'EMAIL_FROM_TEXT' => 14, 'QUESTION_ANSWER_TEMPLATE' => 15, 'LEADERBOARD_TEMPLATE' => 16, 'SYSTEM' => 17, 'RANDOMNESS_ORDER' => 18, 'LOGGEDIN_USER_CONTACT' => 19, 'SHOW_SCORE' => 20, 'SEND_USER_EMAIL' => 21, 'SEND_ADMIN_EMAIL' => 22, 'CONTACT_INFO_LOCATION' => 23, 'USER_NAME' => 24, 'USER_COMP' => 25, 'USER_EMAIL' => 26, 'USER_PHONE' => 27, 'ADMIN_EMAIL' => 28, 'COMMENT_SECTION' => 29, 'QUESTION_FROM_TOTAL' => 30, 'TOTAL_USER_TRIES' => 31, 'TOTAL_USER_TRIES_TEXT' => 32, 'CERTIFICATE_TEMPLATE' => 33, 'SOCIAL_MEDIA' => 34, 'SOCIAL_MEDIA_TEXT' => 35, 'PAGINATION' => 36, 'PAGINATION_TEXT' => 37, 'TIMER_LIMIT' => 38, 'QUIZ_STYE' => 39, 'QUESTION_NUMBERING' => 40, 'QUIZ_SETTINGS' => 41, 'THEME_SELECTED' => 42, 'LAST_ACTIVITY' => 43, 'REQUIRE_LOG_IN' => 44, 'REQUIRE_LOG_IN_TEXT' => 45, 'LIMIT_TOTAL_ENTRIES' => 46, 'LIMIT_TOTAL_ENTRIES_TEXT' => 47, 'SCHEDULED_TIMEFRAME' => 48, 'SCHEDULED_TIMEFRAME_TEXT' => 49, 'DISABLE_ANSWER_ONSELECT' => 50, 'AJAX_SHOW_CORRECT' => 51, 'QUIZ_VIEWS' => 52, 'QUIZ_TAKEN' => 53, 'DELETED' => 54, 'QUIZ_DESCRIPTION' => 55, ),
        BasePeer::TYPE_FIELDNAME => array ('quiz_id' => 0, 'quiz_name' => 1, 'message_before' => 2, 'message_after' => 3, 'message_comment' => 4, 'message_end_template' => 5, 'user_email_template' => 6, 'admin_email_template' => 7, 'submit_button_text' => 8, 'name_field_text' => 9, 'business_field_text' => 10, 'email_field_text' => 11, 'phone_field_text' => 12, 'comment_field_text' => 13, 'email_from_text' => 14, 'question_answer_template' => 15, 'leaderboard_template' => 16, 'system' => 17, 'randomness_order' => 18, 'loggedin_user_contact' => 19, 'show_score' => 20, 'send_user_email' => 21, 'send_admin_email' => 22, 'contact_info_location' => 23, 'user_name' => 24, 'user_comp' => 25, 'user_email' => 26, 'user_phone' => 27, 'admin_email' => 28, 'comment_section' => 29, 'question_from_total' => 30, 'total_user_tries' => 31, 'total_user_tries_text' => 32, 'certificate_template' => 33, 'social_media' => 34, 'social_media_text' => 35, 'pagination' => 36, 'pagination_text' => 37, 'timer_limit' => 38, 'quiz_stye' => 39, 'question_numbering' => 40, 'quiz_settings' => 41, 'theme_selected' => 42, 'last_activity' => 43, 'require_log_in' => 44, 'require_log_in_text' => 45, 'limit_total_entries' => 46, 'limit_total_entries_text' => 47, 'scheduled_timeframe' => 48, 'scheduled_timeframe_text' => 49, 'disable_answer_onselect' => 50, 'ajax_show_correct' => 51, 'quiz_views' => 52, 'quiz_taken' => 53, 'deleted' => 54, 'quiz_description' => 55, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = QuizzesPeer::getFieldNames($toType);
        $key = isset(QuizzesPeer::$fieldKeys[$fromType][$name]) ? QuizzesPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(QuizzesPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, QuizzesPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return QuizzesPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. QuizzesPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(QuizzesPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(QuizzesPeer::QUIZ_ID);
            $criteria->addSelectColumn(QuizzesPeer::QUIZ_NAME);
            $criteria->addSelectColumn(QuizzesPeer::MESSAGE_BEFORE);
            $criteria->addSelectColumn(QuizzesPeer::MESSAGE_AFTER);
            $criteria->addSelectColumn(QuizzesPeer::MESSAGE_COMMENT);
            $criteria->addSelectColumn(QuizzesPeer::MESSAGE_END_TEMPLATE);
            $criteria->addSelectColumn(QuizzesPeer::USER_EMAIL_TEMPLATE);
            $criteria->addSelectColumn(QuizzesPeer::ADMIN_EMAIL_TEMPLATE);
            $criteria->addSelectColumn(QuizzesPeer::SUBMIT_BUTTON_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::NAME_FIELD_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::BUSINESS_FIELD_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::EMAIL_FIELD_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::PHONE_FIELD_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::COMMENT_FIELD_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::EMAIL_FROM_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::QUESTION_ANSWER_TEMPLATE);
            $criteria->addSelectColumn(QuizzesPeer::LEADERBOARD_TEMPLATE);
            $criteria->addSelectColumn(QuizzesPeer::SYSTEM);
            $criteria->addSelectColumn(QuizzesPeer::RANDOMNESS_ORDER);
            $criteria->addSelectColumn(QuizzesPeer::LOGGEDIN_USER_CONTACT);
            $criteria->addSelectColumn(QuizzesPeer::SHOW_SCORE);
            $criteria->addSelectColumn(QuizzesPeer::SEND_USER_EMAIL);
            $criteria->addSelectColumn(QuizzesPeer::SEND_ADMIN_EMAIL);
            $criteria->addSelectColumn(QuizzesPeer::CONTACT_INFO_LOCATION);
            $criteria->addSelectColumn(QuizzesPeer::USER_NAME);
            $criteria->addSelectColumn(QuizzesPeer::USER_COMP);
            $criteria->addSelectColumn(QuizzesPeer::USER_EMAIL);
            $criteria->addSelectColumn(QuizzesPeer::USER_PHONE);
            $criteria->addSelectColumn(QuizzesPeer::ADMIN_EMAIL);
            $criteria->addSelectColumn(QuizzesPeer::COMMENT_SECTION);
            $criteria->addSelectColumn(QuizzesPeer::QUESTION_FROM_TOTAL);
            $criteria->addSelectColumn(QuizzesPeer::TOTAL_USER_TRIES);
            $criteria->addSelectColumn(QuizzesPeer::TOTAL_USER_TRIES_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::CERTIFICATE_TEMPLATE);
            $criteria->addSelectColumn(QuizzesPeer::SOCIAL_MEDIA);
            $criteria->addSelectColumn(QuizzesPeer::SOCIAL_MEDIA_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::PAGINATION);
            $criteria->addSelectColumn(QuizzesPeer::PAGINATION_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::TIMER_LIMIT);
            $criteria->addSelectColumn(QuizzesPeer::QUIZ_STYE);
            $criteria->addSelectColumn(QuizzesPeer::QUESTION_NUMBERING);
            $criteria->addSelectColumn(QuizzesPeer::QUIZ_SETTINGS);
            $criteria->addSelectColumn(QuizzesPeer::THEME_SELECTED);
            $criteria->addSelectColumn(QuizzesPeer::LAST_ACTIVITY);
            $criteria->addSelectColumn(QuizzesPeer::REQUIRE_LOG_IN);
            $criteria->addSelectColumn(QuizzesPeer::REQUIRE_LOG_IN_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::LIMIT_TOTAL_ENTRIES);
            $criteria->addSelectColumn(QuizzesPeer::LIMIT_TOTAL_ENTRIES_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::SCHEDULED_TIMEFRAME);
            $criteria->addSelectColumn(QuizzesPeer::SCHEDULED_TIMEFRAME_TEXT);
            $criteria->addSelectColumn(QuizzesPeer::DISABLE_ANSWER_ONSELECT);
            $criteria->addSelectColumn(QuizzesPeer::AJAX_SHOW_CORRECT);
            $criteria->addSelectColumn(QuizzesPeer::QUIZ_VIEWS);
            $criteria->addSelectColumn(QuizzesPeer::QUIZ_TAKEN);
            $criteria->addSelectColumn(QuizzesPeer::DELETED);
            $criteria->addSelectColumn(QuizzesPeer::QUIZ_DESCRIPTION);
        } else {
            $criteria->addSelectColumn($alias . '.quiz_id');
            $criteria->addSelectColumn($alias . '.quiz_name');
            $criteria->addSelectColumn($alias . '.message_before');
            $criteria->addSelectColumn($alias . '.message_after');
            $criteria->addSelectColumn($alias . '.message_comment');
            $criteria->addSelectColumn($alias . '.message_end_template');
            $criteria->addSelectColumn($alias . '.user_email_template');
            $criteria->addSelectColumn($alias . '.admin_email_template');
            $criteria->addSelectColumn($alias . '.submit_button_text');
            $criteria->addSelectColumn($alias . '.name_field_text');
            $criteria->addSelectColumn($alias . '.business_field_text');
            $criteria->addSelectColumn($alias . '.email_field_text');
            $criteria->addSelectColumn($alias . '.phone_field_text');
            $criteria->addSelectColumn($alias . '.comment_field_text');
            $criteria->addSelectColumn($alias . '.email_from_text');
            $criteria->addSelectColumn($alias . '.question_answer_template');
            $criteria->addSelectColumn($alias . '.leaderboard_template');
            $criteria->addSelectColumn($alias . '.system');
            $criteria->addSelectColumn($alias . '.randomness_order');
            $criteria->addSelectColumn($alias . '.loggedin_user_contact');
            $criteria->addSelectColumn($alias . '.show_score');
            $criteria->addSelectColumn($alias . '.send_user_email');
            $criteria->addSelectColumn($alias . '.send_admin_email');
            $criteria->addSelectColumn($alias . '.contact_info_location');
            $criteria->addSelectColumn($alias . '.user_name');
            $criteria->addSelectColumn($alias . '.user_comp');
            $criteria->addSelectColumn($alias . '.user_email');
            $criteria->addSelectColumn($alias . '.user_phone');
            $criteria->addSelectColumn($alias . '.admin_email');
            $criteria->addSelectColumn($alias . '.comment_section');
            $criteria->addSelectColumn($alias . '.question_from_total');
            $criteria->addSelectColumn($alias . '.total_user_tries');
            $criteria->addSelectColumn($alias . '.total_user_tries_text');
            $criteria->addSelectColumn($alias . '.certificate_template');
            $criteria->addSelectColumn($alias . '.social_media');
            $criteria->addSelectColumn($alias . '.social_media_text');
            $criteria->addSelectColumn($alias . '.pagination');
            $criteria->addSelectColumn($alias . '.pagination_text');
            $criteria->addSelectColumn($alias . '.timer_limit');
            $criteria->addSelectColumn($alias . '.quiz_stye');
            $criteria->addSelectColumn($alias . '.question_numbering');
            $criteria->addSelectColumn($alias . '.quiz_settings');
            $criteria->addSelectColumn($alias . '.theme_selected');
            $criteria->addSelectColumn($alias . '.last_activity');
            $criteria->addSelectColumn($alias . '.require_log_in');
            $criteria->addSelectColumn($alias . '.require_log_in_text');
            $criteria->addSelectColumn($alias . '.limit_total_entries');
            $criteria->addSelectColumn($alias . '.limit_total_entries_text');
            $criteria->addSelectColumn($alias . '.scheduled_timeframe');
            $criteria->addSelectColumn($alias . '.scheduled_timeframe_text');
            $criteria->addSelectColumn($alias . '.disable_answer_onselect');
            $criteria->addSelectColumn($alias . '.ajax_show_correct');
            $criteria->addSelectColumn($alias . '.quiz_views');
            $criteria->addSelectColumn($alias . '.quiz_taken');
            $criteria->addSelectColumn($alias . '.deleted');
            $criteria->addSelectColumn($alias . '.quiz_description');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(QuizzesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            QuizzesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(QuizzesPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return Quizzes
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = QuizzesPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return QuizzesPeer::populateObjects(QuizzesPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            QuizzesPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(QuizzesPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param Quizzes $obj A Quizzes object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getQuizId();
            } // if key === null
            QuizzesPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Quizzes object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Quizzes) {
                $key = (string) $value->getQuizId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Quizzes object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(QuizzesPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return Quizzes Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(QuizzesPeer::$instances[$key])) {
                return QuizzesPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (QuizzesPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        QuizzesPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to rfn_quizzes
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = QuizzesPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = QuizzesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = QuizzesPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                QuizzesPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Quizzes object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = QuizzesPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = QuizzesPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + QuizzesPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = QuizzesPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            QuizzesPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(QuizzesPeer::DATABASE_NAME)->getTable(QuizzesPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseQuizzesPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseQuizzesPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new \QuizzesTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return QuizzesPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Quizzes or Criteria object.
     *
     * @param      mixed $values Criteria or Quizzes object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Quizzes object
        }

        if ($criteria->containsKey(QuizzesPeer::QUIZ_ID) && $criteria->keyContainsValue(QuizzesPeer::QUIZ_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.QuizzesPeer::QUIZ_ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(QuizzesPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Quizzes or Criteria object.
     *
     * @param      mixed $values Criteria or Quizzes object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(QuizzesPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(QuizzesPeer::QUIZ_ID);
            $value = $criteria->remove(QuizzesPeer::QUIZ_ID);
            if ($value) {
                $selectCriteria->add(QuizzesPeer::QUIZ_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(QuizzesPeer::TABLE_NAME);
            }

        } else { // $values is Quizzes object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(QuizzesPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the rfn_quizzes table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(QuizzesPeer::TABLE_NAME, $con, QuizzesPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            QuizzesPeer::clearInstancePool();
            QuizzesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Quizzes or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Quizzes object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            QuizzesPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Quizzes) { // it's a model object
            // invalidate the cache for this single object
            QuizzesPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(QuizzesPeer::DATABASE_NAME);
            $criteria->add(QuizzesPeer::QUIZ_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                QuizzesPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(QuizzesPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            QuizzesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Quizzes object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param Quizzes $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(QuizzesPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(QuizzesPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(QuizzesPeer::DATABASE_NAME, QuizzesPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Quizzes
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = QuizzesPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(QuizzesPeer::DATABASE_NAME);
        $criteria->add(QuizzesPeer::QUIZ_ID, $pk);

        $v = QuizzesPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Quizzes[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuizzesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(QuizzesPeer::DATABASE_NAME);
            $criteria->add(QuizzesPeer::QUIZ_ID, $pks, Criteria::IN);
            $objs = QuizzesPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseQuizzesPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseQuizzesPeer::buildTableMap();

