<?php


/**
 * Base class that represents a query for the 'rfn_results' table.
 *
 *
 *
 * @method ResultsQuery orderByResultId($order = Criteria::ASC) Order by the result_id column
 * @method ResultsQuery orderByQuizId($order = Criteria::ASC) Order by the quiz_id column
 * @method ResultsQuery orderByQuizName($order = Criteria::ASC) Order by the quiz_name column
 * @method ResultsQuery orderByQuizSystem($order = Criteria::ASC) Order by the quiz_system column
 * @method ResultsQuery orderByPointScore($order = Criteria::ASC) Order by the point_score column
 * @method ResultsQuery orderByCorrectScore($order = Criteria::ASC) Order by the correct_score column
 * @method ResultsQuery orderByCorrect($order = Criteria::ASC) Order by the correct column
 * @method ResultsQuery orderByTotal($order = Criteria::ASC) Order by the total column
 * @method ResultsQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method ResultsQuery orderByBusiness($order = Criteria::ASC) Order by the business column
 * @method ResultsQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method ResultsQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method ResultsQuery orderByUser($order = Criteria::ASC) Order by the user column
 * @method ResultsQuery orderByUserIp($order = Criteria::ASC) Order by the user_ip column
 * @method ResultsQuery orderByTimeTaken($order = Criteria::ASC) Order by the time_taken column
 * @method ResultsQuery orderByTimeTakenReal($order = Criteria::ASC) Order by the time_taken_real column
 * @method ResultsQuery orderByQuizResults($order = Criteria::ASC) Order by the quiz_results column
 * @method ResultsQuery orderByDeleted($order = Criteria::ASC) Order by the deleted column
 *
 * @method ResultsQuery groupByResultId() Group by the result_id column
 * @method ResultsQuery groupByQuizId() Group by the quiz_id column
 * @method ResultsQuery groupByQuizName() Group by the quiz_name column
 * @method ResultsQuery groupByQuizSystem() Group by the quiz_system column
 * @method ResultsQuery groupByPointScore() Group by the point_score column
 * @method ResultsQuery groupByCorrectScore() Group by the correct_score column
 * @method ResultsQuery groupByCorrect() Group by the correct column
 * @method ResultsQuery groupByTotal() Group by the total column
 * @method ResultsQuery groupByName() Group by the name column
 * @method ResultsQuery groupByBusiness() Group by the business column
 * @method ResultsQuery groupByEmail() Group by the email column
 * @method ResultsQuery groupByPhone() Group by the phone column
 * @method ResultsQuery groupByUser() Group by the user column
 * @method ResultsQuery groupByUserIp() Group by the user_ip column
 * @method ResultsQuery groupByTimeTaken() Group by the time_taken column
 * @method ResultsQuery groupByTimeTakenReal() Group by the time_taken_real column
 * @method ResultsQuery groupByQuizResults() Group by the quiz_results column
 * @method ResultsQuery groupByDeleted() Group by the deleted column
 *
 * @method ResultsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ResultsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ResultsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Results findOne(PropelPDO $con = null) Return the first Results matching the query
 * @method Results findOneOrCreate(PropelPDO $con = null) Return the first Results matching the query, or a new Results object populated from the query conditions when no match is found
 *
 * @method Results findOneByQuizId(int $quiz_id) Return the first Results filtered by the quiz_id column
 * @method Results findOneByQuizName(string $quiz_name) Return the first Results filtered by the quiz_name column
 * @method Results findOneByQuizSystem(int $quiz_system) Return the first Results filtered by the quiz_system column
 * @method Results findOneByPointScore(int $point_score) Return the first Results filtered by the point_score column
 * @method Results findOneByCorrectScore(int $correct_score) Return the first Results filtered by the correct_score column
 * @method Results findOneByCorrect(int $correct) Return the first Results filtered by the correct column
 * @method Results findOneByTotal(int $total) Return the first Results filtered by the total column
 * @method Results findOneByName(string $name) Return the first Results filtered by the name column
 * @method Results findOneByBusiness(string $business) Return the first Results filtered by the business column
 * @method Results findOneByEmail(string $email) Return the first Results filtered by the email column
 * @method Results findOneByPhone(string $phone) Return the first Results filtered by the phone column
 * @method Results findOneByUser(int $user) Return the first Results filtered by the user column
 * @method Results findOneByUserIp(string $user_ip) Return the first Results filtered by the user_ip column
 * @method Results findOneByTimeTaken(string $time_taken) Return the first Results filtered by the time_taken column
 * @method Results findOneByTimeTakenReal(string $time_taken_real) Return the first Results filtered by the time_taken_real column
 * @method Results findOneByQuizResults(string $quiz_results) Return the first Results filtered by the quiz_results column
 * @method Results findOneByDeleted(int $deleted) Return the first Results filtered by the deleted column
 *
 * @method array findByResultId(int $result_id) Return Results objects filtered by the result_id column
 * @method array findByQuizId(int $quiz_id) Return Results objects filtered by the quiz_id column
 * @method array findByQuizName(string $quiz_name) Return Results objects filtered by the quiz_name column
 * @method array findByQuizSystem(int $quiz_system) Return Results objects filtered by the quiz_system column
 * @method array findByPointScore(int $point_score) Return Results objects filtered by the point_score column
 * @method array findByCorrectScore(int $correct_score) Return Results objects filtered by the correct_score column
 * @method array findByCorrect(int $correct) Return Results objects filtered by the correct column
 * @method array findByTotal(int $total) Return Results objects filtered by the total column
 * @method array findByName(string $name) Return Results objects filtered by the name column
 * @method array findByBusiness(string $business) Return Results objects filtered by the business column
 * @method array findByEmail(string $email) Return Results objects filtered by the email column
 * @method array findByPhone(string $phone) Return Results objects filtered by the phone column
 * @method array findByUser(int $user) Return Results objects filtered by the user column
 * @method array findByUserIp(string $user_ip) Return Results objects filtered by the user_ip column
 * @method array findByTimeTaken(string $time_taken) Return Results objects filtered by the time_taken column
 * @method array findByTimeTakenReal(string $time_taken_real) Return Results objects filtered by the time_taken_real column
 * @method array findByQuizResults(string $quiz_results) Return Results objects filtered by the quiz_results column
 * @method array findByDeleted(int $deleted) Return Results objects filtered by the deleted column
 *
 * @package    propel.generator.quizzes.om
 */
abstract class BaseResultsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseResultsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'rfn_new2';
        }
        if (null === $modelName) {
            $modelName = 'Results';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ResultsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ResultsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ResultsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ResultsQuery) {
            return $criteria;
        }
        $query = new ResultsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Results|Results[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ResultsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ResultsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Results A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByResultId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Results A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `result_id`, `quiz_id`, `quiz_name`, `quiz_system`, `point_score`, `correct_score`, `correct`, `total`, `name`, `business`, `email`, `phone`, `user`, `user_ip`, `time_taken`, `time_taken_real`, `quiz_results`, `deleted` FROM `rfn_results` WHERE `result_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Results();
            $obj->hydrate($row);
            ResultsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Results|Results[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Results[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ResultsPeer::RESULT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ResultsPeer::RESULT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the result_id column
     *
     * Example usage:
     * <code>
     * $query->filterByResultId(1234); // WHERE result_id = 1234
     * $query->filterByResultId(array(12, 34)); // WHERE result_id IN (12, 34)
     * $query->filterByResultId(array('min' => 12)); // WHERE result_id >= 12
     * $query->filterByResultId(array('max' => 12)); // WHERE result_id <= 12
     * </code>
     *
     * @param     mixed $resultId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByResultId($resultId = null, $comparison = null)
    {
        if (is_array($resultId)) {
            $useMinMax = false;
            if (isset($resultId['min'])) {
                $this->addUsingAlias(ResultsPeer::RESULT_ID, $resultId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($resultId['max'])) {
                $this->addUsingAlias(ResultsPeer::RESULT_ID, $resultId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResultsPeer::RESULT_ID, $resultId, $comparison);
    }

    /**
     * Filter the query on the quiz_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizId(1234); // WHERE quiz_id = 1234
     * $query->filterByQuizId(array(12, 34)); // WHERE quiz_id IN (12, 34)
     * $query->filterByQuizId(array('min' => 12)); // WHERE quiz_id >= 12
     * $query->filterByQuizId(array('max' => 12)); // WHERE quiz_id <= 12
     * </code>
     *
     * @param     mixed $quizId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByQuizId($quizId = null, $comparison = null)
    {
        if (is_array($quizId)) {
            $useMinMax = false;
            if (isset($quizId['min'])) {
                $this->addUsingAlias(ResultsPeer::QUIZ_ID, $quizId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quizId['max'])) {
                $this->addUsingAlias(ResultsPeer::QUIZ_ID, $quizId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResultsPeer::QUIZ_ID, $quizId, $comparison);
    }

    /**
     * Filter the query on the quiz_name column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizName('fooValue');   // WHERE quiz_name = 'fooValue'
     * $query->filterByQuizName('%fooValue%'); // WHERE quiz_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $quizName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByQuizName($quizName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quizName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $quizName)) {
                $quizName = str_replace('*', '%', $quizName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ResultsPeer::QUIZ_NAME, $quizName, $comparison);
    }

    /**
     * Filter the query on the quiz_system column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizSystem(1234); // WHERE quiz_system = 1234
     * $query->filterByQuizSystem(array(12, 34)); // WHERE quiz_system IN (12, 34)
     * $query->filterByQuizSystem(array('min' => 12)); // WHERE quiz_system >= 12
     * $query->filterByQuizSystem(array('max' => 12)); // WHERE quiz_system <= 12
     * </code>
     *
     * @param     mixed $quizSystem The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByQuizSystem($quizSystem = null, $comparison = null)
    {
        if (is_array($quizSystem)) {
            $useMinMax = false;
            if (isset($quizSystem['min'])) {
                $this->addUsingAlias(ResultsPeer::QUIZ_SYSTEM, $quizSystem['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quizSystem['max'])) {
                $this->addUsingAlias(ResultsPeer::QUIZ_SYSTEM, $quizSystem['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResultsPeer::QUIZ_SYSTEM, $quizSystem, $comparison);
    }

    /**
     * Filter the query on the point_score column
     *
     * Example usage:
     * <code>
     * $query->filterByPointScore(1234); // WHERE point_score = 1234
     * $query->filterByPointScore(array(12, 34)); // WHERE point_score IN (12, 34)
     * $query->filterByPointScore(array('min' => 12)); // WHERE point_score >= 12
     * $query->filterByPointScore(array('max' => 12)); // WHERE point_score <= 12
     * </code>
     *
     * @param     mixed $pointScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByPointScore($pointScore = null, $comparison = null)
    {
        if (is_array($pointScore)) {
            $useMinMax = false;
            if (isset($pointScore['min'])) {
                $this->addUsingAlias(ResultsPeer::POINT_SCORE, $pointScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pointScore['max'])) {
                $this->addUsingAlias(ResultsPeer::POINT_SCORE, $pointScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResultsPeer::POINT_SCORE, $pointScore, $comparison);
    }

    /**
     * Filter the query on the correct_score column
     *
     * Example usage:
     * <code>
     * $query->filterByCorrectScore(1234); // WHERE correct_score = 1234
     * $query->filterByCorrectScore(array(12, 34)); // WHERE correct_score IN (12, 34)
     * $query->filterByCorrectScore(array('min' => 12)); // WHERE correct_score >= 12
     * $query->filterByCorrectScore(array('max' => 12)); // WHERE correct_score <= 12
     * </code>
     *
     * @param     mixed $correctScore The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByCorrectScore($correctScore = null, $comparison = null)
    {
        if (is_array($correctScore)) {
            $useMinMax = false;
            if (isset($correctScore['min'])) {
                $this->addUsingAlias(ResultsPeer::CORRECT_SCORE, $correctScore['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($correctScore['max'])) {
                $this->addUsingAlias(ResultsPeer::CORRECT_SCORE, $correctScore['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResultsPeer::CORRECT_SCORE, $correctScore, $comparison);
    }

    /**
     * Filter the query on the correct column
     *
     * Example usage:
     * <code>
     * $query->filterByCorrect(1234); // WHERE correct = 1234
     * $query->filterByCorrect(array(12, 34)); // WHERE correct IN (12, 34)
     * $query->filterByCorrect(array('min' => 12)); // WHERE correct >= 12
     * $query->filterByCorrect(array('max' => 12)); // WHERE correct <= 12
     * </code>
     *
     * @param     mixed $correct The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByCorrect($correct = null, $comparison = null)
    {
        if (is_array($correct)) {
            $useMinMax = false;
            if (isset($correct['min'])) {
                $this->addUsingAlias(ResultsPeer::CORRECT, $correct['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($correct['max'])) {
                $this->addUsingAlias(ResultsPeer::CORRECT, $correct['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResultsPeer::CORRECT, $correct, $comparison);
    }

    /**
     * Filter the query on the total column
     *
     * Example usage:
     * <code>
     * $query->filterByTotal(1234); // WHERE total = 1234
     * $query->filterByTotal(array(12, 34)); // WHERE total IN (12, 34)
     * $query->filterByTotal(array('min' => 12)); // WHERE total >= 12
     * $query->filterByTotal(array('max' => 12)); // WHERE total <= 12
     * </code>
     *
     * @param     mixed $total The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByTotal($total = null, $comparison = null)
    {
        if (is_array($total)) {
            $useMinMax = false;
            if (isset($total['min'])) {
                $this->addUsingAlias(ResultsPeer::TOTAL, $total['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($total['max'])) {
                $this->addUsingAlias(ResultsPeer::TOTAL, $total['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResultsPeer::TOTAL, $total, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ResultsPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the business column
     *
     * Example usage:
     * <code>
     * $query->filterByBusiness('fooValue');   // WHERE business = 'fooValue'
     * $query->filterByBusiness('%fooValue%'); // WHERE business LIKE '%fooValue%'
     * </code>
     *
     * @param     string $business The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByBusiness($business = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($business)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $business)) {
                $business = str_replace('*', '%', $business);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ResultsPeer::BUSINESS, $business, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ResultsPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ResultsPeer::PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the user column
     *
     * Example usage:
     * <code>
     * $query->filterByUser(1234); // WHERE user = 1234
     * $query->filterByUser(array(12, 34)); // WHERE user IN (12, 34)
     * $query->filterByUser(array('min' => 12)); // WHERE user >= 12
     * $query->filterByUser(array('max' => 12)); // WHERE user <= 12
     * </code>
     *
     * @param     mixed $user The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByUser($user = null, $comparison = null)
    {
        if (is_array($user)) {
            $useMinMax = false;
            if (isset($user['min'])) {
                $this->addUsingAlias(ResultsPeer::USER, $user['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($user['max'])) {
                $this->addUsingAlias(ResultsPeer::USER, $user['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResultsPeer::USER, $user, $comparison);
    }

    /**
     * Filter the query on the user_ip column
     *
     * Example usage:
     * <code>
     * $query->filterByUserIp('fooValue');   // WHERE user_ip = 'fooValue'
     * $query->filterByUserIp('%fooValue%'); // WHERE user_ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userIp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByUserIp($userIp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userIp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $userIp)) {
                $userIp = str_replace('*', '%', $userIp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ResultsPeer::USER_IP, $userIp, $comparison);
    }

    /**
     * Filter the query on the time_taken column
     *
     * Example usage:
     * <code>
     * $query->filterByTimeTaken('fooValue');   // WHERE time_taken = 'fooValue'
     * $query->filterByTimeTaken('%fooValue%'); // WHERE time_taken LIKE '%fooValue%'
     * </code>
     *
     * @param     string $timeTaken The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByTimeTaken($timeTaken = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($timeTaken)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $timeTaken)) {
                $timeTaken = str_replace('*', '%', $timeTaken);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ResultsPeer::TIME_TAKEN, $timeTaken, $comparison);
    }

    /**
     * Filter the query on the time_taken_real column
     *
     * Example usage:
     * <code>
     * $query->filterByTimeTakenReal('2011-03-14'); // WHERE time_taken_real = '2011-03-14'
     * $query->filterByTimeTakenReal('now'); // WHERE time_taken_real = '2011-03-14'
     * $query->filterByTimeTakenReal(array('max' => 'yesterday')); // WHERE time_taken_real < '2011-03-13'
     * </code>
     *
     * @param     mixed $timeTakenReal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByTimeTakenReal($timeTakenReal = null, $comparison = null)
    {
        if (is_array($timeTakenReal)) {
            $useMinMax = false;
            if (isset($timeTakenReal['min'])) {
                $this->addUsingAlias(ResultsPeer::TIME_TAKEN_REAL, $timeTakenReal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($timeTakenReal['max'])) {
                $this->addUsingAlias(ResultsPeer::TIME_TAKEN_REAL, $timeTakenReal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResultsPeer::TIME_TAKEN_REAL, $timeTakenReal, $comparison);
    }

    /**
     * Filter the query on the quiz_results column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizResults('fooValue');   // WHERE quiz_results = 'fooValue'
     * $query->filterByQuizResults('%fooValue%'); // WHERE quiz_results LIKE '%fooValue%'
     * </code>
     *
     * @param     string $quizResults The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByQuizResults($quizResults = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quizResults)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $quizResults)) {
                $quizResults = str_replace('*', '%', $quizResults);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ResultsPeer::QUIZ_RESULTS, $quizResults, $comparison);
    }

    /**
     * Filter the query on the deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByDeleted(1234); // WHERE deleted = 1234
     * $query->filterByDeleted(array(12, 34)); // WHERE deleted IN (12, 34)
     * $query->filterByDeleted(array('min' => 12)); // WHERE deleted >= 12
     * $query->filterByDeleted(array('max' => 12)); // WHERE deleted <= 12
     * </code>
     *
     * @param     mixed $deleted The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function filterByDeleted($deleted = null, $comparison = null)
    {
        if (is_array($deleted)) {
            $useMinMax = false;
            if (isset($deleted['min'])) {
                $this->addUsingAlias(ResultsPeer::DELETED, $deleted['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deleted['max'])) {
                $this->addUsingAlias(ResultsPeer::DELETED, $deleted['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResultsPeer::DELETED, $deleted, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Results $results Object to remove from the list of results
     *
     * @return ResultsQuery The current query, for fluid interface
     */
    public function prune($results = null)
    {
        if ($results) {
            $this->addUsingAlias(ResultsPeer::RESULT_ID, $results->getResultId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
