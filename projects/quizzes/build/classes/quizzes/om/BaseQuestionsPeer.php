<?php


/**
 * Base static class for performing query and update operations on the 'rfn_questions' table.
 *
 *
 *
 * @package propel.generator.quizzes.om
 */
abstract class BaseQuestionsPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'rfn_new2';

    /** the table name for this class */
    const TABLE_NAME = 'rfn_questions';

    /** the related Propel class for this table */
    const OM_CLASS = 'Questions';

    /** the related TableMap class for this table */
    const TM_CLASS = 'QuestionsTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 26;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 26;

    /** the column name for the question_id field */
    const QUESTION_ID = 'rfn_questions.question_id';

    /** the column name for the quiz_id field */
    const QUIZ_ID = 'rfn_questions.quiz_id';

    /** the column name for the question_name field */
    const QUESTION_NAME = 'rfn_questions.question_name';

    /** the column name for the answer_array field */
    const ANSWER_ARRAY = 'rfn_questions.answer_array';

    /** the column name for the answer_one field */
    const ANSWER_ONE = 'rfn_questions.answer_one';

    /** the column name for the answer_one_points field */
    const ANSWER_ONE_POINTS = 'rfn_questions.answer_one_points';

    /** the column name for the answer_two field */
    const ANSWER_TWO = 'rfn_questions.answer_two';

    /** the column name for the answer_two_points field */
    const ANSWER_TWO_POINTS = 'rfn_questions.answer_two_points';

    /** the column name for the answer_three field */
    const ANSWER_THREE = 'rfn_questions.answer_three';

    /** the column name for the answer_three_points field */
    const ANSWER_THREE_POINTS = 'rfn_questions.answer_three_points';

    /** the column name for the answer_four field */
    const ANSWER_FOUR = 'rfn_questions.answer_four';

    /** the column name for the answer_four_points field */
    const ANSWER_FOUR_POINTS = 'rfn_questions.answer_four_points';

    /** the column name for the answer_five field */
    const ANSWER_FIVE = 'rfn_questions.answer_five';

    /** the column name for the answer_five_points field */
    const ANSWER_FIVE_POINTS = 'rfn_questions.answer_five_points';

    /** the column name for the answer_six field */
    const ANSWER_SIX = 'rfn_questions.answer_six';

    /** the column name for the answer_six_points field */
    const ANSWER_SIX_POINTS = 'rfn_questions.answer_six_points';

    /** the column name for the correct_answer field */
    const CORRECT_ANSWER = 'rfn_questions.correct_answer';

    /** the column name for the question_answer_info field */
    const QUESTION_ANSWER_INFO = 'rfn_questions.question_answer_info';

    /** the column name for the comments field */
    const COMMENTS = 'rfn_questions.comments';

    /** the column name for the hints field */
    const HINTS = 'rfn_questions.hints';

    /** the column name for the question_order field */
    const QUESTION_ORDER = 'rfn_questions.question_order';

    /** the column name for the question_type field */
    const QUESTION_TYPE = 'rfn_questions.question_type';

    /** the column name for the question_type_new field */
    const QUESTION_TYPE_NEW = 'rfn_questions.question_type_new';

    /** the column name for the question_settings field */
    const QUESTION_SETTINGS = 'rfn_questions.question_settings';

    /** the column name for the category field */
    const CATEGORY = 'rfn_questions.category';

    /** the column name for the deleted field */
    const DELETED = 'rfn_questions.deleted';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of Questions objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Questions[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. QuestionsPeer::$fieldNames[QuestionsPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('QuestionId', 'QuizId', 'QuestionName', 'AnswerArray', 'AnswerOne', 'AnswerOnePoints', 'AnswerTwo', 'AnswerTwoPoints', 'AnswerThree', 'AnswerThreePoints', 'AnswerFour', 'AnswerFourPoints', 'AnswerFive', 'AnswerFivePoints', 'AnswerSix', 'AnswerSixPoints', 'CorrectAnswer', 'QuestionAnswerInfo', 'Comments', 'Hints', 'QuestionOrder', 'QuestionType', 'QuestionTypeNew', 'QuestionSettings', 'Category', 'Deleted', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('questionId', 'quizId', 'questionName', 'answerArray', 'answerOne', 'answerOnePoints', 'answerTwo', 'answerTwoPoints', 'answerThree', 'answerThreePoints', 'answerFour', 'answerFourPoints', 'answerFive', 'answerFivePoints', 'answerSix', 'answerSixPoints', 'correctAnswer', 'questionAnswerInfo', 'comments', 'hints', 'questionOrder', 'questionType', 'questionTypeNew', 'questionSettings', 'category', 'deleted', ),
        BasePeer::TYPE_COLNAME => array (QuestionsPeer::QUESTION_ID, QuestionsPeer::QUIZ_ID, QuestionsPeer::QUESTION_NAME, QuestionsPeer::ANSWER_ARRAY, QuestionsPeer::ANSWER_ONE, QuestionsPeer::ANSWER_ONE_POINTS, QuestionsPeer::ANSWER_TWO, QuestionsPeer::ANSWER_TWO_POINTS, QuestionsPeer::ANSWER_THREE, QuestionsPeer::ANSWER_THREE_POINTS, QuestionsPeer::ANSWER_FOUR, QuestionsPeer::ANSWER_FOUR_POINTS, QuestionsPeer::ANSWER_FIVE, QuestionsPeer::ANSWER_FIVE_POINTS, QuestionsPeer::ANSWER_SIX, QuestionsPeer::ANSWER_SIX_POINTS, QuestionsPeer::CORRECT_ANSWER, QuestionsPeer::QUESTION_ANSWER_INFO, QuestionsPeer::COMMENTS, QuestionsPeer::HINTS, QuestionsPeer::QUESTION_ORDER, QuestionsPeer::QUESTION_TYPE, QuestionsPeer::QUESTION_TYPE_NEW, QuestionsPeer::QUESTION_SETTINGS, QuestionsPeer::CATEGORY, QuestionsPeer::DELETED, ),
        BasePeer::TYPE_RAW_COLNAME => array ('QUESTION_ID', 'QUIZ_ID', 'QUESTION_NAME', 'ANSWER_ARRAY', 'ANSWER_ONE', 'ANSWER_ONE_POINTS', 'ANSWER_TWO', 'ANSWER_TWO_POINTS', 'ANSWER_THREE', 'ANSWER_THREE_POINTS', 'ANSWER_FOUR', 'ANSWER_FOUR_POINTS', 'ANSWER_FIVE', 'ANSWER_FIVE_POINTS', 'ANSWER_SIX', 'ANSWER_SIX_POINTS', 'CORRECT_ANSWER', 'QUESTION_ANSWER_INFO', 'COMMENTS', 'HINTS', 'QUESTION_ORDER', 'QUESTION_TYPE', 'QUESTION_TYPE_NEW', 'QUESTION_SETTINGS', 'CATEGORY', 'DELETED', ),
        BasePeer::TYPE_FIELDNAME => array ('question_id', 'quiz_id', 'question_name', 'answer_array', 'answer_one', 'answer_one_points', 'answer_two', 'answer_two_points', 'answer_three', 'answer_three_points', 'answer_four', 'answer_four_points', 'answer_five', 'answer_five_points', 'answer_six', 'answer_six_points', 'correct_answer', 'question_answer_info', 'comments', 'hints', 'question_order', 'question_type', 'question_type_new', 'question_settings', 'category', 'deleted', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. QuestionsPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('QuestionId' => 0, 'QuizId' => 1, 'QuestionName' => 2, 'AnswerArray' => 3, 'AnswerOne' => 4, 'AnswerOnePoints' => 5, 'AnswerTwo' => 6, 'AnswerTwoPoints' => 7, 'AnswerThree' => 8, 'AnswerThreePoints' => 9, 'AnswerFour' => 10, 'AnswerFourPoints' => 11, 'AnswerFive' => 12, 'AnswerFivePoints' => 13, 'AnswerSix' => 14, 'AnswerSixPoints' => 15, 'CorrectAnswer' => 16, 'QuestionAnswerInfo' => 17, 'Comments' => 18, 'Hints' => 19, 'QuestionOrder' => 20, 'QuestionType' => 21, 'QuestionTypeNew' => 22, 'QuestionSettings' => 23, 'Category' => 24, 'Deleted' => 25, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('questionId' => 0, 'quizId' => 1, 'questionName' => 2, 'answerArray' => 3, 'answerOne' => 4, 'answerOnePoints' => 5, 'answerTwo' => 6, 'answerTwoPoints' => 7, 'answerThree' => 8, 'answerThreePoints' => 9, 'answerFour' => 10, 'answerFourPoints' => 11, 'answerFive' => 12, 'answerFivePoints' => 13, 'answerSix' => 14, 'answerSixPoints' => 15, 'correctAnswer' => 16, 'questionAnswerInfo' => 17, 'comments' => 18, 'hints' => 19, 'questionOrder' => 20, 'questionType' => 21, 'questionTypeNew' => 22, 'questionSettings' => 23, 'category' => 24, 'deleted' => 25, ),
        BasePeer::TYPE_COLNAME => array (QuestionsPeer::QUESTION_ID => 0, QuestionsPeer::QUIZ_ID => 1, QuestionsPeer::QUESTION_NAME => 2, QuestionsPeer::ANSWER_ARRAY => 3, QuestionsPeer::ANSWER_ONE => 4, QuestionsPeer::ANSWER_ONE_POINTS => 5, QuestionsPeer::ANSWER_TWO => 6, QuestionsPeer::ANSWER_TWO_POINTS => 7, QuestionsPeer::ANSWER_THREE => 8, QuestionsPeer::ANSWER_THREE_POINTS => 9, QuestionsPeer::ANSWER_FOUR => 10, QuestionsPeer::ANSWER_FOUR_POINTS => 11, QuestionsPeer::ANSWER_FIVE => 12, QuestionsPeer::ANSWER_FIVE_POINTS => 13, QuestionsPeer::ANSWER_SIX => 14, QuestionsPeer::ANSWER_SIX_POINTS => 15, QuestionsPeer::CORRECT_ANSWER => 16, QuestionsPeer::QUESTION_ANSWER_INFO => 17, QuestionsPeer::COMMENTS => 18, QuestionsPeer::HINTS => 19, QuestionsPeer::QUESTION_ORDER => 20, QuestionsPeer::QUESTION_TYPE => 21, QuestionsPeer::QUESTION_TYPE_NEW => 22, QuestionsPeer::QUESTION_SETTINGS => 23, QuestionsPeer::CATEGORY => 24, QuestionsPeer::DELETED => 25, ),
        BasePeer::TYPE_RAW_COLNAME => array ('QUESTION_ID' => 0, 'QUIZ_ID' => 1, 'QUESTION_NAME' => 2, 'ANSWER_ARRAY' => 3, 'ANSWER_ONE' => 4, 'ANSWER_ONE_POINTS' => 5, 'ANSWER_TWO' => 6, 'ANSWER_TWO_POINTS' => 7, 'ANSWER_THREE' => 8, 'ANSWER_THREE_POINTS' => 9, 'ANSWER_FOUR' => 10, 'ANSWER_FOUR_POINTS' => 11, 'ANSWER_FIVE' => 12, 'ANSWER_FIVE_POINTS' => 13, 'ANSWER_SIX' => 14, 'ANSWER_SIX_POINTS' => 15, 'CORRECT_ANSWER' => 16, 'QUESTION_ANSWER_INFO' => 17, 'COMMENTS' => 18, 'HINTS' => 19, 'QUESTION_ORDER' => 20, 'QUESTION_TYPE' => 21, 'QUESTION_TYPE_NEW' => 22, 'QUESTION_SETTINGS' => 23, 'CATEGORY' => 24, 'DELETED' => 25, ),
        BasePeer::TYPE_FIELDNAME => array ('question_id' => 0, 'quiz_id' => 1, 'question_name' => 2, 'answer_array' => 3, 'answer_one' => 4, 'answer_one_points' => 5, 'answer_two' => 6, 'answer_two_points' => 7, 'answer_three' => 8, 'answer_three_points' => 9, 'answer_four' => 10, 'answer_four_points' => 11, 'answer_five' => 12, 'answer_five_points' => 13, 'answer_six' => 14, 'answer_six_points' => 15, 'correct_answer' => 16, 'question_answer_info' => 17, 'comments' => 18, 'hints' => 19, 'question_order' => 20, 'question_type' => 21, 'question_type_new' => 22, 'question_settings' => 23, 'category' => 24, 'deleted' => 25, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = QuestionsPeer::getFieldNames($toType);
        $key = isset(QuestionsPeer::$fieldKeys[$fromType][$name]) ? QuestionsPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(QuestionsPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, QuestionsPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return QuestionsPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. QuestionsPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(QuestionsPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(QuestionsPeer::QUESTION_ID);
            $criteria->addSelectColumn(QuestionsPeer::QUIZ_ID);
            $criteria->addSelectColumn(QuestionsPeer::QUESTION_NAME);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_ARRAY);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_ONE);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_ONE_POINTS);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_TWO);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_TWO_POINTS);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_THREE);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_THREE_POINTS);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_FOUR);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_FOUR_POINTS);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_FIVE);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_FIVE_POINTS);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_SIX);
            $criteria->addSelectColumn(QuestionsPeer::ANSWER_SIX_POINTS);
            $criteria->addSelectColumn(QuestionsPeer::CORRECT_ANSWER);
            $criteria->addSelectColumn(QuestionsPeer::QUESTION_ANSWER_INFO);
            $criteria->addSelectColumn(QuestionsPeer::COMMENTS);
            $criteria->addSelectColumn(QuestionsPeer::HINTS);
            $criteria->addSelectColumn(QuestionsPeer::QUESTION_ORDER);
            $criteria->addSelectColumn(QuestionsPeer::QUESTION_TYPE);
            $criteria->addSelectColumn(QuestionsPeer::QUESTION_TYPE_NEW);
            $criteria->addSelectColumn(QuestionsPeer::QUESTION_SETTINGS);
            $criteria->addSelectColumn(QuestionsPeer::CATEGORY);
            $criteria->addSelectColumn(QuestionsPeer::DELETED);
        } else {
            $criteria->addSelectColumn($alias . '.question_id');
            $criteria->addSelectColumn($alias . '.quiz_id');
            $criteria->addSelectColumn($alias . '.question_name');
            $criteria->addSelectColumn($alias . '.answer_array');
            $criteria->addSelectColumn($alias . '.answer_one');
            $criteria->addSelectColumn($alias . '.answer_one_points');
            $criteria->addSelectColumn($alias . '.answer_two');
            $criteria->addSelectColumn($alias . '.answer_two_points');
            $criteria->addSelectColumn($alias . '.answer_three');
            $criteria->addSelectColumn($alias . '.answer_three_points');
            $criteria->addSelectColumn($alias . '.answer_four');
            $criteria->addSelectColumn($alias . '.answer_four_points');
            $criteria->addSelectColumn($alias . '.answer_five');
            $criteria->addSelectColumn($alias . '.answer_five_points');
            $criteria->addSelectColumn($alias . '.answer_six');
            $criteria->addSelectColumn($alias . '.answer_six_points');
            $criteria->addSelectColumn($alias . '.correct_answer');
            $criteria->addSelectColumn($alias . '.question_answer_info');
            $criteria->addSelectColumn($alias . '.comments');
            $criteria->addSelectColumn($alias . '.hints');
            $criteria->addSelectColumn($alias . '.question_order');
            $criteria->addSelectColumn($alias . '.question_type');
            $criteria->addSelectColumn($alias . '.question_type_new');
            $criteria->addSelectColumn($alias . '.question_settings');
            $criteria->addSelectColumn($alias . '.category');
            $criteria->addSelectColumn($alias . '.deleted');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(QuestionsPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            QuestionsPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(QuestionsPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return Questions
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = QuestionsPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return QuestionsPeer::populateObjects(QuestionsPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            QuestionsPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(QuestionsPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param Questions $obj A Questions object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getQuestionId();
            } // if key === null
            QuestionsPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Questions object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Questions) {
                $key = (string) $value->getQuestionId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Questions object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(QuestionsPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return Questions Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(QuestionsPeer::$instances[$key])) {
                return QuestionsPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (QuestionsPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        QuestionsPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to rfn_questions
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = QuestionsPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = QuestionsPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = QuestionsPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                QuestionsPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Questions object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = QuestionsPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = QuestionsPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + QuestionsPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = QuestionsPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            QuestionsPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(QuestionsPeer::DATABASE_NAME)->getTable(QuestionsPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseQuestionsPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseQuestionsPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new \QuestionsTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return QuestionsPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Questions or Criteria object.
     *
     * @param      mixed $values Criteria or Questions object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Questions object
        }

        if ($criteria->containsKey(QuestionsPeer::QUESTION_ID) && $criteria->keyContainsValue(QuestionsPeer::QUESTION_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.QuestionsPeer::QUESTION_ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(QuestionsPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Questions or Criteria object.
     *
     * @param      mixed $values Criteria or Questions object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(QuestionsPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(QuestionsPeer::QUESTION_ID);
            $value = $criteria->remove(QuestionsPeer::QUESTION_ID);
            if ($value) {
                $selectCriteria->add(QuestionsPeer::QUESTION_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(QuestionsPeer::TABLE_NAME);
            }

        } else { // $values is Questions object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(QuestionsPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the rfn_questions table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(QuestionsPeer::TABLE_NAME, $con, QuestionsPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            QuestionsPeer::clearInstancePool();
            QuestionsPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Questions or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Questions object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            QuestionsPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Questions) { // it's a model object
            // invalidate the cache for this single object
            QuestionsPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(QuestionsPeer::DATABASE_NAME);
            $criteria->add(QuestionsPeer::QUESTION_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                QuestionsPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(QuestionsPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            QuestionsPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Questions object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param Questions $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(QuestionsPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(QuestionsPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(QuestionsPeer::DATABASE_NAME, QuestionsPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Questions
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = QuestionsPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(QuestionsPeer::DATABASE_NAME);
        $criteria->add(QuestionsPeer::QUESTION_ID, $pk);

        $v = QuestionsPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Questions[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(QuestionsPeer::DATABASE_NAME);
            $criteria->add(QuestionsPeer::QUESTION_ID, $pks, Criteria::IN);
            $objs = QuestionsPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseQuestionsPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseQuestionsPeer::buildTableMap();

