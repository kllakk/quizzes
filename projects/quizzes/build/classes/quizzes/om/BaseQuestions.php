<?php


/**
 * Base class that represents a row from the 'rfn_questions' table.
 *
 *
 *
 * @package    propel.generator.quizzes.om
 */
abstract class BaseQuestions extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'QuestionsPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        QuestionsPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the question_id field.
     * @var        int
     */
    protected $question_id;

    /**
     * The value for the quiz_id field.
     * @var        int
     */
    protected $quiz_id;

    /**
     * The value for the question_name field.
     * @var        string
     */
    protected $question_name;

    /**
     * The value for the answer_array field.
     * @var        string
     */
    protected $answer_array;

    /**
     * The value for the answer_one field.
     * @var        string
     */
    protected $answer_one;

    /**
     * The value for the answer_one_points field.
     * @var        int
     */
    protected $answer_one_points;

    /**
     * The value for the answer_two field.
     * @var        string
     */
    protected $answer_two;

    /**
     * The value for the answer_two_points field.
     * @var        int
     */
    protected $answer_two_points;

    /**
     * The value for the answer_three field.
     * @var        string
     */
    protected $answer_three;

    /**
     * The value for the answer_three_points field.
     * @var        int
     */
    protected $answer_three_points;

    /**
     * The value for the answer_four field.
     * @var        string
     */
    protected $answer_four;

    /**
     * The value for the answer_four_points field.
     * @var        int
     */
    protected $answer_four_points;

    /**
     * The value for the answer_five field.
     * @var        string
     */
    protected $answer_five;

    /**
     * The value for the answer_five_points field.
     * @var        int
     */
    protected $answer_five_points;

    /**
     * The value for the answer_six field.
     * @var        string
     */
    protected $answer_six;

    /**
     * The value for the answer_six_points field.
     * @var        int
     */
    protected $answer_six_points;

    /**
     * The value for the correct_answer field.
     * @var        int
     */
    protected $correct_answer;

    /**
     * The value for the question_answer_info field.
     * @var        string
     */
    protected $question_answer_info;

    /**
     * The value for the comments field.
     * @var        int
     */
    protected $comments;

    /**
     * The value for the hints field.
     * @var        string
     */
    protected $hints;

    /**
     * The value for the question_order field.
     * @var        int
     */
    protected $question_order;

    /**
     * The value for the question_type field.
     * @var        int
     */
    protected $question_type;

    /**
     * The value for the question_type_new field.
     * @var        string
     */
    protected $question_type_new;

    /**
     * The value for the question_settings field.
     * @var        string
     */
    protected $question_settings;

    /**
     * The value for the category field.
     * @var        string
     */
    protected $category;

    /**
     * The value for the deleted field.
     * @var        int
     */
    protected $deleted;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [question_id] column value.
     *
     * @return int
     */
    public function getQuestionId()
    {

        return $this->question_id;
    }

    /**
     * Get the [quiz_id] column value.
     *
     * @return int
     */
    public function getQuizId()
    {

        return $this->quiz_id;
    }

    /**
     * Get the [question_name] column value.
     *
     * @return string
     */
    public function getQuestionName()
    {

        return $this->question_name;
    }

    /**
     * Get the [answer_array] column value.
     *
     * @return string
     */
    public function getAnswerArray()
    {

        return $this->answer_array;
    }

    /**
     * Get the [answer_one] column value.
     *
     * @return string
     */
    public function getAnswerOne()
    {

        return $this->answer_one;
    }

    /**
     * Get the [answer_one_points] column value.
     *
     * @return int
     */
    public function getAnswerOnePoints()
    {

        return $this->answer_one_points;
    }

    /**
     * Get the [answer_two] column value.
     *
     * @return string
     */
    public function getAnswerTwo()
    {

        return $this->answer_two;
    }

    /**
     * Get the [answer_two_points] column value.
     *
     * @return int
     */
    public function getAnswerTwoPoints()
    {

        return $this->answer_two_points;
    }

    /**
     * Get the [answer_three] column value.
     *
     * @return string
     */
    public function getAnswerThree()
    {

        return $this->answer_three;
    }

    /**
     * Get the [answer_three_points] column value.
     *
     * @return int
     */
    public function getAnswerThreePoints()
    {

        return $this->answer_three_points;
    }

    /**
     * Get the [answer_four] column value.
     *
     * @return string
     */
    public function getAnswerFour()
    {

        return $this->answer_four;
    }

    /**
     * Get the [answer_four_points] column value.
     *
     * @return int
     */
    public function getAnswerFourPoints()
    {

        return $this->answer_four_points;
    }

    /**
     * Get the [answer_five] column value.
     *
     * @return string
     */
    public function getAnswerFive()
    {

        return $this->answer_five;
    }

    /**
     * Get the [answer_five_points] column value.
     *
     * @return int
     */
    public function getAnswerFivePoints()
    {

        return $this->answer_five_points;
    }

    /**
     * Get the [answer_six] column value.
     *
     * @return string
     */
    public function getAnswerSix()
    {

        return $this->answer_six;
    }

    /**
     * Get the [answer_six_points] column value.
     *
     * @return int
     */
    public function getAnswerSixPoints()
    {

        return $this->answer_six_points;
    }

    /**
     * Get the [correct_answer] column value.
     *
     * @return int
     */
    public function getCorrectAnswer()
    {

        return $this->correct_answer;
    }

    /**
     * Get the [question_answer_info] column value.
     *
     * @return string
     */
    public function getQuestionAnswerInfo()
    {

        return $this->question_answer_info;
    }

    /**
     * Get the [comments] column value.
     *
     * @return int
     */
    public function getComments()
    {

        return $this->comments;
    }

    /**
     * Get the [hints] column value.
     *
     * @return string
     */
    public function getHints()
    {

        return $this->hints;
    }

    /**
     * Get the [question_order] column value.
     *
     * @return int
     */
    public function getQuestionOrder()
    {

        return $this->question_order;
    }

    /**
     * Get the [question_type] column value.
     *
     * @return int
     */
    public function getQuestionType()
    {

        return $this->question_type;
    }

    /**
     * Get the [question_type_new] column value.
     *
     * @return string
     */
    public function getQuestionTypeNew()
    {

        return $this->question_type_new;
    }

    /**
     * Get the [question_settings] column value.
     *
     * @return string
     */
    public function getQuestionSettings()
    {

        return $this->question_settings;
    }

    /**
     * Get the [category] column value.
     *
     * @return string
     */
    public function getCategory()
    {

        return $this->category;
    }

    /**
     * Get the [deleted] column value.
     *
     * @return int
     */
    public function getDeleted()
    {

        return $this->deleted;
    }

    /**
     * Set the value of [question_id] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setQuestionId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->question_id !== $v) {
            $this->question_id = $v;
            $this->modifiedColumns[] = QuestionsPeer::QUESTION_ID;
        }


        return $this;
    } // setQuestionId()

    /**
     * Set the value of [quiz_id] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setQuizId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->quiz_id !== $v) {
            $this->quiz_id = $v;
            $this->modifiedColumns[] = QuestionsPeer::QUIZ_ID;
        }


        return $this;
    } // setQuizId()

    /**
     * Set the value of [question_name] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setQuestionName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->question_name !== $v) {
            $this->question_name = $v;
            $this->modifiedColumns[] = QuestionsPeer::QUESTION_NAME;
        }


        return $this;
    } // setQuestionName()

    /**
     * Set the value of [answer_array] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerArray($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->answer_array !== $v) {
            $this->answer_array = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_ARRAY;
        }


        return $this;
    } // setAnswerArray()

    /**
     * Set the value of [answer_one] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerOne($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->answer_one !== $v) {
            $this->answer_one = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_ONE;
        }


        return $this;
    } // setAnswerOne()

    /**
     * Set the value of [answer_one_points] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerOnePoints($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->answer_one_points !== $v) {
            $this->answer_one_points = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_ONE_POINTS;
        }


        return $this;
    } // setAnswerOnePoints()

    /**
     * Set the value of [answer_two] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerTwo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->answer_two !== $v) {
            $this->answer_two = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_TWO;
        }


        return $this;
    } // setAnswerTwo()

    /**
     * Set the value of [answer_two_points] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerTwoPoints($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->answer_two_points !== $v) {
            $this->answer_two_points = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_TWO_POINTS;
        }


        return $this;
    } // setAnswerTwoPoints()

    /**
     * Set the value of [answer_three] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerThree($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->answer_three !== $v) {
            $this->answer_three = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_THREE;
        }


        return $this;
    } // setAnswerThree()

    /**
     * Set the value of [answer_three_points] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerThreePoints($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->answer_three_points !== $v) {
            $this->answer_three_points = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_THREE_POINTS;
        }


        return $this;
    } // setAnswerThreePoints()

    /**
     * Set the value of [answer_four] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerFour($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->answer_four !== $v) {
            $this->answer_four = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_FOUR;
        }


        return $this;
    } // setAnswerFour()

    /**
     * Set the value of [answer_four_points] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerFourPoints($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->answer_four_points !== $v) {
            $this->answer_four_points = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_FOUR_POINTS;
        }


        return $this;
    } // setAnswerFourPoints()

    /**
     * Set the value of [answer_five] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerFive($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->answer_five !== $v) {
            $this->answer_five = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_FIVE;
        }


        return $this;
    } // setAnswerFive()

    /**
     * Set the value of [answer_five_points] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerFivePoints($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->answer_five_points !== $v) {
            $this->answer_five_points = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_FIVE_POINTS;
        }


        return $this;
    } // setAnswerFivePoints()

    /**
     * Set the value of [answer_six] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerSix($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->answer_six !== $v) {
            $this->answer_six = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_SIX;
        }


        return $this;
    } // setAnswerSix()

    /**
     * Set the value of [answer_six_points] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setAnswerSixPoints($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->answer_six_points !== $v) {
            $this->answer_six_points = $v;
            $this->modifiedColumns[] = QuestionsPeer::ANSWER_SIX_POINTS;
        }


        return $this;
    } // setAnswerSixPoints()

    /**
     * Set the value of [correct_answer] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setCorrectAnswer($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->correct_answer !== $v) {
            $this->correct_answer = $v;
            $this->modifiedColumns[] = QuestionsPeer::CORRECT_ANSWER;
        }


        return $this;
    } // setCorrectAnswer()

    /**
     * Set the value of [question_answer_info] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setQuestionAnswerInfo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->question_answer_info !== $v) {
            $this->question_answer_info = $v;
            $this->modifiedColumns[] = QuestionsPeer::QUESTION_ANSWER_INFO;
        }


        return $this;
    } // setQuestionAnswerInfo()

    /**
     * Set the value of [comments] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setComments($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->comments !== $v) {
            $this->comments = $v;
            $this->modifiedColumns[] = QuestionsPeer::COMMENTS;
        }


        return $this;
    } // setComments()

    /**
     * Set the value of [hints] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setHints($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->hints !== $v) {
            $this->hints = $v;
            $this->modifiedColumns[] = QuestionsPeer::HINTS;
        }


        return $this;
    } // setHints()

    /**
     * Set the value of [question_order] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setQuestionOrder($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->question_order !== $v) {
            $this->question_order = $v;
            $this->modifiedColumns[] = QuestionsPeer::QUESTION_ORDER;
        }


        return $this;
    } // setQuestionOrder()

    /**
     * Set the value of [question_type] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setQuestionType($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->question_type !== $v) {
            $this->question_type = $v;
            $this->modifiedColumns[] = QuestionsPeer::QUESTION_TYPE;
        }


        return $this;
    } // setQuestionType()

    /**
     * Set the value of [question_type_new] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setQuestionTypeNew($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->question_type_new !== $v) {
            $this->question_type_new = $v;
            $this->modifiedColumns[] = QuestionsPeer::QUESTION_TYPE_NEW;
        }


        return $this;
    } // setQuestionTypeNew()

    /**
     * Set the value of [question_settings] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setQuestionSettings($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->question_settings !== $v) {
            $this->question_settings = $v;
            $this->modifiedColumns[] = QuestionsPeer::QUESTION_SETTINGS;
        }


        return $this;
    } // setQuestionSettings()

    /**
     * Set the value of [category] column.
     *
     * @param  string $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setCategory($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->category !== $v) {
            $this->category = $v;
            $this->modifiedColumns[] = QuestionsPeer::CATEGORY;
        }


        return $this;
    } // setCategory()

    /**
     * Set the value of [deleted] column.
     *
     * @param  int $v new value
     * @return Questions The current object (for fluent API support)
     */
    public function setDeleted($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->deleted !== $v) {
            $this->deleted = $v;
            $this->modifiedColumns[] = QuestionsPeer::DELETED;
        }


        return $this;
    } // setDeleted()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->question_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->quiz_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->question_name = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->answer_array = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->answer_one = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->answer_one_points = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->answer_two = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->answer_two_points = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->answer_three = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->answer_three_points = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->answer_four = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->answer_four_points = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->answer_five = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->answer_five_points = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->answer_six = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->answer_six_points = ($row[$startcol + 15] !== null) ? (int) $row[$startcol + 15] : null;
            $this->correct_answer = ($row[$startcol + 16] !== null) ? (int) $row[$startcol + 16] : null;
            $this->question_answer_info = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->comments = ($row[$startcol + 18] !== null) ? (int) $row[$startcol + 18] : null;
            $this->hints = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->question_order = ($row[$startcol + 20] !== null) ? (int) $row[$startcol + 20] : null;
            $this->question_type = ($row[$startcol + 21] !== null) ? (int) $row[$startcol + 21] : null;
            $this->question_type_new = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->question_settings = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->category = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->deleted = ($row[$startcol + 25] !== null) ? (int) $row[$startcol + 25] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 26; // 26 = QuestionsPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Questions object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = QuestionsPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = QuestionsQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                QuestionsPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = QuestionsPeer::QUESTION_ID;
        if (null !== $this->question_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . QuestionsPeer::QUESTION_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(QuestionsPeer::QUESTION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`question_id`';
        }
        if ($this->isColumnModified(QuestionsPeer::QUIZ_ID)) {
            $modifiedColumns[':p' . $index++]  = '`quiz_id`';
        }
        if ($this->isColumnModified(QuestionsPeer::QUESTION_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`question_name`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_ARRAY)) {
            $modifiedColumns[':p' . $index++]  = '`answer_array`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_ONE)) {
            $modifiedColumns[':p' . $index++]  = '`answer_one`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_ONE_POINTS)) {
            $modifiedColumns[':p' . $index++]  = '`answer_one_points`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_TWO)) {
            $modifiedColumns[':p' . $index++]  = '`answer_two`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_TWO_POINTS)) {
            $modifiedColumns[':p' . $index++]  = '`answer_two_points`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_THREE)) {
            $modifiedColumns[':p' . $index++]  = '`answer_three`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_THREE_POINTS)) {
            $modifiedColumns[':p' . $index++]  = '`answer_three_points`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_FOUR)) {
            $modifiedColumns[':p' . $index++]  = '`answer_four`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_FOUR_POINTS)) {
            $modifiedColumns[':p' . $index++]  = '`answer_four_points`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_FIVE)) {
            $modifiedColumns[':p' . $index++]  = '`answer_five`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_FIVE_POINTS)) {
            $modifiedColumns[':p' . $index++]  = '`answer_five_points`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_SIX)) {
            $modifiedColumns[':p' . $index++]  = '`answer_six`';
        }
        if ($this->isColumnModified(QuestionsPeer::ANSWER_SIX_POINTS)) {
            $modifiedColumns[':p' . $index++]  = '`answer_six_points`';
        }
        if ($this->isColumnModified(QuestionsPeer::CORRECT_ANSWER)) {
            $modifiedColumns[':p' . $index++]  = '`correct_answer`';
        }
        if ($this->isColumnModified(QuestionsPeer::QUESTION_ANSWER_INFO)) {
            $modifiedColumns[':p' . $index++]  = '`question_answer_info`';
        }
        if ($this->isColumnModified(QuestionsPeer::COMMENTS)) {
            $modifiedColumns[':p' . $index++]  = '`comments`';
        }
        if ($this->isColumnModified(QuestionsPeer::HINTS)) {
            $modifiedColumns[':p' . $index++]  = '`hints`';
        }
        if ($this->isColumnModified(QuestionsPeer::QUESTION_ORDER)) {
            $modifiedColumns[':p' . $index++]  = '`question_order`';
        }
        if ($this->isColumnModified(QuestionsPeer::QUESTION_TYPE)) {
            $modifiedColumns[':p' . $index++]  = '`question_type`';
        }
        if ($this->isColumnModified(QuestionsPeer::QUESTION_TYPE_NEW)) {
            $modifiedColumns[':p' . $index++]  = '`question_type_new`';
        }
        if ($this->isColumnModified(QuestionsPeer::QUESTION_SETTINGS)) {
            $modifiedColumns[':p' . $index++]  = '`question_settings`';
        }
        if ($this->isColumnModified(QuestionsPeer::CATEGORY)) {
            $modifiedColumns[':p' . $index++]  = '`category`';
        }
        if ($this->isColumnModified(QuestionsPeer::DELETED)) {
            $modifiedColumns[':p' . $index++]  = '`deleted`';
        }

        $sql = sprintf(
            'INSERT INTO `rfn_questions` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`question_id`':
                        $stmt->bindValue($identifier, $this->question_id, PDO::PARAM_INT);
                        break;
                    case '`quiz_id`':
                        $stmt->bindValue($identifier, $this->quiz_id, PDO::PARAM_INT);
                        break;
                    case '`question_name`':
                        $stmt->bindValue($identifier, $this->question_name, PDO::PARAM_STR);
                        break;
                    case '`answer_array`':
                        $stmt->bindValue($identifier, $this->answer_array, PDO::PARAM_STR);
                        break;
                    case '`answer_one`':
                        $stmt->bindValue($identifier, $this->answer_one, PDO::PARAM_STR);
                        break;
                    case '`answer_one_points`':
                        $stmt->bindValue($identifier, $this->answer_one_points, PDO::PARAM_INT);
                        break;
                    case '`answer_two`':
                        $stmt->bindValue($identifier, $this->answer_two, PDO::PARAM_STR);
                        break;
                    case '`answer_two_points`':
                        $stmt->bindValue($identifier, $this->answer_two_points, PDO::PARAM_INT);
                        break;
                    case '`answer_three`':
                        $stmt->bindValue($identifier, $this->answer_three, PDO::PARAM_STR);
                        break;
                    case '`answer_three_points`':
                        $stmt->bindValue($identifier, $this->answer_three_points, PDO::PARAM_INT);
                        break;
                    case '`answer_four`':
                        $stmt->bindValue($identifier, $this->answer_four, PDO::PARAM_STR);
                        break;
                    case '`answer_four_points`':
                        $stmt->bindValue($identifier, $this->answer_four_points, PDO::PARAM_INT);
                        break;
                    case '`answer_five`':
                        $stmt->bindValue($identifier, $this->answer_five, PDO::PARAM_STR);
                        break;
                    case '`answer_five_points`':
                        $stmt->bindValue($identifier, $this->answer_five_points, PDO::PARAM_INT);
                        break;
                    case '`answer_six`':
                        $stmt->bindValue($identifier, $this->answer_six, PDO::PARAM_STR);
                        break;
                    case '`answer_six_points`':
                        $stmt->bindValue($identifier, $this->answer_six_points, PDO::PARAM_INT);
                        break;
                    case '`correct_answer`':
                        $stmt->bindValue($identifier, $this->correct_answer, PDO::PARAM_INT);
                        break;
                    case '`question_answer_info`':
                        $stmt->bindValue($identifier, $this->question_answer_info, PDO::PARAM_STR);
                        break;
                    case '`comments`':
                        $stmt->bindValue($identifier, $this->comments, PDO::PARAM_INT);
                        break;
                    case '`hints`':
                        $stmt->bindValue($identifier, $this->hints, PDO::PARAM_STR);
                        break;
                    case '`question_order`':
                        $stmt->bindValue($identifier, $this->question_order, PDO::PARAM_INT);
                        break;
                    case '`question_type`':
                        $stmt->bindValue($identifier, $this->question_type, PDO::PARAM_INT);
                        break;
                    case '`question_type_new`':
                        $stmt->bindValue($identifier, $this->question_type_new, PDO::PARAM_STR);
                        break;
                    case '`question_settings`':
                        $stmt->bindValue($identifier, $this->question_settings, PDO::PARAM_STR);
                        break;
                    case '`category`':
                        $stmt->bindValue($identifier, $this->category, PDO::PARAM_STR);
                        break;
                    case '`deleted`':
                        $stmt->bindValue($identifier, $this->deleted, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setQuestionId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = QuestionsPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuestionsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getQuestionId();
                break;
            case 1:
                return $this->getQuizId();
                break;
            case 2:
                return $this->getQuestionName();
                break;
            case 3:
                return $this->getAnswerArray();
                break;
            case 4:
                return $this->getAnswerOne();
                break;
            case 5:
                return $this->getAnswerOnePoints();
                break;
            case 6:
                return $this->getAnswerTwo();
                break;
            case 7:
                return $this->getAnswerTwoPoints();
                break;
            case 8:
                return $this->getAnswerThree();
                break;
            case 9:
                return $this->getAnswerThreePoints();
                break;
            case 10:
                return $this->getAnswerFour();
                break;
            case 11:
                return $this->getAnswerFourPoints();
                break;
            case 12:
                return $this->getAnswerFive();
                break;
            case 13:
                return $this->getAnswerFivePoints();
                break;
            case 14:
                return $this->getAnswerSix();
                break;
            case 15:
                return $this->getAnswerSixPoints();
                break;
            case 16:
                return $this->getCorrectAnswer();
                break;
            case 17:
                return $this->getQuestionAnswerInfo();
                break;
            case 18:
                return $this->getComments();
                break;
            case 19:
                return $this->getHints();
                break;
            case 20:
                return $this->getQuestionOrder();
                break;
            case 21:
                return $this->getQuestionType();
                break;
            case 22:
                return $this->getQuestionTypeNew();
                break;
            case 23:
                return $this->getQuestionSettings();
                break;
            case 24:
                return $this->getCategory();
                break;
            case 25:
                return $this->getDeleted();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['Questions'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Questions'][$this->getPrimaryKey()] = true;
        $keys = QuestionsPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getQuestionId(),
            $keys[1] => $this->getQuizId(),
            $keys[2] => $this->getQuestionName(),
            $keys[3] => $this->getAnswerArray(),
            $keys[4] => $this->getAnswerOne(),
            $keys[5] => $this->getAnswerOnePoints(),
            $keys[6] => $this->getAnswerTwo(),
            $keys[7] => $this->getAnswerTwoPoints(),
            $keys[8] => $this->getAnswerThree(),
            $keys[9] => $this->getAnswerThreePoints(),
            $keys[10] => $this->getAnswerFour(),
            $keys[11] => $this->getAnswerFourPoints(),
            $keys[12] => $this->getAnswerFive(),
            $keys[13] => $this->getAnswerFivePoints(),
            $keys[14] => $this->getAnswerSix(),
            $keys[15] => $this->getAnswerSixPoints(),
            $keys[16] => $this->getCorrectAnswer(),
            $keys[17] => $this->getQuestionAnswerInfo(),
            $keys[18] => $this->getComments(),
            $keys[19] => $this->getHints(),
            $keys[20] => $this->getQuestionOrder(),
            $keys[21] => $this->getQuestionType(),
            $keys[22] => $this->getQuestionTypeNew(),
            $keys[23] => $this->getQuestionSettings(),
            $keys[24] => $this->getCategory(),
            $keys[25] => $this->getDeleted(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuestionsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setQuestionId($value);
                break;
            case 1:
                $this->setQuizId($value);
                break;
            case 2:
                $this->setQuestionName($value);
                break;
            case 3:
                $this->setAnswerArray($value);
                break;
            case 4:
                $this->setAnswerOne($value);
                break;
            case 5:
                $this->setAnswerOnePoints($value);
                break;
            case 6:
                $this->setAnswerTwo($value);
                break;
            case 7:
                $this->setAnswerTwoPoints($value);
                break;
            case 8:
                $this->setAnswerThree($value);
                break;
            case 9:
                $this->setAnswerThreePoints($value);
                break;
            case 10:
                $this->setAnswerFour($value);
                break;
            case 11:
                $this->setAnswerFourPoints($value);
                break;
            case 12:
                $this->setAnswerFive($value);
                break;
            case 13:
                $this->setAnswerFivePoints($value);
                break;
            case 14:
                $this->setAnswerSix($value);
                break;
            case 15:
                $this->setAnswerSixPoints($value);
                break;
            case 16:
                $this->setCorrectAnswer($value);
                break;
            case 17:
                $this->setQuestionAnswerInfo($value);
                break;
            case 18:
                $this->setComments($value);
                break;
            case 19:
                $this->setHints($value);
                break;
            case 20:
                $this->setQuestionOrder($value);
                break;
            case 21:
                $this->setQuestionType($value);
                break;
            case 22:
                $this->setQuestionTypeNew($value);
                break;
            case 23:
                $this->setQuestionSettings($value);
                break;
            case 24:
                $this->setCategory($value);
                break;
            case 25:
                $this->setDeleted($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = QuestionsPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setQuestionId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setQuizId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setQuestionName($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setAnswerArray($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setAnswerOne($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setAnswerOnePoints($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setAnswerTwo($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setAnswerTwoPoints($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setAnswerThree($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setAnswerThreePoints($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setAnswerFour($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setAnswerFourPoints($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setAnswerFive($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setAnswerFivePoints($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setAnswerSix($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setAnswerSixPoints($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setCorrectAnswer($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setQuestionAnswerInfo($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setComments($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setHints($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setQuestionOrder($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setQuestionType($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setQuestionTypeNew($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setQuestionSettings($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setCategory($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setDeleted($arr[$keys[25]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(QuestionsPeer::DATABASE_NAME);

        if ($this->isColumnModified(QuestionsPeer::QUESTION_ID)) $criteria->add(QuestionsPeer::QUESTION_ID, $this->question_id);
        if ($this->isColumnModified(QuestionsPeer::QUIZ_ID)) $criteria->add(QuestionsPeer::QUIZ_ID, $this->quiz_id);
        if ($this->isColumnModified(QuestionsPeer::QUESTION_NAME)) $criteria->add(QuestionsPeer::QUESTION_NAME, $this->question_name);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_ARRAY)) $criteria->add(QuestionsPeer::ANSWER_ARRAY, $this->answer_array);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_ONE)) $criteria->add(QuestionsPeer::ANSWER_ONE, $this->answer_one);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_ONE_POINTS)) $criteria->add(QuestionsPeer::ANSWER_ONE_POINTS, $this->answer_one_points);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_TWO)) $criteria->add(QuestionsPeer::ANSWER_TWO, $this->answer_two);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_TWO_POINTS)) $criteria->add(QuestionsPeer::ANSWER_TWO_POINTS, $this->answer_two_points);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_THREE)) $criteria->add(QuestionsPeer::ANSWER_THREE, $this->answer_three);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_THREE_POINTS)) $criteria->add(QuestionsPeer::ANSWER_THREE_POINTS, $this->answer_three_points);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_FOUR)) $criteria->add(QuestionsPeer::ANSWER_FOUR, $this->answer_four);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_FOUR_POINTS)) $criteria->add(QuestionsPeer::ANSWER_FOUR_POINTS, $this->answer_four_points);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_FIVE)) $criteria->add(QuestionsPeer::ANSWER_FIVE, $this->answer_five);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_FIVE_POINTS)) $criteria->add(QuestionsPeer::ANSWER_FIVE_POINTS, $this->answer_five_points);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_SIX)) $criteria->add(QuestionsPeer::ANSWER_SIX, $this->answer_six);
        if ($this->isColumnModified(QuestionsPeer::ANSWER_SIX_POINTS)) $criteria->add(QuestionsPeer::ANSWER_SIX_POINTS, $this->answer_six_points);
        if ($this->isColumnModified(QuestionsPeer::CORRECT_ANSWER)) $criteria->add(QuestionsPeer::CORRECT_ANSWER, $this->correct_answer);
        if ($this->isColumnModified(QuestionsPeer::QUESTION_ANSWER_INFO)) $criteria->add(QuestionsPeer::QUESTION_ANSWER_INFO, $this->question_answer_info);
        if ($this->isColumnModified(QuestionsPeer::COMMENTS)) $criteria->add(QuestionsPeer::COMMENTS, $this->comments);
        if ($this->isColumnModified(QuestionsPeer::HINTS)) $criteria->add(QuestionsPeer::HINTS, $this->hints);
        if ($this->isColumnModified(QuestionsPeer::QUESTION_ORDER)) $criteria->add(QuestionsPeer::QUESTION_ORDER, $this->question_order);
        if ($this->isColumnModified(QuestionsPeer::QUESTION_TYPE)) $criteria->add(QuestionsPeer::QUESTION_TYPE, $this->question_type);
        if ($this->isColumnModified(QuestionsPeer::QUESTION_TYPE_NEW)) $criteria->add(QuestionsPeer::QUESTION_TYPE_NEW, $this->question_type_new);
        if ($this->isColumnModified(QuestionsPeer::QUESTION_SETTINGS)) $criteria->add(QuestionsPeer::QUESTION_SETTINGS, $this->question_settings);
        if ($this->isColumnModified(QuestionsPeer::CATEGORY)) $criteria->add(QuestionsPeer::CATEGORY, $this->category);
        if ($this->isColumnModified(QuestionsPeer::DELETED)) $criteria->add(QuestionsPeer::DELETED, $this->deleted);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(QuestionsPeer::DATABASE_NAME);
        $criteria->add(QuestionsPeer::QUESTION_ID, $this->question_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getQuestionId();
    }

    /**
     * Generic method to set the primary key (question_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setQuestionId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getQuestionId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Questions (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setQuizId($this->getQuizId());
        $copyObj->setQuestionName($this->getQuestionName());
        $copyObj->setAnswerArray($this->getAnswerArray());
        $copyObj->setAnswerOne($this->getAnswerOne());
        $copyObj->setAnswerOnePoints($this->getAnswerOnePoints());
        $copyObj->setAnswerTwo($this->getAnswerTwo());
        $copyObj->setAnswerTwoPoints($this->getAnswerTwoPoints());
        $copyObj->setAnswerThree($this->getAnswerThree());
        $copyObj->setAnswerThreePoints($this->getAnswerThreePoints());
        $copyObj->setAnswerFour($this->getAnswerFour());
        $copyObj->setAnswerFourPoints($this->getAnswerFourPoints());
        $copyObj->setAnswerFive($this->getAnswerFive());
        $copyObj->setAnswerFivePoints($this->getAnswerFivePoints());
        $copyObj->setAnswerSix($this->getAnswerSix());
        $copyObj->setAnswerSixPoints($this->getAnswerSixPoints());
        $copyObj->setCorrectAnswer($this->getCorrectAnswer());
        $copyObj->setQuestionAnswerInfo($this->getQuestionAnswerInfo());
        $copyObj->setComments($this->getComments());
        $copyObj->setHints($this->getHints());
        $copyObj->setQuestionOrder($this->getQuestionOrder());
        $copyObj->setQuestionType($this->getQuestionType());
        $copyObj->setQuestionTypeNew($this->getQuestionTypeNew());
        $copyObj->setQuestionSettings($this->getQuestionSettings());
        $copyObj->setCategory($this->getCategory());
        $copyObj->setDeleted($this->getDeleted());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setQuestionId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Questions Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return QuestionsPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new QuestionsPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->question_id = null;
        $this->quiz_id = null;
        $this->question_name = null;
        $this->answer_array = null;
        $this->answer_one = null;
        $this->answer_one_points = null;
        $this->answer_two = null;
        $this->answer_two_points = null;
        $this->answer_three = null;
        $this->answer_three_points = null;
        $this->answer_four = null;
        $this->answer_four_points = null;
        $this->answer_five = null;
        $this->answer_five_points = null;
        $this->answer_six = null;
        $this->answer_six_points = null;
        $this->correct_answer = null;
        $this->question_answer_info = null;
        $this->comments = null;
        $this->hints = null;
        $this->question_order = null;
        $this->question_type = null;
        $this->question_type_new = null;
        $this->question_settings = null;
        $this->category = null;
        $this->deleted = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(QuestionsPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
