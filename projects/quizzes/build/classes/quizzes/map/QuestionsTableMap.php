<?php



/**
 * This class defines the structure of the 'rfn_questions' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.quizzes.map
 */
class QuestionsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'quizzes.map.QuestionsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rfn_questions');
        $this->setPhpName('Questions');
        $this->setClassname('Questions');
        $this->setPackage('quizzes');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('question_id', 'QuestionId', 'INTEGER', true, 9, null);
        $this->addColumn('quiz_id', 'QuizId', 'INTEGER', true, null, null);
        $this->addColumn('question_name', 'QuestionName', 'LONGVARCHAR', true, null, null);
        $this->addColumn('answer_array', 'AnswerArray', 'LONGVARCHAR', true, null, null);
        $this->addColumn('answer_one', 'AnswerOne', 'LONGVARCHAR', true, null, null);
        $this->addColumn('answer_one_points', 'AnswerOnePoints', 'INTEGER', true, null, null);
        $this->addColumn('answer_two', 'AnswerTwo', 'LONGVARCHAR', true, null, null);
        $this->addColumn('answer_two_points', 'AnswerTwoPoints', 'INTEGER', true, null, null);
        $this->addColumn('answer_three', 'AnswerThree', 'LONGVARCHAR', true, null, null);
        $this->addColumn('answer_three_points', 'AnswerThreePoints', 'INTEGER', true, null, null);
        $this->addColumn('answer_four', 'AnswerFour', 'LONGVARCHAR', true, null, null);
        $this->addColumn('answer_four_points', 'AnswerFourPoints', 'INTEGER', true, null, null);
        $this->addColumn('answer_five', 'AnswerFive', 'LONGVARCHAR', true, null, null);
        $this->addColumn('answer_five_points', 'AnswerFivePoints', 'INTEGER', true, null, null);
        $this->addColumn('answer_six', 'AnswerSix', 'LONGVARCHAR', true, null, null);
        $this->addColumn('answer_six_points', 'AnswerSixPoints', 'INTEGER', true, null, null);
        $this->addColumn('correct_answer', 'CorrectAnswer', 'INTEGER', true, null, null);
        $this->addColumn('question_answer_info', 'QuestionAnswerInfo', 'LONGVARCHAR', true, null, null);
        $this->addColumn('comments', 'Comments', 'INTEGER', true, null, null);
        $this->addColumn('hints', 'Hints', 'LONGVARCHAR', true, null, null);
        $this->addColumn('question_order', 'QuestionOrder', 'INTEGER', true, null, null);
        $this->addColumn('question_type', 'QuestionType', 'INTEGER', true, null, null);
        $this->addColumn('question_type_new', 'QuestionTypeNew', 'LONGVARCHAR', true, null, null);
        $this->addColumn('question_settings', 'QuestionSettings', 'LONGVARCHAR', true, null, null);
        $this->addColumn('category', 'Category', 'LONGVARCHAR', true, null, null);
        $this->addColumn('deleted', 'Deleted', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // QuestionsTableMap
