<?php



/**
 * This class defines the structure of the 'rfn_quizzes' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.quizzes.map
 */
class QuizzesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'quizzes.map.QuizzesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rfn_quizzes');
        $this->setPhpName('Quizzes');
        $this->setClassname('Quizzes');
        $this->setPackage('quizzes');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('quiz_id', 'QuizId', 'INTEGER', true, 9, null);
        $this->addColumn('quiz_name', 'QuizName', 'LONGVARCHAR', true, null, null);
        $this->addColumn('message_before', 'MessageBefore', 'LONGVARCHAR', true, null, null);
        $this->addColumn('message_after', 'MessageAfter', 'LONGVARCHAR', true, null, null);
        $this->addColumn('message_comment', 'MessageComment', 'LONGVARCHAR', true, null, null);
        $this->addColumn('message_end_template', 'MessageEndTemplate', 'LONGVARCHAR', true, null, null);
        $this->addColumn('user_email_template', 'UserEmailTemplate', 'LONGVARCHAR', true, null, null);
        $this->addColumn('admin_email_template', 'AdminEmailTemplate', 'LONGVARCHAR', true, null, null);
        $this->addColumn('submit_button_text', 'SubmitButtonText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('name_field_text', 'NameFieldText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('business_field_text', 'BusinessFieldText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('email_field_text', 'EmailFieldText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('phone_field_text', 'PhoneFieldText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('comment_field_text', 'CommentFieldText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('email_from_text', 'EmailFromText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('question_answer_template', 'QuestionAnswerTemplate', 'LONGVARCHAR', true, null, null);
        $this->addColumn('leaderboard_template', 'LeaderboardTemplate', 'LONGVARCHAR', true, null, null);
        $this->addColumn('system', 'System', 'INTEGER', true, null, null);
        $this->addColumn('randomness_order', 'RandomnessOrder', 'INTEGER', true, null, null);
        $this->addColumn('loggedin_user_contact', 'LoggedinUserContact', 'INTEGER', true, null, null);
        $this->addColumn('show_score', 'ShowScore', 'INTEGER', true, null, null);
        $this->addColumn('send_user_email', 'SendUserEmail', 'INTEGER', true, null, null);
        $this->addColumn('send_admin_email', 'SendAdminEmail', 'INTEGER', true, null, null);
        $this->addColumn('contact_info_location', 'ContactInfoLocation', 'INTEGER', true, null, null);
        $this->addColumn('user_name', 'UserName', 'INTEGER', true, null, null);
        $this->addColumn('user_comp', 'UserComp', 'INTEGER', true, null, null);
        $this->addColumn('user_email', 'UserEmail', 'INTEGER', true, null, null);
        $this->addColumn('user_phone', 'UserPhone', 'INTEGER', true, null, null);
        $this->addColumn('admin_email', 'AdminEmail', 'LONGVARCHAR', true, null, null);
        $this->addColumn('comment_section', 'CommentSection', 'INTEGER', true, null, null);
        $this->addColumn('question_from_total', 'QuestionFromTotal', 'INTEGER', true, null, null);
        $this->addColumn('total_user_tries', 'TotalUserTries', 'INTEGER', true, null, null);
        $this->addColumn('total_user_tries_text', 'TotalUserTriesText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('certificate_template', 'CertificateTemplate', 'LONGVARCHAR', true, null, null);
        $this->addColumn('social_media', 'SocialMedia', 'INTEGER', true, null, null);
        $this->addColumn('social_media_text', 'SocialMediaText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('pagination', 'Pagination', 'INTEGER', true, null, null);
        $this->addColumn('pagination_text', 'PaginationText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('timer_limit', 'TimerLimit', 'INTEGER', true, null, null);
        $this->addColumn('quiz_stye', 'QuizStye', 'LONGVARCHAR', true, null, null);
        $this->addColumn('question_numbering', 'QuestionNumbering', 'INTEGER', true, null, null);
        $this->addColumn('quiz_settings', 'QuizSettings', 'LONGVARCHAR', true, null, null);
        $this->addColumn('theme_selected', 'ThemeSelected', 'LONGVARCHAR', true, null, null);
        $this->addColumn('last_activity', 'LastActivity', 'TIMESTAMP', true, null, null);
        $this->addColumn('require_log_in', 'RequireLogIn', 'INTEGER', true, null, null);
        $this->addColumn('require_log_in_text', 'RequireLogInText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('limit_total_entries', 'LimitTotalEntries', 'INTEGER', true, null, null);
        $this->addColumn('limit_total_entries_text', 'LimitTotalEntriesText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('scheduled_timeframe', 'ScheduledTimeframe', 'LONGVARCHAR', true, null, null);
        $this->addColumn('scheduled_timeframe_text', 'ScheduledTimeframeText', 'LONGVARCHAR', true, null, null);
        $this->addColumn('disable_answer_onselect', 'DisableAnswerOnselect', 'INTEGER', true, null, null);
        $this->addColumn('ajax_show_correct', 'AjaxShowCorrect', 'INTEGER', true, null, null);
        $this->addColumn('quiz_views', 'QuizViews', 'INTEGER', true, null, null);
        $this->addColumn('quiz_taken', 'QuizTaken', 'INTEGER', true, null, null);
        $this->addColumn('deleted', 'Deleted', 'INTEGER', true, null, null);
        $this->addColumn('quiz_description', 'QuizDescription', 'LONGVARCHAR', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // QuizzesTableMap
