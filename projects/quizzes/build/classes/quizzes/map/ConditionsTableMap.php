<?php



/**
 * This class defines the structure of the 'rfn_conditions' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.quizzes.map
 */
class ConditionsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'quizzes.map.ConditionsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rfn_conditions');
        $this->setPhpName('Conditions');
        $this->setClassname('Conditions');
        $this->setPackage('quizzes');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('question_id', 'QuestionId', 'INTEGER', true, null, null);
        $this->addColumn('quiz_id', 'QuizId', 'INTEGER', true, null, null);
        $this->addColumn('condition_array', 'ConditionArray', 'LONGVARCHAR', true, null, null);
        $this->addColumn('deleted', 'Deleted', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // ConditionsTableMap
