<?php



/**
 * This class defines the structure of the 'rfn_results' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.quizzes.map
 */
class ResultsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'quizzes.map.ResultsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rfn_results');
        $this->setPhpName('Results');
        $this->setClassname('Results');
        $this->setPackage('quizzes');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('result_id', 'ResultId', 'INTEGER', true, 9, null);
        $this->addColumn('quiz_id', 'QuizId', 'INTEGER', true, null, null);
        $this->addColumn('quiz_name', 'QuizName', 'LONGVARCHAR', true, null, null);
        $this->addColumn('quiz_system', 'QuizSystem', 'INTEGER', true, null, null);
        $this->addColumn('point_score', 'PointScore', 'INTEGER', true, null, null);
        $this->addColumn('correct_score', 'CorrectScore', 'INTEGER', true, null, null);
        $this->addColumn('correct', 'Correct', 'INTEGER', true, null, null);
        $this->addColumn('total', 'Total', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'LONGVARCHAR', true, null, null);
        $this->addColumn('business', 'Business', 'LONGVARCHAR', true, null, null);
        $this->addColumn('email', 'Email', 'LONGVARCHAR', true, null, null);
        $this->addColumn('phone', 'Phone', 'LONGVARCHAR', true, null, null);
        $this->addColumn('user', 'User', 'INTEGER', true, null, null);
        $this->addColumn('user_ip', 'UserIp', 'LONGVARCHAR', true, null, null);
        $this->addColumn('time_taken', 'TimeTaken', 'LONGVARCHAR', true, null, null);
        $this->addColumn('time_taken_real', 'TimeTakenReal', 'TIMESTAMP', true, null, null);
        $this->addColumn('quiz_results', 'QuizResults', 'LONGVARCHAR', true, null, null);
        $this->addColumn('deleted', 'Deleted', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // ResultsTableMap
