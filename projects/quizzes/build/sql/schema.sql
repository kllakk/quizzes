
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- rfn_quizzes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rfn_quizzes`;

CREATE TABLE `rfn_quizzes`
(
    `quiz_id` INTEGER(9) NOT NULL AUTO_INCREMENT,
    `quiz_name` TEXT NOT NULL,
    `message_before` TEXT NOT NULL,
    `message_after` TEXT NOT NULL,
    `message_comment` TEXT NOT NULL,
    `message_end_template` TEXT NOT NULL,
    `user_email_template` TEXT NOT NULL,
    `admin_email_template` TEXT NOT NULL,
    `submit_button_text` TEXT NOT NULL,
    `name_field_text` TEXT NOT NULL,
    `business_field_text` TEXT NOT NULL,
    `email_field_text` TEXT NOT NULL,
    `phone_field_text` TEXT NOT NULL,
    `comment_field_text` TEXT NOT NULL,
    `email_from_text` TEXT NOT NULL,
    `question_answer_template` TEXT NOT NULL,
    `leaderboard_template` TEXT NOT NULL,
    `system` INTEGER NOT NULL,
    `randomness_order` INTEGER NOT NULL,
    `loggedin_user_contact` INTEGER NOT NULL,
    `show_score` INTEGER NOT NULL,
    `send_user_email` INTEGER NOT NULL,
    `send_admin_email` INTEGER NOT NULL,
    `contact_info_location` INTEGER NOT NULL,
    `user_name` INTEGER NOT NULL,
    `user_comp` INTEGER NOT NULL,
    `user_email` INTEGER NOT NULL,
    `user_phone` INTEGER NOT NULL,
    `admin_email` TEXT NOT NULL,
    `comment_section` INTEGER NOT NULL,
    `question_from_total` INTEGER NOT NULL,
    `total_user_tries` INTEGER NOT NULL,
    `total_user_tries_text` TEXT NOT NULL,
    `certificate_template` TEXT NOT NULL,
    `social_media` INTEGER NOT NULL,
    `social_media_text` TEXT NOT NULL,
    `pagination` INTEGER NOT NULL,
    `pagination_text` TEXT NOT NULL,
    `timer_limit` INTEGER NOT NULL,
    `quiz_stye` TEXT NOT NULL,
    `question_numbering` INTEGER NOT NULL,
    `quiz_settings` TEXT NOT NULL,
    `theme_selected` TEXT NOT NULL,
    `last_activity` DATETIME NOT NULL,
    `require_log_in` INTEGER NOT NULL,
    `require_log_in_text` TEXT NOT NULL,
    `limit_total_entries` INTEGER NOT NULL,
    `limit_total_entries_text` TEXT NOT NULL,
    `scheduled_timeframe` TEXT NOT NULL,
    `scheduled_timeframe_text` TEXT NOT NULL,
    `disable_answer_onselect` INTEGER NOT NULL,
    `ajax_show_correct` INTEGER NOT NULL,
    `quiz_views` INTEGER NOT NULL,
    `quiz_taken` INTEGER NOT NULL,
    `deleted` INTEGER NOT NULL,
    `quiz_description` TEXT NOT NULL,
    PRIMARY KEY (`quiz_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- rfn_questions
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rfn_questions`;

CREATE TABLE `rfn_questions`
(
    `question_id` INTEGER(9) NOT NULL AUTO_INCREMENT,
    `quiz_id` INTEGER NOT NULL,
    `question_name` TEXT NOT NULL,
    `answer_array` TEXT NOT NULL,
    `answer_one` TEXT NOT NULL,
    `answer_one_points` INTEGER NOT NULL,
    `answer_two` TEXT NOT NULL,
    `answer_two_points` INTEGER NOT NULL,
    `answer_three` TEXT NOT NULL,
    `answer_three_points` INTEGER NOT NULL,
    `answer_four` TEXT NOT NULL,
    `answer_four_points` INTEGER NOT NULL,
    `answer_five` TEXT NOT NULL,
    `answer_five_points` INTEGER NOT NULL,
    `answer_six` TEXT NOT NULL,
    `answer_six_points` INTEGER NOT NULL,
    `correct_answer` INTEGER NOT NULL,
    `question_answer_info` TEXT NOT NULL,
    `comments` INTEGER NOT NULL,
    `hints` TEXT NOT NULL,
    `question_order` INTEGER NOT NULL,
    `question_type` INTEGER NOT NULL,
    `question_type_new` TEXT NOT NULL,
    `question_settings` TEXT NOT NULL,
    `category` TEXT NOT NULL,
    `deleted` INTEGER NOT NULL,
    PRIMARY KEY (`question_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- rfn_conditions
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rfn_conditions`;

CREATE TABLE `rfn_conditions`
(
    `question_id` INTEGER NOT NULL,
    `quiz_id` INTEGER NOT NULL,
    `condition_array` TEXT NOT NULL,
    `deleted` INTEGER NOT NULL,
    PRIMARY KEY (`question_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- rfn_results
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rfn_results`;

CREATE TABLE `rfn_results`
(
    `result_id` INTEGER(9) NOT NULL AUTO_INCREMENT,
    `quiz_id` INTEGER NOT NULL,
    `quiz_name` TEXT NOT NULL,
    `quiz_system` INTEGER NOT NULL,
    `point_score` INTEGER NOT NULL,
    `correct_score` INTEGER NOT NULL,
    `correct` INTEGER NOT NULL,
    `total` INTEGER NOT NULL,
    `name` TEXT NOT NULL,
    `business` TEXT NOT NULL,
    `email` TEXT NOT NULL,
    `phone` TEXT NOT NULL,
    `user` INTEGER NOT NULL,
    `user_ip` TEXT NOT NULL,
    `time_taken` TEXT NOT NULL,
    `time_taken_real` DATETIME NOT NULL,
    `quiz_results` TEXT NOT NULL,
    `deleted` INTEGER NOT NULL,
    PRIMARY KEY (`result_id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
