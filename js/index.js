document.addEventListener('DOMContentLoaded', function () {
    new Vue({
        el: '#app',
        components: {
            'app': httpVueLoader('js/components/App.vue')
        }
    });
});