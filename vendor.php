<?php
require ('vendor/autoload.php');

// Initialize Propel with the runtime configuration
Propel::init(__DIR__ . "/projects/quizzes/build/conf/quizzes-conf.php");

// Add the generated 'classes' directory to the include path
set_include_path(__DIR__ . "/projects/quizzes/build/classes" . PATH_SEPARATOR . get_include_path());

$app = new \Slim\Slim();