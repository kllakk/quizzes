#### модуль состоит из двух частей

* управление опросами **quizzes**, необходимо распаковать в корне сайта в папку /quizzes и, затем, небходимо дать права на папку загрузок chmod 777 /quizzes/upload
* компонент опросов **quizzes.component**, необходимо распаковать в папку /bitrix/components/rfn/quizzes

#### в коде страницы с опросом нужно зарегистрировать компонент

```php
<?php $APPLICATION->IncludeComponent(
	"rfn:quizzes",
	".default"
);?>
```

после чего можно создавать и редактировать опросы по пути http://site/quizzes

кнопка для вывода окна опроса на сайте копируется из списка опросов http://site/quizzes