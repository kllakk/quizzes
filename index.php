<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER, $DB;

if ($USER->IsAuthorized()) {

    require('vendor.php');

    if (!$DB->TableExists('rfn_quizzes')) {
        $results = $DB->RunSQLBatch(__DIR__ . '/projects/quizzes/build/sql/schema.sql');
    }

    $app->get('/', function () use ($app) {
        $app->render('home.php');
    });

    $quizzesController = new QuizzesController($app);
    $app->get('/preview/:id', function ($id) use($quizzesController, $app) {
        $quizzesController->preview($id);
    });

    $app->get('/api/quizzes/', function () use ($quizzesController) {
        echo $quizzesController->get();
    });

    $app->post('/api/quizzes/', function () use ($quizzesController) {
        echo $quizzesController->post();
    });

    $app->post('/api/quizzes/import', function () use ($quizzesController) {
        echo $quizzesController->import();
    });

    $app->get('/api/quizzes/export/:id', function ($id) use ($quizzesController) {
        $result = $quizzesController->export($id);
        header('Content-Type: application/json; charset=UTF-8');
        header("Content-Disposition: attachment; filename=export.json");
        ob_end_clean();
        $out = fopen('php://output', 'w');
        fwrite($out, json_encode($result));
        fclose($out);
        ob_flush();
        exit();
    });

    $questionsController = new QuestionsController();
    $app->get('/api/questions/', function () use ($questionsController) {
        echo $questionsController->get();
    });

    $app->post('/api/questions/', function () use($questionsController) {
        echo $questionsController->post();
    });

    $conditionsController = new ConditionsController();
    $app->get('/api/conditions/', function () use ($conditionsController) {
        echo $conditionsController->get();
    });

    $app->post('/api/conditions/', function () use ($conditionsController) {
        echo $conditionsController->post();
    });

    $resultsController = new ResultsController();
    $app->post('/api/results/submit', function () use ($resultsController) {
        echo $resultsController->submit();
    });

    $app->run();
}